makingChampsApp.controller('makingChampsGalleryCtrl',['$scope','apiUtility','Lightbox',function($scope,apiUtility,Lightbox){


    $scope.Lightbox = Lightbox;
    $scope.GalleryModel = {

        gallery : [],
        selectedCategory:0,
        selectedEvent:0,
        filteredEvents:[],
        sorted:0,
        filteredGallery:[]


    };

    $scope.setSelectedCategory=function(index){
        $scope.GalleryModel.selectedCategory=index;
        if(index==0){
            var events=[];
            for(var i=0;i<$scope.GalleryModel.gallery.length;i++){
                for(var j=0;j<$scope.GalleryModel.gallery[i].events.length;j++)
                    events.push($scope.GalleryModel.gallery[i].events[j])
                    }
            $scope.GalleryModel.filteredEvents=events;
        } else{
            $scope.GalleryModel.filteredEvents=$scope.GalleryModel.gallery[$scope.GalleryModel.selectedCategory-1].events;
        }
        this.setSelectedEvent(0);
    }

    
    $scope.setSelectedEvent=function(index){
        $scope.GalleryModel.selectedEvent=index;
        var photos=[];
        if(index==0){
            for(var i=0;i<$scope.GalleryModel.filteredEvents.length;i++){
                //                    console.log($scope.GalleryModel.filteredEvents[i].photos);
                for(var j=0;j<$scope.GalleryModel.filteredEvents[i].photos.length;j++){

                    photos.push($scope.GalleryModel.filteredEvents[i].photos[j])
                }
            }
        } else {
            photos=$scope.GalleryModel.filteredEvents[index-1].photos;
        }
        $scope.GalleryModel.filteredGallery=photos;
        $scope.openLightboxModal = function (index) {
            $scope.Lightbox.openModal($scope.GalleryModel.filteredGallery, index);
        };
        $scope.Lightbox.nextImage=function(){
            if($scope.GalleryModel.filteredGallery[(Lightbox.index + 1) % Lightbox.images.length].type!='image'){
                
            while($scope.GalleryModel.filteredGallery[(Lightbox.index + 1) % Lightbox.images.length].type!='image'){
                
                Lightbox.index=  (Lightbox.index + 1) % Lightbox.images.length;
            }
            Lightbox.setImage((Lightbox.index + 1) % Lightbox.images.length);
            } else {
                
            Lightbox.setImage((Lightbox.index + 1) % Lightbox.images.length);
            }
        }
        
        $scope.Lightbox.prevImage=function(){
            if($scope.GalleryModel.filteredGallery[(Lightbox.index - 1 + Lightbox.images.length) %
                                                   Lightbox.images.length].type!='image'){
                
                while($scope.GalleryModel.filteredGallery[(Lightbox.index - 1 + Lightbox.images.length) %
                                                          Lightbox.images.length].type!='image'){
                
                    Lightbox.index=  (Lightbox.index - 1 + Lightbox.images.length) %
                        Lightbox.images.length;
            }
                Lightbox.setImage((Lightbox.index - 1 + Lightbox.images.length) %
                                  Lightbox.images.length);
            } else {
                
                Lightbox.setImage((Lightbox.index - 1 + Lightbox.images.length) %
                                  Lightbox.images.length);
            }
        }
    }
    

    apiUtility.executeApi('GET','/index.php/api/v1/getPhotos','getPhotos')
    .then(function(result)
          {


        for(var i=0;i<result.length;i++){

            var is_present=false;
            for(j=0;j<$scope.GalleryModel.gallery.length;j++){

                if($scope.GalleryModel.gallery[j].category==result[i].category){
                    is_present=true;
                    break;                            
                }
            }
            if(is_present==true){
                $scope.GalleryModel.gallery[j].events.push(result[i]);
            } else {
                var new_gallery_cat = {
                    category : result[i].category,
                    events : []
                }
                new_gallery_cat.category=result[i].category;
                new_gallery_cat.events.push(result[i]);
                $scope.GalleryModel.gallery.push(new_gallery_cat);
            }
        }
              for(var i=0;i<$scope.GalleryModel.gallery.length;i++){

                  for(var j=0;j<$scope.GalleryModel.gallery[i].events.length;j++){
                    for(var k=0;k<$scope.GalleryModel.gallery[i].events[j].photos.length;k++){
                        url=$scope.GalleryModel.gallery[i].events[j].photos[k].type=='video'?$scope.GalleryModel.gallery[i].events[j].photos[k].name:'http://makingchamps.co.in/content/gallery/'+window.encodeURIComponent($scope.GalleryModel.gallery[i].events[j].photos[k].name);
                        $scope.GalleryModel.gallery[i].events[j].photos[k].shared=0;
                        $scope.GalleryModel.gallery[i].events[j].photos[k].url=(url);
                        $scope.getShareCount(i,j,k,window.encodeURIComponent(url));
                    }

                  }
              }
        $scope.setSelectedCategory($scope.GalleryModel.selectedCategory);
    });
    
    $scope.getShareCount= function (i,j,k,url) {
        apiUtility.executeApi('GET','http://graph.facebook.com/?id='+url)
            .then(function(data)
            {
                $scope.GalleryModel.gallery[i].events[j].photos[k].shared=(data.shares!=undefined?data.shares:0);
            });


    }
    $scope.layoutDone=function(){
        //        $scope.$emit('iso-method', {name:'shuffle', params:null}); 
        //        $scope.$emit('iso-method', {name:'filter', params:'.'}); 
    }

    
    
    

    

}]);