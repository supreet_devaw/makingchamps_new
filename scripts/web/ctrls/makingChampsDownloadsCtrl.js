makingChampsApp.controller('makingChampsDownloadsCtrl',['$scope','$rootScope','apiUtility',function($scope,$rootScope,apiUtility){

    $scope.DownloadModel = {

    	categories : {}

    };

    apiUtility.executeApi('GET','/index.php/api/v1/getDownloads')
    	.then(function(result){


    		$scope.DownloadModel.categories = result;
    		$rootScope.$broadcast('iso-init', {name:null, params:null}); 

    	});

}]);

angular.module('makingChampsApp').filter('downloadVisibility', function () {
    return function (value, loggedInUser) {
       // console.log(value);
       // console.log(loggedInUser);
        if (!value) 
        	return false;

       // if(loggedInUser.id && value.access != 'all')
        {
        //	return false;
        }

       return true;
    }
});
