makingChampsApp.controller('makingChampsBatchesCtrl',['$scope','$ngBootbox','apiUtility',function($scope,$ngBootbox,apiUtility){


    apiUtility.executeApi('GET','/index.php/api/v1/getBatches')
        .then(function(result)
        {

            $scope.batches = result;

        });


    apiUtility.executeApi('GET','/index.php/api/v1/getSchools')
    .then(function(result){

        $scope.schools = result;

    });

    
}]);
