makingChampsApp.controller('makingChampsMyAccountCtrl',['$scope','apiUtility','userUtility','$state','$ngBootbox','FileUploader',function($scope,apiUtility,userUtility,$state,$ngBootbox,FileUploader){

    if(!(userUtility.isUserLoggedIn()))
    {
        $state.go('home');
    }


    $scope.loadMyAccountDetails = function()
    {
        apiUtility.executeApi('GET','/index.php/api/v1/getParentStudentDetails')
        .then(function(result){

            $scope.parentStudentDetails = result;
            var uploader=$scope.uploader={};
            var index={};
                $scope.homework=[];

            for(var i=0;i<result.homework.length;i++){
                $scope.parentStudentDetails.homework[i].method='file';
                // console.log($scope.parentStudentDetails.homework[i]);
                uploader[i] = $scope.uploader[i] = new FileUploader({
                    url: '/index.php/api/v1/homework/'+result.homework[i].id,
                    queueLimit: 1,
                    removeAfterUpload: true,
                    formData:{              
                        index:i
                    }

                });

                uploader[i].onSuccessItem = function(fileItem, response, status, headers) {
                    $scope.parentStudentDetails.homework[this.formData.index].status="reviewing";
//                    console.log(response);
                };
            }



        });

    }

    $scope.addNewTestimonial =  function()
    {

        if(!$scope.newTestimonial)
        {
            return;
        }

        apiUtility.executeApi('POST','/index.php/api/v1/testimonial',{testimonial:$scope.newTestimonial})
        .then(function(result){

            $scope.newTestimonial = '';

        });


        $ngBootbox.hideAll();

    }

    $scope.changePassword = function()
    {

         $scope.customDialogOptions = {
            templateUrl: '/views/web/change-password.html',
            title: 'Change Password'
        };

        $ngBootbox.customDialog($scope.customDialogOptions);


    }

    $scope.saveNewPassword = function()
    {

        if($scope.newPassword != $scope.newPasswordRepeated){

            $scope.changePasswordError = true;
            $scope.changePasswordErrorMessage = 'New passwords dont match. Please enter again carefully';

            return;
        }

        apiUtility.executeApi('POST','/index.php/api/v1/changeStudentPassword',{'old_password' : $scope.oldPassword, 'new_password': $scope.newPassword})
            .then(function(result){

                if(result.success)
                {
                    $ngBootbox.hideAll();
                    $ngBootbox.alert('Password changed successfully');
                }
                else
                {
                    $scope.changePasswordError = true;
                    $scope.changePasswordErrorMessage = result.msg;
                }
                 

            });

    }

    $scope.feedbackView = function(feedbackViewId)
    {
        if( feedbackViewId == $scope.currentFeedbackId)
        {
            $scope.currentFeedbackId = null;
            return;
        }

        $scope.currentFeedbackId = feedbackViewId;

        $scope.newEvent = {};

    }

    $scope.addNewEvent = function(levelTaskId,subTaskObject)
    {
        
        apiUtility.executeApi('POST','/index.php/api/v1/addStudentLevelTaskDetails',{'used_at': $scope.newEvent.place,'comment':$scope.newEvent.comment,'level_task_id':levelTaskId })
        .then(function(result){

                subTaskObject.student_tasks.push(result.data);

        });

         $scope.newEvent = {};

    }

    $scope.SubmitHomework= function (homework) {
        postHomeworkText={
            'text':homework.text
        }
        apiUtility.executeApi('POST','/index.php/api/v1/homeworkText/'+homework.id,postHomeworkText)
            .then(function (data) {
                if(data.success){
                    homework.status='reviewing';
                }
            })
    }

}]);