makingChampsApp.controller('makingChampsMainCtrl',['$scope','$rootScope','$location','userUtility','$ngBootbox','apiUtility',function($scope,$rootScope,$location,userUtility,$ngBootbox,apiUtility){


    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        if ( angular.isDefined( toState.data.pageTitle ) ) {
            $scope.pageTitle = 'Making Champs | '+ toState.data.pageTitle  ;
        }
    });

    $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        yearRange: '1900:-0'
    };


	$scope.loginModel = {};
	$scope.loggedInUser = null;
    $scope.loginCredentialsInvalid;

    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };


    $scope.loginModel.login = function()
    {

        if($scope.loginModel.username == '' || $scope.loginModel.password == '')
        {
            return;
        }

    	userUtility.login({'username':$scope.loginModel.username,'password':$scope.loginModel.password})
    		.then(function(result){


                if(result.id)
                {
                    $location.path('/MyAccount');
                    $scope.loginModel.username = '';
                     $scope.loginModel.password = '';
                     $scope.loggedInUser = result;
                     $scope.loginCredentialsInvalid = false;

                }
                else
                {

                     $scope.loginCredentialsInvalid = true;
                }

    	})

    };

    $scope.submitEnquiry=function(){
        $scope.processingEnquiryForm=true;

        $scope.enquiry.contact = '+91-' + $scope.enquiry.contact;
        
        apiUtility.executeApi('POST','/index.php/api/v1/enquiry',$scope.enquiry)
        .then(function(result)
              {
                  $scope.processingEnquiryForm=false;
        $ngBootbox.hideAll();
            $ngBootbox.alert('Thanks for giving the information. <br/>We will revert within 48 hours.')
            .then(function() {});
        });


    }


    $scope.processingEnquiryForm=false;
    var original="";
    $scope.enquiry={
        std: "",
        contact: "",
        dob: "",
        email: "",
        location: "",
        parent_name: "",
        school_name: "",
        student_name: "",
        comments: ""
    }
    $scope.canSubmit = function() {

        return $scope.enquiry_form.$valid && !angular.equals($scope.enquiry, original) && !$scope.processingEnquiryForm;
    }
    
    $scope.loginModel.logout = function()
    {


    	userUtility.logout().
    		then(function(){

    			$scope.loggedInUser = null;
    			$scope.$broadcast('scope:userLoggedOut',{});

    		});
    	
    }


	$rootScope.$on('rootScope:userLoggedIn', function (event, data) {
	   
	    $scope.loggedInUser = data;
	    $scope.$broadcast('scope:userLoggedIn',data);

	 });
    
    
    $scope.getEnquiryForm = function(){
        $scope.customDialogOptions = {
            templateUrl: '/views/web/enquiry-form.html',
            title: 'Please fill the form so that we can revert with relevant details.'
        };

        $ngBootbox.customDialog($scope.customDialogOptions);

    }

    $scope.forgotPasswordForm = function(){
        $scope.customDialogOptions = {
            templateUrl: '/views/web/forgot-password.html',
            title: 'Enter your username.',
            controller:"forgotCtrl"
        };

        $ngBootbox.customDialog($scope.customDialogOptions);

    }


    $scope.loginForm = function(){
        $scope.customDialogOptions = {
            templateUrl: '/views/web/loginForm.html',
            title: 'Sign In',
            controller:"makingChampsMainCtrl"
        };

        $ngBootbox.customDialog($scope.customDialogOptions);

    }


}]);

makingChampsApp.controller('forgotCtrl',['$scope','apiUtility','$ngBootbox',function($scope,apiUtility,$ngBootbox){
    $scope.forgotModel={
        user_name:"",
        user_type:"student"

    }
    $scope.ErrorMessage="";
    $scope.sendNewPassword= function () {
        apiUtility.executeApi('POST','/index.php/api/v1/forgot_password',$scope.forgotModel)
            .then(function(result)
            {

                if(result.success){
                    $ngBootbox.hideAll();
                    $ngBootbox.alert(result.msg);
                } else {
                    $scope.ErrorMessage=result.msg;
                }
            });
    }
}]);

$(document).scroll(function(){
            if($('body').scrollTop() > 0) {
                $('#header').css('background-color','red');
            } else {
                $('#header').css('background-color','blue');
            }
});