
makingChampsApp.controller('makingChmapsAboutCtrl',['$scope','$rootScope','apiUtility',function($scope,$rootScope,apiUtility){

    $scope.AboutModel = {

        media : {},
        team:{},
        school:{}

    };

    apiUtility.executeApi('GET','/index.php/api/v1/getMedia')
    .then(function(result){

        $scope.AboutModel.media = result;

    });
    apiUtility.executeApi('GET','/index.php/api/v1/getTeamMembers')
    .then(function(result){

        $scope.AboutModel.team = result;

    });
    
    $scope.layoutDone=function(){
        $scope.$emit('iso-method', {name:'shuffle', params:null}); 
    }

}]);