makingChampsApp.controller('makingChampsOfferingsCtrl',['$scope','$ngBootbox',function($scope,$ngBootbox){

    $scope.currentTopic = 'theneed';

    $scope.changeTopic = function(topic)
    {
        $scope.currentTopic = topic;   
    }

     $scope.showDetails = function(viewName){

     	var title = ""

     	switch(viewName)
     	{

     		case 'learning-in-action':
     				title = 'Learning In Action';
     				break;

     		case 'spaced-repetition':
     				title = 'Spaced Repetition'
     				break;

     		case 'delibrate-practise':
     				title =	 'Delibrate Practise';
     				break;

            case 'level0':
                    title = 'Level 0';
                    break;

            case 'level1':
                    title = 'Level 1';
                    break;

            case 'level2':
                    title = 'Level 2';
                    break;

            case 'level3':
                    title = 'Level 3';
                    break;

     	}


     	var path='/views/web/offerings/' + viewName + '.html';

        $scope.customDialogOptions = {
            templateUrl: path,
            title: title
        };
        $ngBootbox.customDialog($scope.customDialogOptions);

    }
    
}]);
