
makingChampsApp.controller('makingChampsLevelCtrl',['$scope','$sce',function($scope,$sce){

    $scope.Level0Data = {
 
        CourseDuration : '20',
        RegularBatch : $sce.trustAsHtml('Regular batch : Once a week <br/> Summer Vacation batch : 5 days a week '),
        EachSession: '90 minutes',
 
        Skills : {
 
            skill0 : {
                Caption : 'Self Introduction',
                Description : 'Introducing oneself smartly to people whom we meet for the first time',
                ImageName : 'self_intro.jpg'
                },
            skill1 : {
                Caption : 'Speaking On Prepared Topic',
                Description : 'Confidently giving a speech on the content already available.',
                ImageName : 'public_speaking.jpg'
            },
            skill2 : {
                Caption : 'Story Telling',
                Description : 'Telling story in an interesting way, with and without props',
                ImageName : 'story.jpg'
            },
 
            skill3 : {
                Caption : ' Interacting with guests and friends',
                Description : ' Learning how to handle social interaction at home and at somebody’s place',
                ImageName : 'intearacting.jpg'
            },
            skill4 : {
                Caption : ' Etiquettes in telephonic conversation',
                Description : ' Learning the basic rules of telephonic conversation when calling and receiving the call',
                ImageName : 'telephone.jpg'
            }
      
        }
    }

    $scope.Level1Data = {
 
        CourseDuration : '30',
        RegularBatch : $sce.trustAsHtml('Regular batch : Once a week <br/> Summer Vacation batch : 5 days a week '),
        EachSession: '90 minutes',
 
        Skills : {
 
            skill0 : {
                Caption : 'Public speaking',
                Description : 'Confidently speak to a group on a prepared topic as well as impromptu',
                ImageName : 'public_speak.jpg'
                },
            
            skill1 : {
                Caption : ' Problem solving',
                Description : ' How to look at problems and find creative solutions for becoming happy',
                ImageName : 'problem.jpg'
            },
            
            skill2 : {
                Caption : 'Developing Content',
                Description : 'To express our ideas in a most effective way  ',
                ImageName : 'develop.jpg'
            },
            skill3 : {
                Caption : 'Working in a team',
                Description : 'How to work in a team and get good results',
                ImageName : 'team.jpg'
            },
 
            skill4 : {
                Caption : ' Time management',
                Description : ' To get the most out of the given time and do things which are important for us',
                ImageName : 'time_mgt.jpg'
            }
      
        }
    }

    $scope.Level2Data = {
 
        CourseDuration : '30',
        RegularBatch : $sce.trustAsHtml('Regular batch :  Once a week '),
        EachSession: '90 minutes',
 
        Skills : {
 
            skill0 : {
                Caption : 'Goal setting and action planning ',
                Description : 'To set worthwhile goals, break into smaller parts and work in a time bound manner to achieve it ',
                ImageName : 'goal.jpg'
                },
            skill1 : {
                Caption : 'Creativity',
                Description : 'Think laterally, to think of something new – in expressing oneself and for solving some problems ',
                ImageName : 'creativity.jpg'
            },
            skill2 : {
                Caption : ' Handling conflicts and seeking consensus ',
                Description : ' Being able to handle disagreements, solving conflicts with people and convincing others   ',
                ImageName : 'conflicts.jpg'
            },
            skill3 : {
                Caption : ' Effective writing',
                Description : ' Structuring the written message in a way that it serves the purpose for various applications',
                ImageName : 'writing.jpg'
            }
      
        }
    }

    $scope.Level3Data = {
 
        CourseDuration : '30',
        RegularBatch : $sce.trustAsHtml('Regular batch : Once a week '),
        EachSession: '90 minutes',
 
        Skills : {
 
            skill0 : {
                Caption : 'Handling  competition & failures ',
                Description : 'To compete aggressively, in a fair way, handle victories and failures with balance and learn from all these experiences',
                ImageName : 'competition.jpg'
                },
            skill1 : {
                Caption : 'Group Discussion & Debates',
                Description : 'Listen the points of others, make your own points in an assertive way, debate – for and against on a given topic',
                ImageName : 'group.jpg'
            },
            skill2 : {
                Caption : ' Getting work done from others',
                Description : ' Multiply your efforts by taking support from others as well as understand how others will also get work done from you',
                ImageName : 'work.jpg'
            },
            skill3 : {
                Caption : ' Innovation',
                Description : ' To make something useful / valuable by doing something new. Look at the opportunities around and find a solution for the same',
                ImageName : 'innovation.jpg'
            }
      
        }
    }

    $scope.currentSkill = 'skill0';

    $scope.skillDetail =  function(key)
    {
        $scope.currentSkill = key;
    }

}]);