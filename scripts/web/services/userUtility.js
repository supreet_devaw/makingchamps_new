makingChampsApp.service('userUtility', [
    '$q', '$http','$rootScope','apiUtility',
    function($q, $http,$rootScope,apiUtility) {
        // common method for all API Calls

        var userLoggedIn = false;

        this.isUserLoggedIn = function()
        {
             return userLoggedIn;
        }

        this.login = function(userDetails)
        {

              var loginPromise = apiUtility.executeApi('POST','/index.php/api/v1/login',userDetails)
              .then()
              {
                    userLoggedIn = true;  
              }
              
              return loginPromise;         
        }     

        this.establishSession = function()
        {
          
            var getLoggedInUserPromise = apiUtility.executeApi('GET','/index.php/api/v1/getLoggedInUser')
                .then()
                {
                     userLoggedIn = true;
                }
                
                return getLoggedInUserPromise;

        }   

        this.logout = function()
        {
            var logoutPrmoise = apiUtility.executeApi('GET','/index.php/api/v1/logout')
                .then()
                {
                     userLoggedIn = false;
                }

                return logoutPrmoise;
        }

    }
]);