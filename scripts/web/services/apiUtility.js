makingChampsApp.service('apiUtility', [
    '$q', '$http', 'apiRoot', '$rootScope',
    function($q, $http, apiRoot, $rootScope) {
        // common method for all API Calls

        var opts = {
            lines: 12 // The number of lines to draw
            , length: 28 // The length of each line
            , width: 14 // The line thickness
            , radius: 42 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#000' // #rgb or #rrggbb or array of colors
            , opacity: 0.25 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 99999 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        }
        var target = document.getElementById('foo')
        var spinner = new Spinner(opts).spin(target);

        var cache = {};
        this.executeApi = function(method, url, requestData, cacheKey, headersObject) { //cacheKey to store data in the service for futher use.
            var deferred = $q.defer();

            if (cacheKey && cache[cacheKey]) {
                deferred.resolve(cache[cacheKey]);
            } else {
                var spinner = new Spinner().spin(document.getElementById('center'))
                $http({
                    method: method,
                    url: url,
                    headers: headersObject, // it takes object as value
                    data: requestData
                }).success(function(data) {

                    if (cacheKey) cache[cacheKey] = data;
                    deferred.resolve(data);
                }).error(function(data, status) {

                    switch (status) {
                    case 400:
                        //toaster.pop('error', "Bad Request", data);
                        break;

                    case 401:
                        //toaster.pop('error', "Unauthorized", data);
                        break;

                    case 403:
                        //toaster.pop('error', "Forbidden", data);
                        break;

                        /* case 409:
                        toaster.pop('error', "Conflict", data);
                        break;*/
                    }

                    deferred.reject({ errorData: data, statusCode: status });

                }).finally(function () {
                    spinner.stop();
                    // Execute logic independent of success/error
                });
            }

            return deferred.promise;
        };

    }
]);