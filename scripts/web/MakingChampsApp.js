var makingChampsApp = angular.module('makingChampsApp', [
    'ui.router','afkl.lazyImage','angAccordion','iso.directives','ngScrollTo','imagesLoaded','ngBootbox',"angularFileUpload","ngAnimate","ui.date","angular-loading-bar","bootstrapLightbox","djds4rce.angular-socialshare"
]);


makingChampsApp.config(function($locationProvider) {
    $locationProvider.html5Mode(false).hashPrefix('!');
});

makingChampsApp.config(['$stateProvider', '$urlRouterProvider', '$provide','$sceDelegateProvider','$urlMatcherFactoryProvider','LightboxProvider', function ($stateProvider, $urlRouterProvider, $provide,$sceDelegateProvider,$urlMatcherFactory,LightboxProvider) {
    $urlMatcherFactory.caseInsensitive(true);
    $urlMatcherFactory.strictMode(false);

    $provide.value('apiRoot', $('#baseUrl').attr('href'));
//    LightboxProvider.templateUrl = 'demo2-lightbox-modal.html';
    
    LightboxProvider.getImageUrl = function (imageUrl) {
        if(imageUrl.type=='image')
        return '/content/gallery/'+imageUrl.name;
        else LightboxProvider.nextImage()
            
    };

    // set the caption of each image as its text color
    LightboxProvider.getImageCaption = function (imageUrl) {
        return imageUrl.caption;
    };

    LightboxProvider.calculateImageDimensionLimits = function (dimensions) {
        return {
            'maxWidth': dimensions.windowWidth >= 768 ? // default
            dimensions.windowWidth - 92 :
            dimensions.windowWidth - 52,
            'maxHeight': 1600                           // custom
        };
    };

    // the modal height calculation has to be changed since our custom template is
    // taller than the default template
    LightboxProvider.calculateModalDimensions = function (dimensions) {
        var width = Math.max(400, dimensions.imageDisplayWidth + 32);

        if (width >= dimensions.windowWidth - 20 || dimensions.windowWidth < 768) {
            width = 'auto';
        }

        return {
            'width': width,                             // default
            'height': 'auto'                            // custom
        };
    };
    
    //Adding youtube embed url to whitelist
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://www.youtube.com/embed/**'
    ]);


    // For any unmatched url, redirect to /state1
    $urlRouterProvider.when("", "/Home");
    $urlRouterProvider.when("/", "/Home");
    $urlRouterProvider.otherwise("/Home");
    //
    // Now set up the states
    $stateProvider
    .state('Home', {
        url: "/Home",
        templateUrl: "views/web/home.html",
        controller:"makingChampsHomeCtrl",
        data:{ pageTitle: 'Program by IIM Alumni and CA for children' }
    })
    .state('Testimonials', {
        url: "/Testimonials",
        templateUrl: "views/web/testimonals.html",
        controller:"makingChampsTestimonalsCtrl",
        data:{ pageTitle: 'Home' }
    })
    .state('Offerings', {
        url: "/Offerings",
        templateUrl: "views/web/offerings.html",
        controller: "makingChampsOfferingsCtrl",
        data:{ pageTitle: 'Home' }
    })
    .state('Batches', {
        url: "/Batches",
        templateUrl: "views/web/batches.html",
        controller: "makingChampsBatchesCtrl",
        data:{ pageTitle: 'Home' }
    })
    .state('Faqs', {
        url: "/Faqs",
        templateUrl: "views/web/faqs.html",
        data:{ pageTitle: 'Home' }
    })
    .state('Download', {
        url: "/Download",
        templateUrl:"views/web/download.html",
        controller: "makingChampsDownloadsCtrl",
        data:{ pageTitle: 'Home' }
    })
    .state('PhotoGallery', {
        url: "/Gallery",
        templateUrl: 'views/web/photogallery.html',
        controller: "makingChampsGalleryCtrl",
        data:{ pageTitle: 'Home' }
    })
    .state('AboutUs', {
        url: "/AboutUs",
        templateUrl: 'views/web/aboutus.html',
        controller: "makingChmapsAboutCtrl",
        data:{ pageTitle: 'Home' }

    })
    .state('NewsAndEvents', {
        url: "/NewsAndEvents",
        templateUrl: 'views/web/newsAndEvents.html',
        controller: "makingChmapsNewsAndEventsCtrl",
        data:{ pageTitle: 'Home' }

    })
    .state('ContactUs', {
        url: "/ContactUs",
        templateUrl: 'views/web/contactus.html',
        data:{ pageTitle: 'Home' }

    })
    .state('MyAccount', {
        url: "/MyAccount",
        templateUrl: 'views/web/myaccount.html',
        controller: 'makingChampsMyAccountCtrl',
        data:{ pageTitle: 'Home' }
    }).state('Careers', {
        url: "/Careers",
        templateUrl: 'views/web/joinUs.html',
        data:{ pageTitle: 'Careers' }
    })

    .state('Disclaimer', {
        url: "/Disclaimer",
        templateUrl: 'views/web/disclaimer.html',
        data:{ pageTitle: 'Home' }
    });

}])




makingChampsApp.run(['$rootScope','userUtility','$anchorScroll',function($rootScope,userUtility,$anchorScroll){

    userUtility.establishSession()
    .then(function(result){

        if (result) 
        {

            $rootScope.$emit('rootScope:userLoggedIn', result);

        };

    });
    $rootScope.$on("$locationChangeSuccess", function() {
        $anchorScroll();
    });


}]);

makingChampsApp.run(function($FB){
  $FB.init('1681917838706262');
});

angular.module('makingChampsApp').filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});

angular.module('makingChampsApp').directive('repeatDone', function() {
    return function(scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
})

angular.module('makingChampsApp').filter('timeStringToShorTime', function($filter) {
    var angularDateFilter = $filter('date');
    
    
    function timeStringToShorTime(input) {
        var time = input.split(':');  
        var timeString = new Date(); // creates a Date Object using the clients current time
        timeString.setHours  (+time[0]); // set Time accordingly, using implicit type coercion
        timeString.setMinutes( time[1]); // you can pass Number or String, it doesn't matter
        timeString.setSeconds( time[2]);
        return  angularDateFilter(timeString, 'hh:mm a');
    }

    return timeStringToShorTime;
})
