angular
.module('app')
.factory('userUtility', userUtility);
userUtility.$inject = ['$q', '$http', 'apiRoot', '$rootScope','apiUtility','logger'];


function userUtility($q,$http,apiRoot,$rootScope,apiUtility, logger) {
    
    var user={};
    var testimonials={};
    var homework={};
    var enquiries={};
    var loggedIn=false;
    return {
        loginUser: loginUser,
        logoutUser:logoutUser,
        getUser:getUser,
        setUser:setUser,
        isUserLoggedIn:isUserLoggedIn,
        setLoggedInStatus:setLoggedInStatus,
        getNewTestimonials:getNewTestimonials,
        getNewEnquiries:getNewEnquiries,
        getTeacherAcademicDetails:getTeacherAcademicDetails,
        getMyBatches:getMyBatches,
        changePassword:changePassword
    };

    function loginUser(){
    }
    function logoutUser(){
        
        return apiUtility.executeApi('GET','api/v1/adminLogout');
    }
    function getUser(){
        
        return JSON.parse(window.localStorage['user']!=undefined?window.localStorage['user']:{});
    }
    function setUser(user){
        this.setLoggedInStatus(true);
        this.user=user;
        window.localStorage['user']=JSON.stringify(user);
    }
    function isUserLoggedIn(){
        
        return window.localStorage['loggedIn']!=undefined?(window.localStorage['loggedIn']=='false'?false:true):false;
    }
    function getNewTestimonials(){
        return apiUtility.executeApi('GET','api/v1/getNewTestimonials');
    }
    function setLoggedInStatus(status){
        
        window.localStorage['loggedIn']=status;
        this.loggedIn=status;
    }
    
    function getNewEnquiries(){
        
        return apiUtility.executeApi('GET','api/v1/getNewEnquiries');
    }
    
    function getTeacherAcademicDetails(){
        return apiUtility.executeApi('GET','api/v1/getTeacherAcademicDetails');
    }
    
    function getMyBatches(list_type){
        
        return apiUtility.executeApi('GET','api/v1/getMyBatches/'+list_type);
    }
    
    function changePassword(password){
        return apiUtility.executeApi('POST','api/v1/changeAdminPassword/',password);
        
    }
}
