angular
    .module('app')
    .factory('deleteItem', deleteItem);
//userUtility.$inject = ['$q', '$http', 'apiRoot', '$rootScope','apiUtility','logger'];


function deleteItem() {

    var deleteId="";
    return {
        setId: setId,
        getId:getId
    };

    function setId(id){
        deleteId=id;
    }
    function getId(){

        return deleteId;
    }

}
