MakingChampsAdminApp.controller("SchoolCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter,$modal,deleteItem){
    $scope.schoolModel={
        schools:{},
        modalInstance:{}
    };



        $scope.getSchools = function(){
        apiUtility.executeApi('GET','api/v1/getSchools')
        .then(function(result){

            $scope.schoolModel.schools=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageSchools = $scope.filteredSchools.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredSchools = $filter("filter")($scope.schoolModel.schools, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredSchools = $filter("orderBy")($scope.schoolModel.schools, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageSchools = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });
        
        


    }

    $scope.school={
        id:"",
        name:"",
        address:""
    }

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addSchool',
        queueLimit: 1,
        removeAfterUpload: true
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        if(response.success){
            $scope.school={
                id:"",
                name:"",
                address:""
            }
            $scope.add_school.$setPristine();
            logger.logSuccess(response.msg);
            $scope.getSchools();
        } else {
            logger.logError(response.msg);
        }

    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };
    
    $scope.addSchool=function(){

        if($scope.school.id==""){
            uploader.onBeforeUploadItem = function(item) {
                item.formData.push({
                    name: $scope.school.name,
                    address: $scope.school.address
                });
            }
            uploader.uploadAll();
        } else {
            if(uploader.queue.length>0){

                uploader.onBeforeUploadItem = function(item) {
                    item.formData.push({
                        id: $scope.school.id,
                        name: $scope.school.name,
                        address: $scope.school.address
                    });
                }
                uploader.uploadAll();
            } else {

                $scope.updated_school={
                    name: $scope.school.name,
                    address: $scope.school.address
                }
                apiUtility.executeApi('POST','api/v1/updateSchool/'+$scope.school.id,$scope.updated_school)
                    .then(function(data){
                        if(data.success) {

                            $scope.school = {
                                id: "",
                                name: "",
                                address: ""
                            }
                            $scope.add_school.$setPristine();
                            logger.logSuccess("School id  " + data.data.id + " update as follows <br/> name: " + data.data.name + "<br/>address: " + data.data.address);
                        } else {
                            logger.logError(data.msg);
                        }
                    });
            }

        }


    }

    var original="";
    $scope.canSubmit = function() {
        return $scope.add_school.$valid && !angular.equals($scope.school, original)
    }
    
    $scope.editSchool=function(school){
        $scope.school=school;
//        uploader.queue.push('/content/schools/'+school.logo);
//        $scope.school.logo='/content/schools/'+school.logo;
//        $scope.school.id=school.id;
//        $scope.school.name=school.name;
//        console.log(uploader.queue);
    }
    
    $scope.deleteSchool=function(id){
        deleteItem.setId(id);
        var modalInstance
            modalInstance = $modal.open({
           templateUrl:"/views/admin/deleteSchool.html",
           controller: "ModalInstanceCtrl"

       }), modalInstance.result.then(function(id) {
                apiUtility.executeApi('DELETE','api/v1/deleteSchool/'+id)
                    .then(function(data){
                        $scope.schoolModel.schools=(data);
                        $scope.search();
                        logger.logSuccess("Deleted School Successfully");
                    });
        }, function() {
        });
    }

    $scope.deleteSchoolImage=function(id){

        apiUtility.executeApi("DELETE",'api/v1/deleteSchoolImage/'+id)
            .then(function(data){
                if(data.success){
                    $scope.school=data.school;
                    logger.logSuccess(data.msg);
                    $scope.getSchools();
                } else {
                    logger.logError(data.msg);
                }
            })
    }
    


});


MakingChampsAdminApp.controller("ModalInstanceCtrl", ["$scope", "$modalInstance","deleteItem", function($scope, $modalInstance,deleteItem) {
    $scope.id=deleteItem.getId();

    $scope.ok = function() {
        $modalInstance.close($scope.id)
     };

    $scope.cancel = function() {
         $modalInstance.dismiss("cancel")
    }
}])


