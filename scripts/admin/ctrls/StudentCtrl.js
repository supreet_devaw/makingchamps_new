MakingChampsAdminApp.controller("StudentCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter,$route,$location,$modal,deleteItem){
    $scope.studentModel={
        all_students:{},
        student:{
            details:{
                id:"",
                name:"",
                user_name:"",
                batch_id:"",
                parent_id:"",
                school_id:"",
                address:"",
                dob:"",
                class:"",
                admit_date:""
            },
            parent:{},
            batch:{},
            teacher:{},
            school:{},
            option:'',
            id:''
        },
        schools:{},
        parents:{},
        batches:{}

    };

    var original="";
    $scope.canSubmitStudentForm = function() {
        return true;//scope.add_student.$valid && !angular.equals($scope.studentModel.student.details, original)
    }

    if($route.current.params.id!=undefined){
        $scope.studentModel.student.id=$route.current.params.id;
        if($route.current.params.option.toLowerCase()=='view' || $route.current.params.option.toLowerCase()=='edit'  || $route.current.params.option.toLowerCase()=='add'){
            $scope.studentModel.student.option=$route.current.params.option.toLowerCase();
        } else {
            $location.path('#/404');
        }
        if($route.current.params.option.toLowerCase()=='view' || $route.current.params.option.toLowerCase()=='edit'){
            apiUtility.executeApi('GET','api/v1/getStudent/'+$scope.studentModel.student.id)
            .then(function(data){
                $scope.studentModel.student.details=data;
                $scope.studentModel.student.batch=data.batch;
                $scope.studentModel.student.parent=data.parents;
                $scope.studentModel.student.school=data.school;
                $scope.studentModel.student.teacher=data.teacher;
                // console.log(data);
            });
            if($route.current.params.option.toLowerCase()=='edit'){
                apiUtility.executeApi('GET','api/v1/getAllBatches')
                .then(function(data){
                    $scope.studentModel.batches=data;
                });
                apiUtility.executeApi('GET','api/v1/getSchools')
                .then(function(data){
                    $scope.studentModel.schools=data;
                });
                apiUtility.executeApi('GET','api/v1/getParents')
                .then(function(data){
                    $scope.studentModel.parents=data;
                });
            }
        }

    } else {
        if($route.current.$$route.originalPath.toLowerCase()=='/students/add'){
            $scope.studentModel.student.option='add';
            apiUtility.executeApi('GET','api/v1/getAllBatches')
            .then(function(data){
                $scope.studentModel.batches=data;
            });
            apiUtility.executeApi('GET','api/v1/getSchools')
            .then(function(data){
                $scope.studentModel.schools=data;
            });
            apiUtility.executeApi('GET','api/v1/getParents')
            .then(function(data){
                $scope.studentModel.parents=data;
            });
        }
    }

    $scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "shortDate"]; $scope.format = $scope.formats[0];
    $scope.today = function() {
        return $scope.dt = new Date
    };
    $scope.today();
    $scope.showWeeks = !0;
    $scope.toggleWeeks = function() {
        return $scope.showWeeks = !$scope.showWeeks
    }
    $scope.clear = function() {
        return $scope.dt = null
    },
        $scope.disabled = function(date, mode) {
        return "day" === mode && (0 === date.getDay() || 6 === date.getDay())
    }
    $scope.toggleMin = function() {
        var _ref;
        return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
            "null": new Date
        }
    }
    $scope.toggleMin(), $scope.open = function($event) {
        return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
    }    
    $scope.dateOptions = {
        "year-format": "'yy'",
        "starting-day": 1
    }
    $scope.getStudents = function(){

        $scope.studentModel.student={
            details:{},
            parent:{},
            batch:{},
            option:'',
            teacher:{},
            id:''
        };
        apiUtility.executeApi('GET','api/v1/getStudents')
        .then(function(result){

            $scope.studentModel.all_students=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageStudents = $scope.filteredStudents.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredStudents = $filter("filter")($scope.studentModel.all_students, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredStudents = $filter("orderBy")($scope.studentModel.all_students, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageStudents = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addStudent',
        queueLimit: 1,
        removeAfterUpload: true
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        
        if(response.success){
            
        logger.logSuccess("Student   "+response.student.name+" added successfully ");
        $location.path('students/view/'+response.student.id);
        } else {
            
        logger.logError(data.msg);
        }


    };

  

    $scope.addStudent=function(){
        $scope.student=$scope.studentModel.student.details;
        if(Object.prototype.toString.call($scope.student.admit_date)=='[object Date]'){

            var todayUTC = new Date(Date.UTC($scope.student.admit_date.getFullYear(), $scope.student.admit_date.getMonth(), $scope.student.admit_date.getDate()));
            $scope.student.admit_date=todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
        }

        if(Object.prototype.toString.call($scope.student.dob)=='[object Date]'){

            var todayUTC = new Date(Date.UTC($scope.student.dob.getFullYear(), $scope.student.dob.getMonth(), $scope.student.dob.getDate()));
            $scope.student.dob=todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
        }
        if($scope.studentModel.student.id!=''){

            if (uploader.queue.length > 0) {

                uploader.onBeforeUploadItem = function (item) {
                    item.formData.push({
                        id: $scope.studentModel.student.details.id,
                        name: $scope.studentModel.student.details.name,
                        user_name:$scope.studentModel.student.details.user_name,
                        batch_id:$scope.studentModel.student.details.batch_id,
                        parent_id:$scope.studentModel.student.details.parent_id,
                        school_id: $scope.studentModel.student.details.school_id,
                        address:$scope.studentModel.student.details.address,
                        dob:$scope.studentModel.student.details.dob,
                        class:$scope.studentModel.student.details.class,
                        admit_date:$scope.studentModel.student.details.admit_date,                    });
                }
                uploader.uploadAll();
            } else {
                apiUtility.executeApi('POST', 'api/v1/updateStudent/' + $scope.studentModel.student.id, $scope.student)
                    .then(function (data) {

                        if(data.success) {
                            logger.logSuccess("Student id  " + data.student.id + " updated ");
                            $location.path('students/view/' + data.student.id);
                        } else {
                            logger.logError(data.msg);
                        }
                    });
            }
        } else {
            if(uploader.queue.length){
                
            uploader.onBeforeUploadItem = function(item) {
                item.formData.push({
                    name: $scope.studentModel.student.details.name,
                    user_name:$scope.studentModel.student.details.user_name,
                    batch_id:$scope.studentModel.student.details.batch_id,
                    parent_id:$scope.studentModel.student.details.parent_id,
                    school_id: $scope.studentModel.student.details.school_id,
                    address:$scope.studentModel.student.details.address,
                    dob:$scope.studentModel.student.details.dob,
                    class:$scope.studentModel.student.details.class,
                    admit_date:$scope.studentModel.student.details.admit_date,
                });
            }
            uploader.uploadAll();
                 
            } else {
                apiUtility.executeApi('POST','api/v1/addStudent',$scope.student)
                .then(function(data){
                    
                    if(data.success){
                        
                    logger.logSuccess("Student  "+data.student.name+" is added successfully");
                        $location.path('students/view/'+data.student.id);
                    } else {
                    logger.logError(data.msg);
                        
                    }
                });
            }
        
                }
            }
          $scope.deleteStudent=function(id){

              deleteItem.setId(id);
              var modalInstance
              modalInstance = $modal.open({
                  templateUrl:"/views/admin/student/deleteStudent.html",
                  controller: "ModalInstanceCtrl"

              }), modalInstance.result.then(function(id) {
                  apiUtility.executeApi('DELETE','api/v1/deleteStudent/'+id)
                      .then(function(data){
                          logger.logSuccess("Deleted Student Successfully");

                          if($scope.studentModel.id==undefined){
                              $scope.studentModel.all_students=data;
                              $scope.filteredStudents = $filter("filter")($scope.studentModel.all_students, $scope.searchKeywords);
                              $scope.search();
                          } else {
                              $location.path('students');
                          }

                      });
              }, function() {
              });


            }
    
            $scope.deleteStudentImage= function (id) {
                apiUtility.executeApi("DELETE",'api/v1/deleteStudentImage/'+id)
                    .then(function(data){
                        if(data.success){
                            logger.logSuccess(data.msg);
                            $location.path('students/view/'+data.student.id);
                        } else {
                            logger.logError(data.msg);
                        }
                    })
            }

    });
