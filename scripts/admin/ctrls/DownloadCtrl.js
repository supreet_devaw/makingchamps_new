MakingChampsAdminApp.controller('DownloadCtrl',function($scope,$http,$filter,apiUtility,FileUploader){
    $scope.downloadModel={
        downloadCategory:{},
        downloadFiles:{}
    }
    $scope.searchKeywords = "";
    $scope.filteredCategories = [];
    $scope.filteredFiles = [];
    $scope.row = "";

    $scope.getDownloadCategories = function(){
        apiUtility.executeApi('GET','api/v1/getDownloadCategories')
        .then(function(result){

            $scope.downloadModel.downloadCategory=result;
            
            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageStores = $scope.filteredCategories.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredCategories = $filter("filter")($scope.downloadModel.downloadCategory, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredCategories = $filter("orderBy")($scope.downloadModel.downloadCategory, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageStores = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });

    }
    
    var original;

    $scope.category = {
        id: "",
        name: ""
    }
    $scope.canSubmit = function() {
        return $scope.add_category.$valid && !angular.equals($scope.category, original)
    }

    $scope.editCategory=function(category){
        $scope.category=category;
    }
    $scope.addDownloadCategory=function(){
        apiUtility.executeApi('POST','api/v1/addDownloadCategory',$scope.category)
        .then(function(data){
            if(data.length){
                $scope.downloadModel.downloadCategory=data;
                $scope.search();
                $scope.category = {
                    id: "",
                    name: ""
                }
                $scope.add_category.$setPristine();

            }
        });
    }
      

    $scope.deleteDownloadCategory=function(id){
        apiUtility.executeApi('DELETE','api/v1/deleteDownloadCategory/'+id)
        .then(function(data){
            $scope.downloadModel.downloadCategory=(data);
            $scope.search();
        });
    }
    
    $scope.getDownloadFiles = function(){
        apiUtility.executeApi('GET','api/v1/getDownloadFiles')
        .then(function(result){

            $scope.downloadModel.downloadFiles=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageFiles = $scope.filteredFiles.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredFiles = $filter("filter")($scope.downloadModel.downloadFiles, $scope.searchKeywords); 
                $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredFiles = $filter("orderBy")($scope.downloadModel.downloadFiles, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageFiles = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });

    }
    
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addDonwloadFile',
        queueLimit: 1,
        removeAfterUpload: true
    });

    $scope.file={
        id:"",
        file_caption:"",
        download_categories_id:"",
        access:"",
    }
    $scope.canSubmitFiles = function() {
    
        return $scope.add_file.$valid && !angular.equals($scope.file, original) && (uploader.getNotUploadedItems().length || $scope.file.id!="")
    }

    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
        $scope.downloadModel.downloadFiles=response;
        $scope.file={
            id:"",
            file_caption:"",
            download_categories_id:"",
            access:"",
        }
        $scope.add_file.$setPristine();
        $scope.search();

    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    $scope.addDownloadFile=function(){
        
        if($scope.file.id==""){
        uploader.onBeforeUploadItem = function(item) {
            item.formData.push({
                file_caption: $scope.file.file_caption,
                category_id: $scope.file.download_categories_id,
                access: $scope.file.access,
            });
        }
        uploader.uploadAll();
        } else {
            
            $scope.updated_file={
                file_caption: $scope.file.file_caption,
                category_id: $scope.file.download_categories_id,
                access: $scope.file.access,
            }
            apiUtility.executeApi('POST','api/v1/updateDownloadFile/'+$scope.file.id,$scope.updated_file)
            .then(function(data){
                
                $scope.file={
                    id:"",
                    file_caption:"",
                    download_categories_id:"",
                    access:"",
                }
                $scope.add_file.$setPristine();
                $scope.downloadModel.downloadFiles=(data);
                $scope.search();

            });
        }
       

    }
    
    $scope.deleteDownloadFile=function(id){
        apiUtility.executeApi('DELETE','api/v1/deleteDownloadFile/'+id)
        .then(function(data){
            $scope.downloadModel.downloadFiles=(data);
            $scope.search();

        });

    }
    
    $scope.toggleDownloadFileStatus=function(id){


        apiUtility.executeApi('GET','api/v1/toggleDownloadFileStatus/'+id)
        .then(function(data){
            $scope.downloadModel.downloadFiles=(data);
            $scope.search();
        });


    }
    
    $scope.editFile=function(file){
        $scope.file=file;
    }
        
});