MakingChampsAdminApp.controller('EventCtrl',function($scope,$http,$filter,apiUtility,FileUploader){
    $scope.eventModel={
        events:{}
    }
    $scope.searchKeywords = "";
    $scope.filteredEvents = [];
    $scope.row = "";


    $scope.getEvents = function(){
        apiUtility.executeApi('GET','api/v1/getAllEvents')
        .then(function(result){
            
            $scope.eventModel.events=result;
            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageEvents = $scope.filteredEvents.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredEvents = $filter("filter")($scope.eventModel.events, $scope.searchKeywords); 
                $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredEvents = $filter("orderBy")($scope.eventModel.events, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageEvents = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });

    }

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addEvent',
        queueLimit: 1,
        removeAfterUpload: true
    });

    $scope.event={
        id:"",
        caption:"",
        eventDate:"",
    }
    var original="";
    $scope.canSubmitEvent = function() {

        return $scope.add_event.$valid && !angular.equals($scope.event, original) && (uploader.getNotUploadedItems().length || $scope.event.id!="")
    }

    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
        $scope.eventModel.events=response;
        $scope.event={
            id:"",
            caption:"",
            eventDate:"",
        }
        $scope.add_event.$setPristine();
        $scope.search();

    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    $scope.addEvent=function(){
        if(Object.prototype.toString.call($scope.event.eventDate)=='[object Date]'){

            var todayUTC = new Date(Date.UTC($scope.event.eventDate.getFullYear(), $scope.event.eventDate.getMonth(), $scope.event.eventDate.getDate()));
            $scope.event.eventDate=todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
        }
        
        if($scope.event.id==""){
            uploader.onBeforeUploadItem = function(item) {
                item.formData.push({
                    caption: $scope.event.caption,
                    eventDate: $scope.event.eventDate,
                });
            }
            uploader.uploadAll();
        } else {

            $scope.updated_event={
                caption: $scope.event.caption,
                eventDate: $scope.event.eventDate,
            }
            apiUtility.executeApi('POST','api/v1/updateEvent/'+$scope.event.id,$scope.updated_event)
            .then(function(data){

                $scope.event={
                    id:"",
                    caption:"",
                    eventDate:""
                }
                $scope.add_event.$setPristine();
                $scope.eventModel.evaluate=(data);
                $scope.search();

            });
        }


    }

    $scope.deleteEvent=function(id){
        apiUtility.executeApi('DELETE','api/v1/deleteEvent/'+id)
        .then(function(data){
            $scope.eventModel.events=(data);
            $scope.search();

        });

    }
    $scope.editEvent=function(event){
        $scope.event=event;
    }

});