MakingChampsAdminApp.controller("ParentCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter,$modal,deleteItem){
    $scope.parentModel={
        parent:{}
    };

    $scope.getParents = function(){
        apiUtility.executeApi('GET','api/v1/getParents')
            .then(function(result){

                $scope.parentModel.parent=result;
                // console.log(result);

                $scope.select = function(page) {
                    var end, start;
                    start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageParents = $scope.filteredParents.slice(start, end);
                };
                $scope.onFilterChange = function() {
                    $scope.select(1); $scope.currentPage = 1; $scope.row = "";
                };
                $scope.onNumPerPageChange = function() {
                    $scope.select(1); $scope.currentPage = 1;
                };
                $scope.onOrderChange = function() {
                    $scope.select(1); $scope.currentPage = 1;
                };
                $scope.search = function() {
                    $scope.filteredParents = $filter("filter")($scope.parentModel.parent, $scope.searchKeywords); $scope.onFilterChange();
                };
                $scope.order = function(rowName) {
                    $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredParents = $filter("orderBy")($scope.parentModel.parent, rowName), $scope.onOrderChange()) : void 0
                };
                $scope.numPerPageOpt = [3, 5, 10, 20];
                $scope.numPerPage = $scope.numPerPageOpt[2];
                $scope.currentPage = 1;
                $scope.currentPageParents = [];
                $scope.search();
                $scope.select($scope.currentPage);
            });

    }



    $scope.parent={
        id:"",
        father_name:"",
        father_email:"",
        father_mobile:"",
        father_job:"",
        mother_name:"",
        mother_email:"",
        mother_mobile:"",
        mother_job:""
    }

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addParent',
        queueLimit: 1,
        removeAfterUpload: true
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        if(response.success){
            //$scope.parentModel.parent.push(response.data);
            $scope.parent={
                id:"",
                father_name:"",
                father_email:"",
                father_mobile:"",
                father_job:"",
                mother_name:"",
                mother_email:"",
                mother_mobile:"",
                mother_job:""
            }
            $scope.add_parent.$setPristine();
            $scope.getParents();
            //$scope.filteredMembers = $filter("filter")($scope.parentModel.parent, $scope.searchKeywords);
            logger.logSuccess(response.msg);
        } else {
            logger.logError(response.msg);
        }


    };

    $scope.addParent=function(){

        if($scope.parent.id==""){

            if(uploader.queue.length){

                uploader.onBeforeUploadItem = function(item) {
                    item.formData.push({
                        father_name: $scope.parent.father_name,
                        father_email:$scope.parent.father_email,
                        father_mobile:$scope.parent.father_mobile,
                        father_job:$scope.parent.father_job,
                        mother_name: $scope.parent.mother_name,
                        mother_email:$scope.parent.mother_email,
                        mother_mobile:$scope.parent.mother_mobile,
                        mother_job:$scope.parent.mother_job

                    });
                    // console.log(item);
                }
                uploader.uploadAll();
            } else {

                apiUtility.executeApi('POST', 'api/v1/addParent', $scope.parent)
                    .then(function (data) {
                        if(data.success){
                            //$scope.parentModel.parent.push(data.data);

                            $scope.getParents();
                            $scope.parent = {
                                id: "",
                                father_name: "",
                                father_email: "",
                                father_mobile: "",
                                father_job: "",
                                mother_name: "",
                                mother_email: "",
                                mother_mobile: "",
                                mother_job: ""
                            }

                            $scope.add_parent.$setPristine();
                            $scope.filteredParents = $filter("filter")($scope.parentModel.parent, $scope.searchKeywords);
                            logger.logSuccess(response.msg);

                        } else {
                            logger.logError(data.msg);
                        }

                    });

            }
        } else {
            if (uploader.queue.length > 0) {

                uploader.onBeforeUploadItem = function (item) {
                    item.formData.push({
                        id: $scope.parent.id,
                        father_name: $scope.parent.father_name,
                        father_email: $scope.parent.father_email,
                        father_mobile: $scope.parent.father_mobile,
                        father_job: $scope.parent.father_job,
                        mother_name: $scope.parent.mother_name,
                        mother_email: $scope.parent.mother_email,
                        mother_mobile: $scope.parent.mother_mobile,
                        mother_job: $scope.parent.mother_job
                    });

                    // console.log(item);
                }
                uploader.uploadAll();
            } else {
                $scope.updated_parent = {
                    id: $scope.parent.id,
                    father_name: $scope.parent.father_name,
                    father_email: $scope.parent.father_email,
                    father_mobile: $scope.parent.father_mobile,
                    father_job: $scope.parent.father_job,
                    mother_name: $scope.parent.mother_name,
                    mother_email: $scope.parent.mother_email,
                    mother_mobile: $scope.parent.mother_mobile,
                    mother_job: $scope.parent.mother_job,
                }
                apiUtility.executeApi('POST', 'api/v1/updateParent/' + $scope.parent.id, $scope.updated_parent)
                    .then(function (data) {
                        if(data.success){
                            $scope.getParents();

                            $scope.parent = {
                                id: "",
                                father_name: "",
                                father_email: "",
                                father_mobile: "",
                                father_job: "",
                                mother_name: "",
                                mother_email: "",
                                mother_mobile: "",
                                mother_job: ""
                            }

                            $scope.add_parent.$setPristine();
                            $scope.filteredParents = $filter("filter")($scope.parentModel.parent, $scope.searchKeywords);
                            logger.logSuccess(response.msg);

                        } else {
                            logger.logError(data.msg);
                        }

                    });
            }
        }


    }

    var original="";
    $scope.canSubmit = function() {
        return $scope.add_parent.$valid && !angular.equals($scope.parent, original)
    }

    $scope.editParent=function(parent){
        $scope.parent=parent;
    }

    $scope.deleteParent=function(id){

        deleteItem.setId(id);
        var modalInstance
        modalInstance = $modal.open({
            templateUrl:"/views/admin/deleteParent.html",
            controller: "ModalInstanceCtrl"

        }), modalInstance.result.then(function(id) {

            apiUtility.executeApi('DELETE','api/v1/deleteParent/'+id)
                .then(function(data){
                    $scope.parentModel.parent=data;
                    $scope.search();
                    logger.logSuccess("Deleted Parent Successfully");

                });
        }, function() {
        });
    }
    //
    //


    $scope.deleteParentImage= function (id) {
        apiUtility.executeApi("DELETE",'api/v1/deleteParentImage/'+id)
            .then(function(data){
                if(data.success){
                    $scope.parent=data.parent;
                    logger.logSuccess(data.msg);
                    $scope.getParents();
                } else {
                    logger.logError(data.msg);
                }
            })

    }


});
