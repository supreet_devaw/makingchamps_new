MakingChampsAdminApp.controller("HomePageSliderCtrl",function($scope,$http,FileUploader,apiUtility,logger){
    $scope.HomePageSliderModel={
        SliderImages:{}
    };
    
    apiUtility.executeApi('GET','api/v1/getHomeSlider/inactive')
    .then(function(result){
        $scope.HomePageSliderModel.SliderImages=(result.homeSlider);
    });
    
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addHomePageSliderImages',
        removeAfterUpload: true
    });

    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
//        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
//        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
//        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
//        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
//        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
//        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
//        console.info('onSuccessItem', fileItem, response, status, headers);
        $scope.HomePageSliderModel.SliderImages=(response.homeSlider);
        logger.logSuccess("Added New Image To Slider  Successfully");

    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
//        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
//        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
//        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
//        console.info('onCompleteAll');
    };


    $scope.removeImageHomeSlider=function(id){

        apiUtility.executeApi('DELETE','api/v1/removeHomePageSliderImage/'+id)
        .then(function(data){
            $scope.HomePageSliderModel.SliderImages=(data.homeSlider);
            logger.logSuccess("Deleted Successfully");

        });
    }

    $scope.toggleHomePageSliderImageStatus=function(id){
        
        
        apiUtility.executeApi('GET','api/v1/toggleHomePageSliderImageStatus/'+id)
        .then(function(result){
            $scope.HomePageSliderModel.SliderImages=(result.homeSlider);
            logger.logSuccess("Modified Status Successfully");

        });
        
       
    }

});
