MakingChampsAdminApp.controller("NavCtrl", ["$scope", "taskStorage", "filterFilter","userUtility", function($scope, taskStorage, filterFilter,userUtility) {

    $scope.showNavbar=false;
    if(userUtility.isUserLoggedIn()){

        if(userUtility.getUser().access=='admin')
            $scope.showNavbar=true;
    }
    var tasks;
    return tasks = $scope.tasks = taskStorage.get(), $scope.taskRemainingCount = filterFilter(tasks, {
        completed: !1
    }).length, $scope.$on("taskRemaining:changed", function(event, count) {
        return $scope.taskRemainingCount = count
    })
}]);