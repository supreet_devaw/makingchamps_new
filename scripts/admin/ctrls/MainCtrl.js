MakingChampsAdminApp.controller('MainCtrl',['$scope','$rootScope','$location','apiUtility','$route','$rootScope','userUtility','$modal','logger',function($scope,$rootScope,$location,apiUtility,$route,$rootScope,userUtility,$modal,logger){


    $scope.mainModel={
      newTestimonials:{},
        user:{},
        loggedInUser:false,
        newEnquiries:{}
    };
    if(userUtility.isUserLoggedIn())
    $scope.mainModel.user=userUtility.getUser();
    $scope.loginModel = {};
    $scope.loggedInUser = null;
    $scope.loginCredentialsInvalid;


    $scope.hideTestimonialOnWebsite=function(testimonial){
        var $index=($scope.mainModel.newTestimonials.indexOf(testimonial));

        apiUtility.executeApi('GET','api/v1/updateTestimonialStatus/'+testimonial.id+'/reject').
        then(function(data){
            logger.log("Testimonial will not be shown on website");                                 $scope.mainModel.newTestimonials.splice($index, 1);
        });
    }
    $scope.showTestimonialOnWebsite=function(testimonial){
        var $index=($scope.mainModel.newTestimonials.indexOf(testimonial));

        apiUtility.executeApi('GET','api/v1/updateTestimonialStatus/'+testimonial.id+'/approved').
        then(function(data){
            logger.logSuccess("Testimonial will be shown on website successfully");                                 $scope.mainModel.newTestimonials.splice($index, 1);
        });
    }
    $scope.$on('user-loggedin', function(event, args) {
        if(userUtility.isUserLoggedIn()){
            $scope.mainModel.user=userUtility.getUser();
        }
        
        if(userUtility.isUserLoggedIn() && $scope.mainModel.user.access=='admin'){
            userUtility.getNewTestimonials()
            .then(function(data){
                $scope.mainModel.newTestimonials=data;
            });
            userUtility.getNewEnquiries()
            .then(function(data){
                $scope.mainModel.newEnquiries=data;

            });
        } else {
            $scope.mainModel.newTestimonials={};
            $scope.mainModel.newEnquiries={};
        }
    });
    
    $scope.logOut=function(){     
        userUtility.logoutUser().then(function(data){
            $location.path('/login');
            userUtility.setUser({});
            userUtility.setLoggedInStatus(false);
            $scope.mainModel.user=userUtility.getUser();
            $scope.mainModel.loggedInUser=userUtility.isUserLoggedIn();
        })
        
    }
   
    $rootScope.$on("$locationChangeSuccess",function(event, next, current){
        
        if($rootScope.appInitialized!=undefined && $rootScope.appInitialized!=false){
            
            if($scope.mainModel.user.id=='' || $scope.mainModel.user.id==undefined){
                $scope.mainModel.loggedInUser=userUtility.isUserLoggedIn();
                if(userUtility.isUserLoggedIn())
                $scope.mainModel.user=userUtility.getUser();
        }
        if(!userUtility.isUserLoggedIn() && $location.path()!='/login' && $location.path()!='/forgot'){
            $location.path('/login');
        }
            if($route.current && userUtility.isUserLoggedIn()){

                if($route.current.$$route.access && $scope.mainModel.user.access && $route.current.$$route.access.indexOf('all')==-1)
                    if($route.current.$$route.access.indexOf($scope.mainModel.user.access)==-1){
                $location.path('/unauthorized');
            }
        }
        }
    });
    
    

    

    $scope.changePasswordButtonClick = function() {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "views/admin/changePassword.html",
            controller: "PasswordCtrl"
        });
    }
    
}]);

MakingChampsAdminApp.controller('PasswordCtrl',['$scope','userUtility','$modalInstance',function($scope,userUtility,$modalInstance){
    $scope.passwordModal={
        password:{
            old_password:'',
            new_password:'',
            confirm_password:''
        },
        changePasswordErrorMessage:''
    }
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    }

    $scope.saveNewPassword=function(){
        // console.log($scope.passwordModal);
        if($scope.passwordModal.password.new_password==$scope.passwordModal.password.confirm_password){
            
            userUtility.changePassword($scope.passwordModal.password)
            .then(function(data){
                // console.log(data); 
        });
        } else {
            $scope.passwordModal.changePasswordErrorMessage="New passwords dont match. Please enter again carefully";
        }
        
    }
}]);



MakingChampsAdminApp.controller('forgotCtrl',['$scope','apiUtility',function($scope,apiUtility){
    $scope.forgotModel={
        user_name:"",
        user_type:"admin"

    }
    $scope.ErrorMessage="";
    $scope.sendNewPassword= function () {
        apiUtility.executeApi('POST','/index.php/api/v1/forgot_password',$scope.forgotModel)
            .then(function(result)
            {

                if(result.success){
                    //$ngBootbox.hideAll();
                    //$ngBootbox.alert(result.msg);
                    $scope.ErrorMessage="";
                    $scope.SuccessMsg=result.msg;
                } else {
                    $scope.ErrorMessage=result.msg;
                    $scope.SuccessMsg="";
                }
            });
    }
}]);