MakingChampsAdminApp.controller("BatchesCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter,$route,$location,$modal,deleteItem){
    
    $scope.batchesModel={
        allBatches:{},
        batch:{
            batchInfo:{},
            batchSchedule:{},
            batchStudentsList:{},
            batchTaskStatus:{},
            batchTeacherDetails:{},
        },
        new_batch:{
            id:"",
            caption:"",
            start_date:"",
            end_date:"",
            level_id:"",
            teacher_id:"",
            details:[{
                start_time:"",
                end_time:"",
                day:"",
                location:"",
                batch_code:""
            }]
            
        },
        batch_id:0,
        teachers:{},
        levels:[{
                    id:0,
                    name:"Level 0"
                },{
                    id:1,
                    name:"Level 1"
                },{
                    id:2,
                    name:"Level 2"
                },{
                    id:3,
                    name:"Level 3"
                }],
    }
    
    $scope.mytime = new Date;
//    console.log($scope.mytime.getMinutes());
    $scope.mytime.setMinutes($scope.mytime.getMinutes() - ($scope.mytime.getMinutes()%15));
    for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){        $scope.batchesModel.new_batch.details[i].start_time=$scope.batchesModel.new_batch.details[i].end_time=$scope.mytime;
    }
    $scope.hstep = 1;
    $scope.mstep = 15;
    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };
    $scope.ismeridian = !0;
    $scope.toggleMode = function() {
        return $scope.ismeridian = !$scope.ismeridian
    }

    $scope.AddNewDay=function(){
        var mytime = new Date;
        //    console.log($scope.mytime.getMinutes());
        mytime.setMinutes($scope.mytime.getMinutes() - ($scope.mytime.getMinutes()%15));
        
        var details={
            start_time:mytime,
            end_time:mytime,
            day:"",
            location:"",
            batch_code:""
        };
        $scope.batchesModel.new_batch.details.push(details);
    }
    
    
    $scope.getAllBatches=function(){
        apiUtility.executeApi('GET','api/v1/getAllBatches')
            .then(function(data){
            $scope.batchesModel.allBatches=(data);
//            console.log(data);
            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageBatches = $scope.filteredBatches.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredBatches = $filter("filter")($scope.batchesModel.allBatches, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredBatches = $filter("orderBy")($scope.batchesModel.allBatches, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageBatches = [];
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }
    
    $scope.getBatchDetails=function(){
        $scope.batchesModel.batch_id=$route.current.params.batch_id;

        apiUtility.executeApi('GET','api/v1/getBatchInfo/'+$scope.batchesModel.batch_id)
        .then(function(data){
            
            // console.log(data);
            $scope.batchesModel.batch.batchInfo=(data.batchInfo);
            $scope.batchesModel.batch.batchSchedule=(data.batchSchedule);
            $scope.batchesModel.batch.batchStudentsList=(data.batchStudentsList);
            $scope.batchesModel.batch.batchTaskStatus=(data.batchTaskStatus);
            $scope.batchesModel.batch.batchTeacherDetails=(data.batchTeacherDetails);
        })
    }
    
    $scope.getFormData=function(){
        apiUtility.executeApi('GET','api/v1/getTeamMembers')
        .then(function(result){
            $scope.batchesModel.teachers=result;
        });
        
        if($route.current.originalPath=='/batches/edit/:batch_id'){
            $scope.batchesModel.batch_id=($route.current.params.batch_id);
            apiUtility.executeApi('GET','api/v1/getBatch/'+$scope.batchesModel.batch_id)
            .then(function(result){
                $scope.batchesModel.new_batch=result;
                
                    var new_date=new Date;
                for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){
                    var start_time=$scope.batchesModel.new_batch.details[i].start_time.split(":");
                    new_date.setHours(start_time[0]);
                    new_date.setMinutes(start_time[1]);
                    $scope.batchesModel.new_batch.details[i].start_time=angular.copy(new_date);
                    var end_time=$scope.batchesModel.new_batch.details[i].end_time.split(":");
                    new_date.setHours(end_time[0]);
                    new_date.setMinutes(end_time[1]);
                    $scope.batchesModel.new_batch.details[i].end_time=angular.copy(new_date);

                }
            });
        }
    }
    
    
    $scope.addNewBatch=function(){
        if(Object.prototype.toString.call($scope.batchesModel.new_batch.start_date)=='[object Date]'){

            var todayUTC = new Date(Date.UTC($scope.batchesModel.new_batch.start_date.getFullYear(), $scope.batchesModel.new_batch.start_date.getMonth(), $scope.batchesModel.new_batch.start_date.getDate()));
            $scope.batchesModel.new_batch.start_date=todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
        }
        
        if(Object.prototype.toString.call($scope.batchesModel.new_batch.end_date)=='[object Date]'){

            var todayUTC = new Date(Date.UTC($scope.batchesModel.new_batch.end_date.getFullYear(), $scope.batchesModel.new_batch.end_date.getMonth(), $scope.batchesModel.new_batch.end_date.getDate()));
            $scope.batchesModel.new_batch.end_date=todayUTC.toISOString().slice(0, 10).replace(/-/g, '-');
        }
        
        if($scope.batchesModel.new_batch.id==''){
            for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){
                $scope.batchesModel.new_batch.details[i].start_time=$scope.batchesModel.new_batch.details[i].start_time.getHours()+':'+$scope.batchesModel.new_batch.details[i].start_time.getMinutes()+':00';

                $scope.batchesModel.new_batch.details[i].end_time=$scope.batchesModel.new_batch.details[i].end_time.getHours()+':'+$scope.batchesModel.new_batch.details[i].end_time.getMinutes()+':00';
            }
            apiUtility.executeApi('POST','api/v1/addBatch',$scope.batchesModel.new_batch)
            .then(function(data){
                
                // console.log(data)
                if(data.success==true){
                    
                logger.logSuccess("Added new Batch  "+data.batch.batch_code);
                $location.path('batches/allBatches');
                } else {
                    
                logger.logError(data.msg);
                    var new_date=new Date;
                    for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){
                        var start_time=$scope.batchesModel.new_batch.details[i].start_time.split(":");
                        new_date.setHours(start_time[0]);
                        new_date.setMinutes(start_time[1]);
                        $scope.batchesModel.new_batch.details[i].start_time=angular.copy(new_date);
                        var end_time=$scope.batchesModel.new_batch.details[i].end_time.split(":");
                        new_date.setHours(end_time[0]);
                        new_date.setMinutes(end_time[1]);
                        $scope.batchesModel.new_batch.details[i].end_time=angular.copy(new_date);

                    }
                }

            });
        } else {
            for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){
                $scope.batchesModel.new_batch.details[i].start_time=$scope.batchesModel.new_batch.details[i].start_time.getHours()+':'+$scope.batchesModel.new_batch.details[i].start_time.getMinutes()+':00';

                $scope.batchesModel.new_batch.details[i].end_time=$scope.batchesModel.new_batch.details[i].end_time.getHours()+':'+$scope.batchesModel.new_batch.details[i].end_time.getMinutes()+':00';
            } apiUtility.executeApi('POST','api/v1/updateBatch/'+$scope.batchesModel.new_batch.id,$scope.batchesModel.new_batch)
            .then(function(data){

                if(data.success==true){
                    // console.log(data);
                    logger.logSuccess("Updated Batch  "+data.batch.batch_code);
                    $location.path('batches/allBatches');
                } else {

                    logger.logError(data.msg);
                    var new_date=new Date;
                    for(var i=0;i<$scope.batchesModel.new_batch.details.length;i++){
                        var start_time=$scope.batchesModel.new_batch.details[i].start_time.split(":");
                        new_date.setHours(start_time[0]);
                        new_date.setMinutes(start_time[1]);
                        $scope.batchesModel.new_batch.details[i].start_time=angular.copy(new_date);
                        var end_time=$scope.batchesModel.new_batch.details[i].end_time.split(":");
                        new_date.setHours(end_time[0]);
                        new_date.setMinutes(end_time[1]);
                        $scope.batchesModel.new_batch.details[i].end_time=angular.copy(new_date);

                    }
                }
//                console.log(data);
//                logger.logSuccess("Updated Batch");
//                $location.path('batches/allBatches');

            });
        }
        
    }
    
    $scope.deleteBatch=function(id){

        deleteItem.setId(id);
        var modalInstance
        modalInstance = $modal.open({
            templateUrl:"/views/admin/batches/deleteBatch.html",
            controller: "ModalInstanceCtrl"

        }), modalInstance.result.then(function(id) {

            apiUtility.executeApi('DELETE','api/v1/deleteBatch/'+id)
                .then(function(data){
                    logger.logSuccess("Deleted Batch Successfully");

                    if($scope.batchesModel.batch_id==0){
                        $scope.batchesModel.allBatches=data;
                        $scope.filteredBatches = $filter("filter")($scope.batchesModel.allBatches, $scope.searchKeywords);
                        $scope.search();
                    } else {
                        $location.path('batches/allBatches');
                    }

                });

        }, function() {
        });


    }



    var original="";
    $scope.canSubmit = function() {
        return $scope.add_batch.$valid && !angular.equals($scope.batchesModel.new_batch, original)
    }
});