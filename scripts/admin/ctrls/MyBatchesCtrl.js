MakingChampsAdminApp.controller("MyBatchesCtrl", ["$scope","userUtility","$route","$location","$filter","apiUtility","logger", function($scope,userUtility,$route,$location,$filter,apiUtility,logger) {
    $scope.myBatchesModel={        
        batches:{},
        list_type:'',
        batch:{
            batchInfo:{},
            batchSchedule:{},
            batchStudentsList:{},
            batchHomeworksList:{},
            batchTaskStatus:{}

        },
        batch_id:0,
        homework:{
            id:"",
            homework:"",
            batch_id:0
        },
        in_process:false,
        editKey:0,
        student_id:0,
        student_details:{
            details:{},
            parent_details:{},
            homework:{},
            stars:{},
            studentStarDetails:{}
        },
        overStar:[],
        percent:[]
    }

    $scope.getMyBatches=function(){

        $scope.myBatchesModel.list_type=$route.current.params.list_type.toLowerCase();

        switch ($scope.myBatchesModel.list_type){
            case 'all':
            case'past':
            case 'upcoming':
            case 'ongoing':
                userUtility.getMyBatches($scope.myBatchesModel.list_type)
                .then(function(data){
                    $scope.myBatchesModel.batches=data;

                    $scope.select = function(page) {
                        var end, start;
                        start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageBatches = $scope.filteredBatches.slice(start, end);
                    };
                    $scope.onFilterChange = function() {
                        $scope.select(1); $scope.currentPage = 1; $scope.row = "";
                    };
                    $scope.onNumPerPageChange = function() {
                        $scope.select(1); $scope.currentPage = 1;
                    };
                    $scope.onOrderChange = function() {
                        $scope.select(1); $scope.currentPage = 1;
                    };
                    $scope.search = function() {
                        $scope.filteredBatches = $filter("filter")($scope.myBatchesModel.batches, $scope.searchKeywords); $scope.onFilterChange();
                    };
                    $scope.order = function(rowName) {
                        $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredBatches = $filter("orderBy")($scope.myBatchesModel.batches, rowName), $scope.onOrderChange()) : void 0
                    };
                    $scope.numPerPageOpt = [3, 5, 10, 20];
                    $scope.numPerPage = $scope.numPerPageOpt[2];
                    $scope.currentPage = 1;
                    $scope.currentPageBatches = [];
                    $scope.search();
                    $scope.select($scope.currentPage);
                });
                break;

            default:
                $location.path('#/404');
                break;
        }

    }

    $scope.getBatchDetails=function(){
        $scope.myBatchesModel.batch_id=$route.current.params.batch_id;

        apiUtility.executeApi('GET','/index.php/api/v1/getBatchInfo/'+$scope.myBatchesModel.batch_id)
        .then(function(data){
            $scope.myBatchesModel.batch.batchInfo=(data.batchInfo);
            $scope.myBatchesModel.batch.batchSchedule=(data.batchSchedule);
            $scope.myBatchesModel.batch.batchStudentsList=(data.batchStudentsList);
            $scope.myBatchesModel.batch.batchHomeworksList=(data.batchHomeworksList);
            $scope.myBatchesModel.batch.batchTaskStatus=(data.batchTaskStatus);
        })
    }

    $scope.toggleStatus=function(subtask){
        subtask.is_completed=!subtask.is_completed;
        apiUtility.executeApi('GET','/index.php/api/v1/toggleSubtaskCompletionStatus/'+subtask.id+'/'+$scope.myBatchesModel.batch_id)
        .then(function(data){
            // console.log(data);
            if(data.success){
                
                $scope.myBatchesModel.batch.batchTaskStatus=data.data;
            } else {
                // console.log(data.msg)
            }
        })
    }
    $scope.addHomework=function(){
        $scope.myBatchesModel.in_process=true;
        if($scope.myBatchesModel.homework.id==""){
            $scope.myBatchesModel.homework.batch_id=$route.current.params.batch_id;
            apiUtility.executeApi('POST','/index.php/api/v1/generateHomework',$scope.myBatchesModel.homework)
            .then(function(data){
                if(data.success) {
                    logger.logSuccess('Homework added successfully');
                    $scope.myBatchesModel.batch.batchHomeworksList.push($scope.myBatchesModel.homework);
                    $scope.myBatchesModel.homework = {
                        id: "",
                        homework: ""
                    };

                    for(var i=0;i<data.studentIdLists.length;i++){
                        $scope.emailStudentHomework(data.studentIdLists[i]);
                    }
                } else {
                    logger.logError(data.msg);
                }
            })

        } else {

            apiUtility.executeApi('POST','/index.php/api/v1/editHomework/'+$scope.myBatchesModel.homework.id,$scope.myBatchesModel.homework)
            .then(function(data){
                $scope.myBatchesModel.batch.batchHomeworksList[$scope.myBatchesModel.editKey]=data;
                $scope.myBatchesModel.homework={
                    id:"",
                    homework:""
                };
            });
        }
        $scope.add_homework.$setPristine();
        $scope.myBatchesModel.in_process=false;
    }


    $scope.emailStudentHomework=function(studentHomeworkId){
        apiUtility.executeApi('GET','/index.php/api/v1/emailStudentHomework/'+studentHomeworkId)
            .then(function (data) {
                if(data.success){
                    if(data.mother_email!=undefined){
                        if(data.mother_email)
                            logger.logSuccess('Homework Email Send To '+data.data.student.name+' mother');
                        else
                            logger.logError('Homework Email not Send To '+data.data.student.name+' mother');
                    }
                    if(data.father_email!=undefined){
                        if(data.mother_email)
                            logger.logSuccess('Homework Email Send To '+data.data.student.name+' father');
                        else
                            logger.logError('Homework Email not Send To '+data.data.student.name+' father');

                    }


                } else {
                    logger.logError(data.msg);
                }
            })
    }
    $scope.editHomework=function(homework){
        $scope.myBatchesModel.editKey=$scope.myBatchesModel.batch.batchHomeworksList.indexOf(homework);
        $scope.myBatchesModel.homework=angular.copy(homework);
    }

    $scope.deleteHomework=function(homework){
        
        apiUtility.executeApi('DELETE','/index.php/api/v1/deleteHomework/'+homework.id)
            .then(function(data){
                $scope.myBatchesModel.batch.batchHomeworksList.pop(homework);
        })
    }
    
    $scope.canSubmitHomework=function(){
        return ($scope.myBatchesModel.homework.homework.length>10 && !$scope.myBatchesModel.in_process);
    }
    
    $scope.getStudentDetails=function(){
        $scope.myBatchesModel.student_id=$route.current.params.student_id;
        apiUtility.executeApi('GET','/index.php/api/v1/getStudentBatchDetails/'+$scope.myBatchesModel.student_id)
            .then(function(data){
            $scope.myBatchesModel.student_details.details=data.details;
            $scope.myBatchesModel.student_details.parent_details=data.parent_details;
            $scope.myBatchesModel.student_details.stars=data.stars;
            $scope.myBatchesModel.student_details.homework=data.homework;
    //$scope.myBatchesModel.student_details.studentStarDetails=data.studentStarDetails;
            //$scope.myBatchesModel.assing_dummy.student_id=$scope.myBatchesModel.student_details.details.id;
            //$scope.myBatchesModel.assing_dummy.batch_id=$scope.myBatchesModel.student_details.details.batch_id;
            //for(var i=0;i<$scope.myBatchesModel.student_details.studentStarDetails.length;i++){
            //    $scope.myBatchesModel.assing_dummy.star_id=$scope.myBatchesModel.student_details.studentStarDetails[i].id;
            //    $scope.myBatchesModel.assign_star[$scope.myBatchesModel.student_details.studentStarDetails[i].id]= angular.copy($scope.myBatchesModel.assing_dummy);
            //}
        })
    }
    
    $scope.addRating=function(star_id){
        var assign_star={
            'star_id':star_id,
            'student_id':$scope.myBatchesModel.student_id,
            'batch_id':$scope.myBatchesModel.student_details.details.batch_id
        };
        apiUtility.executeApi('post','/index.php/api/v1/addStudentRating',assign_star)
            .then(function(response){
                if(response.success){
                    logger.logSuccess('Rating added successfully');
                    $scope.myBatchesModel.student_details.stars[star_id].ratings.latest_student_rating++;
                    $scope.myBatchesModel.student_details.stars[star_id].ratings.cumulative_student_rating++;
                } else {
                    logger.logError(response.msg);
                }
            });
    }

    $scope.getReviewingHomeworkCount=function(student){
        // console.log(student);
        return 0;
    }

    $scope.max=10;



    $scope.markHomeworkIncomplete=function(homework){
        var homework_status={
            'student_id':$scope.myBatchesModel.student_id,
            'status':'incomplete'
        };
        // console.log(homework);
      apiUtility.executeApi('POST','/index.php/api/v1/updateHomeworkStatus/'+homework.homework_id,homework_status)
          .then(function(data){
                if(data.success)
                    homework.status='incomplete';
              else
                logger.logError(data.msg);
          });
    };

    $scope.markHomeworkComplete=function(homework){
        var homework_status={
            'student_id':$scope.myBatchesModel.student_id,
            'status':'complete'
        };
        
        apiUtility.executeApi('POST','/index.php/api/v1/updateHomeworkStatus/'+homework.homework_id,homework_status)
            .then(function(data){
                if(data.success)
                    homework.status='complete';
                else
                    logger.logError(data.msg);
            });
    };

}]);