MakingChampsAdminApp.controller("TestimonialCtrl",function($scope,$http,apiUtility,logger,$filter,$route,$location){

    $scope.testimonialModel={
        allTestimonials:{},
        testimonial:{
            testimonial:'',
            parent_id:'',
            posted_by:'',
            id:''
        },
        parents:{},
    }

   
$scope.getAllParents=function(){
    apiUtility.executeApi('GET','api/v1/getParents')
        .then(function(data){
        $scope.testimonialModel.parents=(data);
              });
};
    $scope.getAllParents();

    $scope.getAllTestimonials=function(){
        apiUtility.executeApi('GET','api/v1/getAllTestimonials')
        .then(function(data){
            $scope.testimonialModel.allTestimonials=(data);
           $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageTestimonials = $scope.filteredTestimonials.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredTestimonials = $filter("filter")($scope.testimonialModel.allTestimonials, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredTestimonials = $filter("orderBy")($scope.testimonialModel.allTestimonials, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageTestimonials = [];
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }


    
    $scope.editTestimonial=function(testimonial){
        $scope.testimonialModel.testimonial=testimonial;
    }
    $scope.addTestimonial=function(){
        if($scope.testimonialModel.testimonial.id==''){
        
            apiUtility.executeApi('POST','api/v1/addTestimonial',$scope.testimonialModel.testimonial)
            .then(function(data){
                if(data.success==true){

                    logger.logSuccess("Added new testimonial successfully");
                    $scope.testimonialModel.testimonial={
                        testimonial:'',
                        parent_id:'',
                        posted_by:'',
                        id:''
                    };
                    $scope.add_testimonial.$setPristine();
                    $scope.testimonialModel.allTestimonials.push(data.testimonial);
                    $scope.search();
                } else {

                    logger.logError(data.msg);
                }

            });
        } else {
            apiUtility.executeApi('POST','api/v1/updateTestimonial/'+$scope.testimonialModel.testimonial.id,$scope.testimonialModel.testimonial)
            .then(function(data){

                if(data.success==true){
                    logger.logSuccess("Updated Testimonial  ");
                    $scope.testimonialModel.testimonial={
                        testimonial:'',
                            parent_id:'',
                        posted_by:'',
                                id:''
                    };
                    $scope.add_testimonial.$setPristine();
                } else {

                    logger.logError(data.msg);
                }

            });
        }

    }
    
    $scope.hideTestimonialOnWebsite=function(testimonial){
        var $index=($scope.testimonialModel.allTestimonials.indexOf(testimonial));

        apiUtility.executeApi('GET','api/v1/updateTestimonialStatus/'+testimonial.id+'/reject').
        then(function(data){
            logger.log("Testimonial will not be shown on website");  
            $scope.testimonialModel.allTestimonials[$index]=testimonial;
        });
    }
    $scope.showTestimonialOnWebsite=function(testimonial){
        var $index=($scope.testimonialModel.allTestimonials.indexOf(testimonial));

        apiUtility.executeApi('GET','api/v1/updateTestimonialStatus/'+testimonial.id+'/approved').
        then(function(data){
            logger.logSuccess("Testimonial will be shown on website successfully"); 
            $scope.testimonialModel.allTestimonials[$index]=testimonial;
        });
    }

    
    var original="";
    $scope.canSubmit = function() {
        return $scope.add_testimonial.$valid && !angular.equals($scope.testimonialModel.testimonial, original)
    }
});