MakingChampsAdminApp.controller("NewsCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter){
    $scope.newsModel={
        news:{}
    };

    $scope.getNews = function(){
        apiUtility.executeApi('GET','api/v1/getNews/inActive')
        .then(function(result){

            $scope.newsModel.news=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageNews = $scope.filteredNews.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredNews = $filter("filter")($scope.newsModel.news, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredNews = $filter("orderBy")($scope.newsModel.news, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageNews = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }

    $scope.news={
        id:"",
        feed:"",
        link:"",
        status:""
    }

    $scope.addNews=function(){

        if($scope.news.id!=''){
        apiUtility.executeApi('POST','api/v1/updateNews/'+$scope.news.id,$scope.news)
            .then(function(data){
            $scope.news={
                id:"",
                feed:"",
                link:"",
                status:""
            }

                $scope.add_news.$setPristine();
                $scope.filteredNews = $filter("filter")($scope.newsModel.news, $scope.searchKeywords);
                logger.logSuccess("News id  "+data.id+" updated ");

            });
        } else {
            apiUtility.executeApi('POST','api/v1/addNews',$scope.news)
            .then(function(data){
                $scope.news={
                    id:"",
                    feed:"",
                    link:"",
                    status:""
                }
                $scope.newsModel.news.push(data);
                $scope.add_news.$setPristine();
                $scope.search();
                logger.logSuccess("News id  "+data.feed+" added successfully ");

            });
        }
        }


    

    var original="";
    $scope.canSubmit = function() {
        return $scope.add_news.$valid && !angular.equals($scope.news, original)
    }

    $scope.editNews=function(news){
                $scope.news=news;
    }

    $scope.deleteNews=function(id){

        apiUtility.executeApi('DELETE','api/v1/deleteNews/'+id)
        .then(function(data){
            $scope.newsModel.news=data;
            $scope.search();
            logger.logSuccess("Deleted News Successfully");

        });
    }

});
