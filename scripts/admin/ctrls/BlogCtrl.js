MakingChampsAdminApp.controller("BlogCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter){
    $scope.blogModel={
        blogs:{}
    };

    $scope.getBlogs = function(){
        apiUtility.executeApi('GET','api/v1/getBlogs')
        .then(function(result){

            $scope.blogModel.blogs=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageBlogs = $scope.filteredBlogs.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredBlogs = $filter("filter")($scope.blogModel.blogs, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredBlogs = $filter("orderBy")($scope.blogModel.blogs, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageBlogs = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }

    $scope.blog={
        id:"",
        caption:"",
        link:""
    }

    $scope.addBlog=function(){

        if($scope.blog.id!=''){
            apiUtility.executeApi('POST','api/v1/updateBlog/'+$scope.blog.id,$scope.blog)
            .then(function(data){
                $scope.blog={
                    id:"",
                    caption:"",
                    link:""
                }

                $scope.add_blog.$setPristine();
                $scope.filteredMedia = $filter("filter")($scope.blogModel.blogs, $scope.searchKeywords);
                logger.logSuccess("Blog id  "+data.id+" updated ");

            });
        } else {
            apiUtility.executeApi('POST','api/v1/addBlog',$scope.blog)
            .then(function(data){
                $scope.blog={
                    id:"",
                    caption:"",
                    link:""
                }
                $scope.blogModel.blogs.push(data);
                $scope.add_blog.$setPristine();
                $scope.search();
                logger.logSuccess("Blog id  "+data.id+" added successfully ");

            });
        }
    }




    var original="";
    $scope.canSubmit = function() {
        return $scope.add_blog.$valid && !angular.equals($scope.blog, original)
    }

    $scope.editBlog=function(blog){
        $scope.blog=blog;
    }

    $scope.deleteBlog=function(id){

        apiUtility.executeApi('DELETE','api/v1/deleteBlog/'+id)
        .then(function(data){
            $scope.blogModel.blogs=data;
            $scope.search();
            logger.logSuccess("Deleted Blog Successfully");

        });
    }

});
