MakingChampsAdminApp.controller("MediaCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter){
    $scope.mediaModel={
        media:{}
    };

    $scope.getMedia = function(){
        apiUtility.executeApi('GET','api/v1/getAllMedia')
        .then(function(result){

            $scope.mediaModel.media=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageMedia = $scope.filteredMedia.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredMedia = $filter("filter")($scope.mediaModel.media, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredMedia = $filter("orderBy")($scope.mediaModel.media, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageMedia = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }

    $scope.media={
        id:"",
        caption:"",
        link:""
    }

    $scope.addMedia=function(){

        if($scope.media.id!=''){
            apiUtility.executeApi('POST','api/v1/updateMedia/'+$scope.media.id,$scope.media)
            .then(function(data){
                $scope.media={
                    id:"",
                    caption:"",
                    link:""
                }

                $scope.add_media.$setPristine();
                // console.log(data);
                $scope.filteredMedia = $filter("filter")($scope.mediaModel.media, $scope.searchKeywords);
                logger.logSuccess("Media id  "+data.id+" updated ");

            });
        } else {
            apiUtility.executeApi('POST','api/v1/addMedia',$scope.media)
            .then(function(data){
                $scope.media={
                    id:"",
                    caption:"",
                    link:""
                }
                $scope.mediaModel.media.push(data);
                $scope.add_media.$setPristine();
                $scope.search();
                logger.logSuccess("Media id  "+data.id+" added successfully ");

            });
        }
    }




    var original="";
    $scope.canSubmit = function() {
        return $scope.add_media.$valid && !angular.equals($scope.media, original)
    }

    $scope.editMedia=function(media){
        $scope.media=media;
    }

    $scope.deleteMedia=function(id){

        apiUtility.executeApi('DELETE','api/v1/deleteMedia/'+id)
        .then(function(data){
            $scope.mediaModel.media=data;
            $scope.search();
            logger.logSuccess("Deleted Media Successfully");

        });
    }

});
