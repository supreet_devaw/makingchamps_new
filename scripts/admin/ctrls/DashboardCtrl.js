MakingChampsAdminApp.controller("DashboardCtrl", ["$scope","userUtility","$modal", function($scope,userUtility,$modal) {
    $scope.dashboardModel={
        teacher:{},
        isTeacherLoggedIn:false,
        teacherCurrentBatchesCount:{
            count:0,
            reviewingHomeworkCount:0
        },
        teacherUpcomingBatchesCount:{
            count:0,
            reviewingHomeworkCount:0
        },
        teacherPastBatchesCount:{
            count:0,
            reviewingHomeworkCount:0
        }

    }

    $scope.dashboardModel.isTeacherLoggedIn=userUtility.isUserLoggedIn();

    if($scope.dashboardModel.isTeacherLoggedIn){

        $scope.dashboardModel.teacher=userUtility.getUser();


        userUtility.getTeacherAcademicDetails()
        .then(function(data){
            $scope.dashboardModel.teacherCurrentBatchesCount=data.teacherCurrentBatchesCount;
            $scope.dashboardModel.teacherUpcomingBatchesCount=data.teacherUpcomingBatchesCount;
            $scope.dashboardModel.teacherPastBatchesCount=data.teacherPastBatchesCount;

        });

    }
    
}]);