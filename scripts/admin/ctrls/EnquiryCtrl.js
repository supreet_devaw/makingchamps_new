MakingChampsAdminApp.controller("EnquiryCtrl",function($scope,$http,apiUtility,logger,$filter,$route){
    $scope.enquiryModel={
        enquiries:{},
        enquiry_id:0,
        enquiry:{}
    };

    $scope.getEnquiries = function(){
        apiUtility.executeApi('GET','/index.php/api/v1/getEnquiries')
        .then(function(result){
// console.log(result);
            $scope.enquiryModel.enquiries=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageEnquiries = $scope.filteredEnquiries.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredEnquiries = $filter("filter")($scope.enquiryModel.enquiries, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredEnquiries = $filter("orderBy")($scope.enquiryModel.enquiries, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageEnquiries = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }

    $scope.getEnquiryDetails=function(){
        $scope.enquiryModel.enquiry_id=$route.current.params.enquiry_id;
        apiUtility.executeApi('GET','/index.php/api/v1/getEnquiryDetails/'+$scope.enquiryModel.enquiry_id)
            .then(function(data){

            $scope.enquiryModel.enquiry=data;
        });
    }

});
