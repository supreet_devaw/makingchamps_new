makingChampsApp.controller('makingChampsMainCtrl',['$scope','$rootScope','$location','userUtility','$ngBootbox','apiUtility',function($scope,$rootScope,$location,userUtility,$ngBootbox,apiUtility){

	$scope.loginModel = {};
	$scope.loggedInUser = null;
    $scope.loginCredentialsInvalid;

    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };


    $scope.loginModel.login = function()
    {
 
        if($scope.loginModel.username == '' || $scope.loginModel.password == '')
        {
            return;
        }

    	userUtility.login({'username':$scope.loginModel.username,'password':$scope.loginModel.password})
    		.then(function(result){

    			console.log(result);

                if(result.id)
                {
                     $scope.loginModel.username = '';
                     $scope.loginModel.password = '';
                     $scope.loggedInUser = result;
                     $scope.loginCredentialsInvalid = false;

                }
                else
                {

                     $scope.loginCredentialsInvalid = true;
                }

    	})

    };

    $scope.submitEnquiry=function(){

        $scope.enquiry.contact = '+91-' + $scope.enquiry.contact;
        
        apiUtility.executeApi('POST','api/v1/enquiry',$scope.enquiry)
        .then(function(result)
              {
        $ngBootbox.hideAll();
            $ngBootbox.alert('Thanks for giving the information. <br/>We will revert within 48 hours.')
            .then(function() {});
        });

    }
    
    var original="";
    $scope.enquiry={
        std: "",
        contact: "",
        dob: new Date(),
        email: "",
        location: "",
        parent_name: "",
        school_name: "",
        student_name: "",
        comments: "",
    }
    $scope.canSubmit = function() {
        return $scope.enquiry_form.$valid && !angular.equals($scope.enquiry, original)
    }
    
    $scope.loginModel.logout = function()
    {


    	userUtility.logout().
    		then(function(){

    			$scope.loggedInUser = null;
    			$scope.$broadcast('scope:userLoggedOut',{});

    		});
    	
    }


	$rootScope.$on('rootScope:userLoggedIn', function (event, data) {
	   
	    $scope.loggedInUser = data;
	    $scope.$broadcast('scope:userLoggedIn',data);

	 });
    
    
    $scope.getEnquiryForm = function(){
        $scope.customDialogOptions = {
            templateUrl: '/views/web/enquiry-form.html',
            title: 'Please fill the form so that we can revert with relevant details.'
        };

        $ngBootbox.customDialog($scope.customDialogOptions);

    }
    
}]);

$(document).scroll(function(){
            if($('body').scrollTop() > 0) {
                $('#header').css('background-color','red');
            } else {
                $('#header').css('background-color','blue');
            }
});