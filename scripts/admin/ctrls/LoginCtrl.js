MakingChampsAdminApp.controller("loginCtrl", ["$scope","apiUtility","$location","$rootScope","userUtility", function($scope,apiUtility,$location,$rootScope,userUtility) {

    var original;
    $scope.error="";
    $scope.inProcess=false;

    $scope.login=function(){
        $scope.error=""
        $scope.inProcess=true;
        apiUtility.executeApi('POST','api/v1/adminLogin',$scope.user)
        .then(function(data){
            if(!data.id){
                $scope.error="Username or Password invalid";
                $scope.inProcess=false;
            } else {

                userUtility.setLoggedInStatus(true);
                userUtility.setUser(data);
                $rootScope.$broadcast('user-loggedin');
                $location.path('/dashboard');
            }
        })
        .catch(function(data){
            // console.log(data);
            if(data.statusCode==500){
                $scope.error="The Server Replied to failed please contact the support team";
                $scope.inProcess=false;
            }
        });
    }
    return $scope.user = {
        username: "",
        password: ""
    }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user), $scope.revert = function() {
        return $scope.user = angular.copy(original), $scope.form_signin.$setPristine()
    }, $scope.canRevert = function() {
        return !angular.equals($scope.user, original) || !$scope.form_signin.$pristine
    }, $scope.canSubmit = function() {
        return $scope.form_signin.$valid && !angular.equals($scope.user, original)
    }, $scope.submitForm = function() {
        return $scope.showInfoOnSubmit = !0, $scope.revert()
    }


}]);