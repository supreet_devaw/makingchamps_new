MakingChampsAdminApp.controller('GalleryCtrl',function($scope,$http,$filter,apiUtility,FileUploader,logger,$modal,deleteItem){
    $scope.galleryModel={
        galleryEvents:{},
        galleryPhotos:{}
    }
    $scope.searchKeywords = "";
    $scope.filteredCategories = [];
    $scope.row = "";
    $scope.getGalleryEvents = function(){

        apiUtility.executeApi('GET','api/v1/getGalleryEvents')
        .then(function(data){
            $scope.galleryModel.galleryEvents=data;
            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageEvents = $scope.filteredEvents.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredEvents = $filter("filter")($scope.galleryModel.galleryEvents, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredEvents = $filter("orderBy")($scope.galleryModel.galleryEvents, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageEvents = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });
    }

    var original;

    $scope.event = {
        id:"",
        name: "",
        category:""
    }
    $scope.canSubmit = function() {
        return $scope.add_event.$valid && !angular.equals($scope.event, original)
    }

    $scope.addGalleryEvent=function(){
        apiUtility.executeApi('POST','api/v1/addGaleryEvent',$scope.event)
        .then(function(data){
            if(data.length){
                $scope.galleryModel.galleryEvents=data;
                $scope.search();
                $scope.event = {
                    id:"",
                    name: "",
                    category:""
                }
                $scope.add_event.$setPristine();
                if($scope.event.id!='')
                    logger.logSuccess("Added New Gallery Event Successfully");
                else
                    logger.logSuccess("Updated Gallery Event Successfully");
            }
        });
    }

    $scope.deleteGalleryEvent=function(id){

        deleteItem.setId(id);
        var modalInstance
        modalInstance = $modal.open({
            templateUrl:"/views/admin/gallery/deleteEvent.html",
            controller: "ModalInstanceCtrl"

        }), modalInstance.result.then(function(id) {

            apiUtility.executeApi('DELETE','api/v1/deleteGalleryEvent/'+id)
                .then(function(data){
                    $scope.galleryModel.galleryEvents=(data);
                    $scope.search();
                    logger.logSuccess("Deleted Gallery Event Successfully");

                });

        }, function() {
        });


    }

    $scope.toggleGalleryEventStatus=function(id){
        apiUtility.executeApi('GET','api/v1/toggleGalleryEventStatus/'+id)
        .then(function(data){
            $scope.galleryModel.galleryEvents=(data);
            $scope.search();
            logger.logSuccess("Changed Status of Event Successfully");

        });
    }

    $scope.editEvent=function(event){
        $scope.event=event;
    }




    $scope.getGalleryPhotos = function(){
        apiUtility.executeApi('GET','api/v1/getGalleryPhotos')
        .then(function(result){
            $scope.galleryModel.galleryPhotos=result;

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPagePhotos = $scope.filteredPhotos.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredPhotos = $filter("filter")($scope.galleryModel.galleryPhotos, $scope.searchKeywords); 
                $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredPhotos = $filter("orderBy")($scope.galleryModel.galleryPhotos, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPagePhotos = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });

    }



    $scope.photo={
        id:"",
        caption:"",
        gallery_events_id:"",
        type:"",
        name:"",
        show_on_home_page:0,
    }
    $scope.canSubmitFiles = function() {

        return $scope.add_photo.$valid && !angular.equals($scope.photo, original) && (uploader.getNotUploadedItems().length || $scope.photo.id!="" || $scope.photo.name!=""  )
    }

    // FILTERS
    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addGalleryPhoto',
        queueLimit: 1,
        removeAfterUpload: true
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.log(response);
        if(response!=null && response!='null'){

            $scope.galleryModel.galleryPhotos=response;
            $scope.search();
            $scope.photo={
                id:"",
                caption:"",
                gallery_events_id:"",
                type:"",
                name:"",
                show_on_home_page:0,
            }
            $scope.add_photo.$setPristine();
            
            logger.logSuccess("Added  Gallery item Successfully");
        } else {
            logger.logError(" item not uploaded");

        }


    };


    $scope.addGalleryPhoto=function(){

        if($scope.photo.id==""){

            if($scope.photo.type=='video'){
                apiUtility.executeApi('POST','api/v1/addGalleryPhoto',$scope.photo)
                .then(function(data){
                    $scope.galleryModel.galleryPhotos=data;
                    $scope.search();
                    $scope.photo={
                        id:"",
                        caption:"",
                        gallery_events_id:"",
                        type:"",
                        name:"",
                        show_on_home_page:0,
                    }
                    $scope.add_photo.$setPristine();
                    logger.logSuccess("Added  Gallery item Successfully");

                });
            }
            else {
                uploader.onBeforeUploadItem = function(item) {
                    item.formData.push({
                        caption: $scope.photo.caption,
                        gallery_events_id: $scope.photo.gallery_events_id,
                        show_on_home_page: $scope.photo.show_on_home_page
                    });
                }
                uploader.uploadAll();
            }
        } else {

            $scope.updated_photo={
                caption: $scope.photo.caption,
                gallery_events_id: $scope.photo.gallery_events_id,
                show_on_home_page: $scope.photo.show_on_home_page==false?0:1,
                name: $scope.photo.name,
            }
            apiUtility.executeApi('POST','api/v1/updateGalleryPhoto/'+$scope.photo.id,$scope.updated_photo)
            .then(function(data){

                $scope.photo={
                    id:"",
                    caption:"",
                    gallery_events_id:"",
                    type:"",
                    name:"",
                    show_on_home_page:0,
                }
                $scope.add_photo.$setPristine();
                $scope.galleryModel.galleryPhotos=(data);
                $scope.search();
                logger.logSuccess("Updated info for photo Successfully");

            });
        }


    }

    $scope.editPhoto=function(photo){
        $scope.photo=photo;
    }

    $scope.toggleGalleryPhotoStatus=function(id){


        apiUtility.executeApi('GET','api/v1/toggleGalleryPhotoStatus/'+id)
        .then(function(data){
            $scope.galleryModel.galleryPhotos=(data);
            $scope.search();
            logger.logSuccess("Changed Photo Status Successfully");

        });


    }

    $scope.deleteGalleryPhoto=function(id){
        apiUtility.executeApi('DELETE','api/v1/deleteGalleryPhoto/'+id)
        .then(function(data){
            $scope.galleryModel.galleryPhotos=(data);
            $scope.search();
            logger.logSuccess("Deleted Photo Successfully");

        });

    }
});