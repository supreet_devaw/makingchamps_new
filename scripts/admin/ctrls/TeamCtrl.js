MakingChampsAdminApp.controller("TeamCtrl",function($scope,$http,FileUploader,apiUtility,logger,$filter,$modal,deleteItem){
    $scope.teamModel={
        members:{}
    };

    $scope.getTeamMembers = function(){
        apiUtility.executeApi('GET','api/v1/getAllTeamMembers')
        .then(function(result){

            $scope.teamModel.members=result;
            // console.log(result);

            $scope.select = function(page) {
                var end, start;
                start = (page - 1) * $scope.numPerPage; end = start + $scope.numPerPage; $scope.currentPageMembers = $scope.filteredMembers.slice(start, end);
            };
            $scope.onFilterChange = function() {
                $scope.select(1); $scope.currentPage = 1; $scope.row = "";
            };
            $scope.onNumPerPageChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.onOrderChange = function() {
                $scope.select(1); $scope.currentPage = 1;
            };
            $scope.search = function() {
                $scope.filteredMembers = $filter("filter")($scope.teamModel.members, $scope.searchKeywords); $scope.onFilterChange();
            };
            $scope.order = function(rowName) {
                $scope.row !== rowName ? ($scope.row = rowName,  $scope.filteredMembers = $filter("orderBy")($scope.teamModel.members, rowName), $scope.onOrderChange()) : void 0
            };
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageMembers = [];
            $scope.search();
            $scope.select($scope.currentPage);
        });




    }

    $scope.member={
        id:"",
        name:"",
        email:"",
        mobile:"",
        post:"",
        username:"",
        description:"",
        linkedin:"",
        access:"",
        show_on_web:true
    }

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/v1/addTeamMember',
        queueLimit: 1,
        removeAfterUpload: true
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.log(response);
        $scope.teamModel.members.push(response);
        $scope.member={
            id:"",
            name:"",
            email:"",
            mobile:"",
            post:"",
            username:"",
            description:"",
            linkedin:"",
            access:"",
            show_on_web:true
        }
        $scope.getTeamMembers();
        $scope.add_team_member.$setPristine();
        $scope.filteredMembers = $filter("filter")($scope.teamModel.members, $scope.searchKeywords);
        logger.logSuccess("Memeber  "+response.name+" added successfully ");

    };

    $scope.addTeamMember=function(){

        if($scope.member.id==""){
            uploader.onBeforeUploadItem = function(item) {
                item.formData.push({
                    name: $scope.member.name,
                    email:$scope.member.email,
                    mobile:$scope.member.mobile,
                    post:$scope.member.post,
                    username:$scope.member.username,
                    description:$scope.member.description,
                    linkedin:$scope.member.linkedin,
                    access:$scope.member.access,
                    show_on_web:$scope.member.show_on_web == true ? 1 : 0
                });
            }
            uploader.uploadAll();
        } else {
            if (uploader.queue.length > 0) {

                uploader.onBeforeUploadItem = function (item) {
                    item.formData.push({
                        id: $scope.member.id,
                        name: $scope.member.name,
                        email: $scope.member.email,
                        mobile: $scope.member.mobile,
                        post: $scope.member.post,
                        username: $scope.member.username,
                        description: $scope.member.description,
                        linkedin: $scope.member.linkedin,
                        access: $scope.member.access,
                        show_on_web: $scope.member.show_on_web == true ? 1 : 0
                    });
                }
                uploader.uploadAll();
            } else {
                $scope.updated_member = {
                    name: $scope.member.name,
                    email: $scope.member.email,
                    mobile: $scope.member.mobile,
                    post: $scope.member.post,
                    username: $scope.member.username,
                    description: $scope.member.description,
                    linkedin: $scope.member.linkedin,
                    access: $scope.member.access,
                    show_on_web: $scope.member.show_on_web == true ? 1 : 0
                }

                apiUtility.executeApi('POST', 'api/v1/updateTeamMeber/' + $scope.member.id, $scope.updated_member)
                    .then(function (data) {
                        $scope.member = {
                            id: "",
                            name: "",
                            email: "",
                            mobile: "",
                            post: "",
                            username: "",
                            description: "",
                            linkedin: "",
                            access: "",
                            show_on_web: true
                        }

                        $scope.add_team_member.$setPristine();
                        $scope.filteredMembers = $filter("filter")($scope.teamModel.members, $scope.searchKeywords);
                        logger.logSuccess("Memeber id  " + data.id + " updated ");

                    });
            }
        }
        $scope.getTeamMembers();
    }

    var original="";
    $scope.canSubmit = function() {
        return $scope.add_team_member.$valid && !angular.equals($scope.member, original)
    }

    $scope.editMember=function(member){
        $scope.member=member;
        $scope.member.show_on_web=member.show_on_web == 1
    }

    $scope.deleteTeamMember=function(id){
        deleteItem.setId(id);
        var modalInstance
        modalInstance = $modal.open({
            templateUrl:"/views/admin/deleteTeamMember.html",
            controller: "ModalInstanceCtrl"

        }), modalInstance.result.then(function(id) {
            apiUtility.executeApi('DELETE','api/v1/deleteTeamMember/'+id)
                .then(function(data){
                    $scope.getTeamMembers();
                    $scope.search();
                    logger.logSuccess("Deleted Member Successfully");

                });
        }, function() {
        });


    }
    $scope.deleteTeamMemberImage= function (id) {
            apiUtility.executeApi("DELETE",'api/v1/deleteTeamMemberImage/'+id)
                .then(function(data){
                    if(data.success){
                        $scope.member=data.member;
                        logger.logSuccess(data.msg);
                        $scope.getTeamMembers();
                    } else {
                        logger.logError(data.msg);
                    }
                })

        }



});
