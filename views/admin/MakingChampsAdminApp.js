MakingChampsAdminApp=angular.module("app", ["ngRoute", "ngAnimate", "ui.bootstrap", "easypiechart", "mgo-angular-wizard", "textAngular", "ui.tree", "ngMap", "ngTagsInput", "app.ui.ctrls", "app.ui.directives", "app.ui.services", "app.controllers", "app.directives", "app.form.validation", "app.ui.form.ctrls", "app.ui.form.directives", "app.tables", "app.map", "app.task", "app.localization", "app.chart.ctrls", "app.chart.directives", "app.page.ctrls","angularFileUpload",'angular-loading-bar']).config(["$routeProvider","$provide","$sceDelegateProvider", function($routeProvider,$provide,$sceDelegateProvider) {
    
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://www.youtube.com/embed/**'
    ]);
$provide.value('apiRoot', $('#baseUrl').attr('href'));
    return $routeProvider.when("/", {
        redirectTo: "/dashboard"
    }).when("/dashboard", {
        templateUrl: "views/admin/dashboard.html",
        access: ['admin','teacher']
    }).when("/login", {
        templateUrl: "views/admin/login.html",
    }).when("/home-page-slider", {
        templateUrl: "views/admin/home-page-slider.html",    
        access: ['admin']
    }).when("/schools", {
        templateUrl: "views/admin/schools.html",    
        access: ['admin']
    }).when("/team", {
        templateUrl: "views/admin/team.html",  
        access: ['admin']
    }).when("/parents", {
        templateUrl: "views/admin/parents.html",  
        access: ['admin']
    }).when("/students", {
        templateUrl: "views/admin/student/students.html",  
        access: ['admin']
    }).when("/students/:option/:id", {
        templateUrl: "views/admin/student/student_details.html",  
        access: ['admin']
    }).when("/students/add", {
        templateUrl: "views/admin/student/student_details.html",  
        access: ['admin']
    }).when("/news", {
        templateUrl: "views/admin/news.html",  
        access: ['admin']
    }).when("/media", {
        templateUrl: "views/admin/media.html",  
        access: ['admin']
    }).when("/downloads/download-category", {
        templateUrl: "views/admin/downloads/download-category.html",
        access: ['admin']
    }).when("/downloads/download-files", {
        templateUrl: "views/admin/downloads/download-files.html",
        access: ['admin']
    }).when("/gallery/gallery-events", {
        templateUrl: "views/admin/gallery/gallery-events.html",
        access: ['admin']
    }).when("/gallery/gallery-items", {
        templateUrl: "views/admin/gallery/gallery-items.html",
        access: ['admin']
    }).when("/blogs", {
        templateUrl: "views/admin/blogs.html",
        access: ['admin']
    }).when("/events", {
        templateUrl: "views/admin/events.html",
        access: ['admin']
    }).when("/enquiries", {
        templateUrl: "views/admin/enquiries.html",
        access: ['admin']
    }).when("/testimonials", {
        templateUrl: "views/admin/testimonials.html",
        access: ['admin']
    }).when("/mybatches/:list_type", {
        templateUrl: "views/admin/mybatches/mybatches_list.html",
        access: ['admin','teacher']
    }).when("/mybatches/view/:batch_id", {
        templateUrl: "views/admin/mybatches/view.html",
        access: ['admin','teacher']
    }).when("/mybatches/student_details/:student_id", {
        templateUrl: "views/admin/mybatches/student_details.html",
        access: ['admin','teacher']
    }).when("/batches/allBatches", {
        templateUrl: "views/admin/batches/all.html",
        access: ['admin','teacher']
    }).when("/batches/addBatches", {
        templateUrl: "views/admin/batches/add.html",
        access: ['admin','teacher']
    }).when("/batches/view/:batch_id", {
        templateUrl: "views/admin/batches/view.html",
        access: ['admin','teacher']
    }).when("/batches/edit/:batch_id", {
        templateUrl: "views/admin/batches/add.html",
        access: ['admin','teacher']
    }).when("/404", {
        templateUrl: "views/admin/404.html",
        access: ['all']
    }).when("/unauthorized", {
        templateUrl: "views/admin/unauthorized.html"
    }).otherwise({
        redirectTo: "/404"
    })
}])

.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])
.run(function($rootScope,apiUtility,$location,userUtility){
    $rootScope.appInitialized=false;

    apiUtility.executeApi('GET','/api/v1/isTeamMemberLogin')
        .then(function(data){
        userUtility.setLoggedInStatus(data.isLoggedIn);
        
        if(!userUtility.isUserLoggedIn() && $location.path()!='/login' && $location.path()!='/forgot'){
            $location.path('/login');
        }  else {
            userUtility.setUser(JSON.parse(data.user));
            $rootScope.$broadcast('user-loggedin');
        
            if($location.path()=='/login'){
                $location.path('/dashboard');
            }
        }
        $rootScope.appInitialized=true;

        
        
    })
})