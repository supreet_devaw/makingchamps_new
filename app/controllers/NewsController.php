<?php

class NewsController extends BaseController {

    /**
     * Display a list of All NewsFeed Entries.
     *
     * @return Response
     */
    public function getNews($status='active')
    {
        
        if($status=='inActive'){
          return  NewsFeed::all();
        } else {
            return NewsFeed::where('status','!=','inactive')->orderBy('id', 'DESC')->get();
        }
    }

    /**
     * Store a new News in Database.
     *
     * @return Response
     */
    public function addNews()
    {
	try
	{
	    $myNewsFeedObject = new NewsFeed;
	    $myNewsFeed = $myNewsFeedObject->addNewRequestNewsFeed(Input::all());
	    return $myNewsFeed;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }


    /**
     * Display specific NewsFeed details.
     *
     * @param  int  $id
     * @return Response
     */
    public function getSingleNews($id)
    {
	try
	{
	    $myNewsFeedObject = new NewsFeed;
	    $myNews = $myNewsFeedObject->getNewsFeed($id);
	    return $myNews;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }


    /**
     * Update the specific NewsFeed in Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateNews($id)
    {
	try
	{
	    $myNewsFeedObject = new NewsFeed;
	    $myRequestObject = Input::all();
	    $myNewsFeed = $myNewsFeedObject->updateNewsFeed($id,$myRequestObject);
	    return $myNewsFeed;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }


    /**
     * Delete specific NewsFeed from Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteNews($id)
    {
	try
	{
	    $myNewsFeedObject = new NewsFeed;
	    $myNewsFeed = $myNewsFeedObject->deleteNewsFeed($id);
	    return $this->getNews();
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }
}
