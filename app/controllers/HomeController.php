<?php

class HomeController extends BaseController {

    
    public function getHomeSlider($status='active'){
        
        if($status=='inactive'){
            $data['homeSlider']=HomePageSlider::all();

        } else {
        $data['homeSlider']=HomePageSlider::where('status','active')->get();
        }
        return $data;
    }
    
    
    public function addHomePageSliderImages(){
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=time().$_FILES[ 'file' ][ 'name' ];
            $uploadPath =public_path().'/content/home_page_slider/'.$file_name;
            $status=move_uploaded_file( $tempPath, $uploadPath );
            if($status){
                HomePageSlider::create([ 
                    'image_name' =>$file_name
                ]);  

            }
            $homeSlider=HomePageSlider::all();
            $answer = array( 'answer' => 'File transfer completed' ,'success'=>$status,'homeSlider'=>$homeSlider);
            $json = json_encode( $answer );
            echo $json;
        } else {
            echo 'No files';
        }

    }
    
    public function removeHomePageSliderImage($id){
        $slider = HomePageSlider::find($id);
        File::delete(public_path('/content/home_page_slider/').$slider->image_name);
        $success=$slider->delete();
        $homeSlider=HomePageSlider::all();
        $answer = array( 'success'=>$success,'homeSlider'=>$homeSlider);
        $json = json_encode( $answer );
        echo $json;

    }
    
    public function toggleHomePageSliderImageStatus($id){
        $slider = HomePageSlider::find($id);
        $slider->status=(($slider->status=='active')?'inactive':'active');
        $slider->save();
        $homeSlider=HomePageSlider::all();
        $answer = array( 'success'=>true,'homeSlider'=>$homeSlider);
        $json = json_encode( $answer );
        echo $json;
    }

}
