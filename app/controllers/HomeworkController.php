<?php

class HomeworkController extends BaseController {



    public function addHomework($student_homework_id)
    {
        
            $userJsonArray = Session::get('users');
        $users = json_decode($userJsonArray);
        $user= json_encode($users[0]);
                $response=array();
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=$users[0]->name.'_'.$student_homework_id.'_'.time().'.'.pathinfo($_FILES[ 'file' ][ 'name' ], PATHINFO_EXTENSION);
            $uploadPath =public_path().'/content/student_homework/'.$file_name;
            $response['status']=move_uploaded_file( $tempPath, $uploadPath );
            if($response['status']){
                
                $stud_homework=StudentHomework::find($student_homework_id);
                $stud_homework->status="reviewing";
                $stud_homework->file_name=$file_name;
                $stud_homework->save();
                $response['homework']=$stud_homework;

            }
            return $response;
        }
        
    }



    public function addHomeworkText($student_homework_id){
            $resp=array();
            try{
                $userJsonArray = Session::get('users');
                $users = json_decode($userJsonArray);
                $user= json_encode($users[0]);

                $file_name=$users[0]->name.'_'.$student_homework_id.'_'.time().'.txt';
                $uploadPath =public_path().'/content/student_homework/'.$file_name;

                $file = fopen($uploadPath,"wb");
                if($file){
                    $content =Input::get('text');
                    fwrite($file,$content);
                    fclose($file);
                    $resp['success']=true;
                    $stud_homework=StudentHomework::find($student_homework_id);
                    $stud_homework->status="reviewing";
                    $stud_homework->file_name=$file_name;
                    $stud_homework->save();
                    $resp['homework']=$stud_homework;
                } else {
                    $resp['success']=false;
                    $resp['msg']='unable to upload your homework. No space on server';
                }
            }catch (Exception $ex){
                $resp['success']=false;
                $resp['msg']='unable to upload your homework. Please try again after sometime or contact makingchamps';
                $resp['ex']=$ex->getMessage();
            }
        return $resp;
    }

    public function assignHomework($batch_id, $homework_id)
    {
	try
	{
	    $myStudentObject = new Student;
	    $myStudentHomeworkObject = new StudentHomework;
	    $myBatchStudents = $myStudentObject->getBatchStudentsList($batch_id);
	    $myAssignment = $myStudentHomeworkObject->assignHomework($homework_id, $myBatchStudents);


        return $myAssignment;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success']=false;
	    $result['msg']='Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function generateHomework()
    {
	try
	{
	    $myHomeworkObject = new Homework;
	    $myRequestObject = Input::all();
	    $myHomework = $myHomeworkObject->addNewRequestHomework($myRequestObject);
	    $myAssignment = $this->assignHomework($myRequestObject['batch_id'],$myHomework->id);

        $studentHomeworkList=StudentHomework::where('homework_id',$myHomework->id)->lists('id');
        foreach($studentHomeworkList as $studentHomewor)
            $this->emailStudentHomework($studentHomewor);

	    return $myAssignment;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    $result['test'] = $myHomework;
	    return $result;
	}
    }

    public function editHomework($id)
    {
	try
	{
	    $myHomeworkObject = new Homework;
	    $myRequestObject = Input::all();
	    $myHomework = $myHomeworkObject->editHomework($id, $myRequestObject);
	    return $myHomework;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function deleteHomework($id)
    {
	try
	{
	    $myHomeworkObject = new Homework;
	    $myHomework = $myHomeworkObject->deleteHomework($id);
	    return $myHomework;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }


    public function updateHomeworkStatus($id)
    {
	$result = [];
	try
	{
	    $myStudentHomeworkObject = new StudentHomework;
	    $myRequestObject = Input::all();
	    $myHomework = $myStudentHomeworkObject->updateHomeworkStatus($id, $myRequestObject);
	    return $myHomework;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function changeStudentHomeworkStatus($id)
    {
	$result = [];
	try
	{
	    $myRequestObject = Input::all();
	    $myStudentHomeworkObject = new StudentHomework;
	    $statusChangeResult = $myStudentHomeworkObject->updateHomeworkStatus($id, $myRequestObject);
	    return $statusChangeResult;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public  function  emailStudentHomework($studentHomeworkId){
        $data=array();
 if($_SERVER['REQUEST_METHOD']=="GET"){
        try {


                 Log::info('method=='.$_SERVER['REQUEST_METHOD']);


            $data['data'] = StudentHomework::find($studentHomeworkId);

            if ($data['data']) {
                $data['data']->Student->parents;
                $data['data']->homework->batch->admin;
            }

            if(isset($data['data']['student']['parents']['mother_email']))
                $data['mother_email'] = $this->sendEmail('emails.studentHomework', array('data' => $data['data'], 'to' => 'mother'), /*'supreet.s.sethi@gmail.com'*/ $data['data']['student']['parents']['mother_email'] , $data['data']['student']['parents']['mother_name'], "MakingChamps - New Homework Assigned to    {$data['data']['student']['name']} from  {$data['data']['homework']['batch']['admin']['name']}");
            if(isset($data['data']['student']['parents']['father_email']))
                $data['father_email'] = $this->sendEmail('emails.studentHomework', array('data' => $data['data'], 'to' => 'father'), /*'supreet.s.sethi@gmail.com'*/ $data['data']['student']['parents']['father_email'] , $data['data']['student']['parents']['father_name'], "MakingChamps - New Homework Assigned to    {$data['data']['student']['name']} from  {$data['data']['homework']['batch']['admin']['name']}");
//            return View::make('emails.studentHomework', array('data' => $data['data'], 'to' => 'father'));
            $data['success']=true;
        } catch(Exception $ex){
            $data['success']=false;
            $data['msg']=$ex->getMessage();
        }
        return $data;
    }

    }

}
