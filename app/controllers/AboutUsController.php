<?php

class AboutUsController extends BaseController {



    public function getTeamMembers()
    {
        return Admin::all();
    }
    
    public function getMedia(){
        return Media::orderBy('id', 'DESC')->get();
    }
    
    public function getSchools(){
        return School::all();
    }
    
    public function addSchool(){
       $response=array();
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));

        try {
            if (!isset($_POST['id'])) {


                if (!empty($_FILES)) {
                    $tempPath = $_FILES['file']['tmp_name'];
                    $file_name = time() . $_FILES['file']['name'];
                    $uploadPath = public_path() . '/content/schools/' . $file_name;
                    $status = move_uploaded_file($tempPath, $uploadPath);
                    if ($status) {
                        $school = School::create([
                            'logo' => $file_name,
                            'name' => $_POST['name'],
                            'address' => $_POST['address'],
                        ]);

                    }
                    $response['success']=true;
                    $response['msg']='School '.$school->name.' with id '.$school->id.' added successfully';
                    $response['data']=$school;
                }
            } else {
                return $this->updateSchool($_POST['id']);
            }
        }catch (Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }

        return $response;

    }
    
    public function updateSchool($id){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $response=array();
        try {

            $response=array();

            $school = School::find($id);
            if (!empty($_FILES)) {
                $tempPath = $_FILES['file']['tmp_name'];
                $file_name = time() . $_FILES['file']['name'];
                $uploadPath = public_path() . '/content/schools/' . $file_name;
                $status = move_uploaded_file($tempPath, $uploadPath);
                if ($status) {
                    $school->logo = $file_name;
                    $school->name = $_POST['name'];
                    $school->address = $_POST['address'];
                }
            } else{
                $school->name = $_POST->name;
                $school->address = $_POST->address;
            }
            $school->save();
            $response['success']=true;
            $response['data']=$school->toArray();
            $response['msg']='School '.$school->id.' Updated Successfully';

            return $response;
        }catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }

        return $response;
    }

    public function deleteSchool($id){
        $school = School::find($id);
        File::delete(public_path('/content/schools/').$school->logo);

        $success=$school->delete();

        echo $this->getSchools();
    }


    public function deleteSchoolImage($id){
        $response=array();
        try {
            $school = School::find($id);
            $deleteImage = $this->deleteImage('schools/' . $school->logo);
            if ($deleteImage['success']) {
                $school->logo='';
                $school->save();
                $response=$deleteImage;
                $response['school']=$school;
                $response['msg']='Image Deleted Successfully';
            } else {
                $response=$deleteImage;
            }
        } catch (Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }
        return $response;
    }

}
