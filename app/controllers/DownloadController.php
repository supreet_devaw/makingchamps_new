<?php

class DownloadController extends BaseController {



    public function getDownloads($status='active')
    {

            $downloadCategory = DownloadCategory::all();
        if($status=='inActive'){
            for($i=0;$i<sizeof($downloadCategory);$i++){
                $downloadCategory[$i]['downloadFiles']=$downloadCategory[$i]->downloadFiles()->get();
            }
        } else {
            for($i=0;$i<sizeof($downloadCategory);$i++){
                $downloadCategory[$i]['downloadFiles']=$downloadCategory[$i]->downloadFiles()->where('status','!=','inactive')->get();
            }
        }
        return $downloadCategory;
    }
    
    public function getDownloadCategories(){
       return DownloadCategory::all();

    }
    
    public function addDownloadCategory(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if(isset($_POST->id)  && $_POST->id!="" ){
                    $category = DownloadCategory::find($_POST->id);
            $category->name=$_POST->name;
            $category->save();
        }
         else if(isset($_POST->name)){
            DownloadCategory::create([
                'name'=> $_POST->name
            ]);
        }
        return $this->getDownloadCategories();
    }
    
    public function deleteDownloadCategory($id){
        $category = DownloadCategory::find($id);
        $success=$category->delete();
        
        echo DownloadCategory::all();
    }
    
    public function deleteDownloadFile($id){
        $file = DownloadFiles::find($id);
        File::delete(public_path('/content/downloads/').$file->file_name);

        $success=$file->delete();
        
        echo DownloadFiles::all();
    }
    
    public function getDownloadFiles(){
        $downloadFiles=DownloadFiles::all();
        foreach($downloadFiles as $files)
         $files->downloadCategory;
        return $downloadFiles;
    }
    
    public function addDonwloadFile(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=time().$_FILES[ 'file' ][ 'name' ];
            $uploadPath =public_path().'/content/downloads/'.$file_name;
            $status=move_uploaded_file( $tempPath, $uploadPath );
            if($status){
                DownloadFiles::create([ 
                    'file_name' =>$file_name,
                    'file_caption' =>$_POST['file_caption'],
                    'access' =>$_POST['access'],
                    'download_categories_id' =>$_POST['category_id'],
                ]);  

            }
            Log::info($_POST);
            return $this->getDownloadFiles();
        }
        
        return json_encode($_POST);
    }
    
    
    public function toggleDownloadFileStatus($id){
        $file = DownloadFiles::find($id);
        $file->status=(($file->status=='active')?'inactive':'active');
        $file->save();
        return $this->getDownloadFiles();
    }
    
    
    public function updateDownloadFile($id){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $file = DownloadFiles::find($id);
        $file->file_caption=$_POST->file_caption;
        $file->access=$_POST->access;
        $file->download_categories_id=$_POST->category_id;
        $file->save();
        return $this->getDownloadFiles();
    }
        
}
