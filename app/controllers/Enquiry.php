<?php

class Enquiry extends Eloquent{

    protected $table = 'enquiry';
    protected $fillable = array('contact','dob','email','location','parent_name','school_name','class','student_name','comments');


}