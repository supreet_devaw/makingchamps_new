<?php

class MediaController extends BaseController {

    /**
     * Display a list of All Media Entries.
     *
     * @return Response
     */
    public function getAllMedia()
    {
	try
	{
	    $myMediaObject = new Media;
	    $myAllMedia = $myMediaObject->getAllMedia();
	    return $myAllMedia;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    /**
     * Store a new Media in Database.
     *
     * @return Response
     */
    public function addMedia()
    {
	try
	{
	    $myMediaObject = new Media;
	    $myMedia = $myMediaObject->addNewRequestMedia(Input::all());
	    return $myMedia;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }


    /**
     * Display specific media details.
     *
     * @param  int  $id
     * @return Response
     */
    public function getMedia($id)
    {
	try
	{
	    $myMediaObject = new Media;
	    $myMedia = $myMediaObject->getMedia($id);
	    return $myMedia;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    /**
     * Update the specific meida in Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateMedia($id)
    {
	try
	{
	    $myMediaObject = new Media;
	    $myRequestObject = Input::all();
	    $myMedia = $myMediaObject->updateMedia($id,$myRequestObject);
	    return $myMedia;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }


    /**
     * Delete specific Media from Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteMedia($id)
    {
	try
	{
	    $myMediaObject = new Media;
	    $myMedia = $myMediaObject->deleteMedia($id);
        return $this->getAllMedia();
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }



}
