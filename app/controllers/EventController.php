<?php

class EventController extends BaseController {

    public function getEvents()
    {
        $events=array();
        $events['past']=Events::where('eventDate','<',date("Y-m-d"))->orderBy('id', 'DESC')->get();
        $events['upcoming']=Events::where('eventDate','>=',date("Y-m-d"))->orderBy('id', 'DESC')->get();
        return $events;//Events::get();
    }

    public function getAllEvents()
    {
        return Events::all();
    }

    public function getBlogs(){
       return Blogs::get();
    }
    
    public function addEvent(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=time().$_FILES[ 'file' ][ 'name' ];
            $uploadPath =public_path().'/content/events/'.$file_name;
            $status=move_uploaded_file( $tempPath, $uploadPath );
            if($status){
                Events::create([ 
                    'link' =>$file_name,
                    'caption' =>$_POST['caption'],
                    'eventDate' =>$_POST['eventDate']
                ]);  

            }
            Log::info($_POST);
            return $this->getAllEvents();
        }

        return json_encode($_POST);
    }
    
    public function deleteEvent($id){
        $file = Events::find($id);
        File::delete(public_path('/content/events/').$file->file_name);

        $success=$file->delete();

        echo Events::all();
    }
}
