<?php

class ParentController extends BaseController {

    /**
     * Display a list of All Parent Entries.
     *
     * @return Response
     */
    public function getParents()
    {
        try
        {
            $myParentObject = new Parents;
            $myParents = $myParentObject->getParents();
            return $myParents;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }


    /**
     * Actions before rendering AddParent Form, if any.
     *
     * @return Response
     */
    public function renderAddParent()
    {
        //
    }


    /**
     * Store a new Parent in Database.
     *
     * @return Response
     */
    public function addParent()
    {

        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));

//        return Input::all();
        if(!Input::get('id')) {
            try {

                $response = array();

                $file_name = '';
                if (!empty($_FILES)) {
                    $tempPath = $_FILES['file']['tmp_name'];
                    $file_name = Input::get('father_name') . time() . $_FILES['file']['name'];
                    $uploadPath = public_path() . '/content/parent/' . $file_name;
                    $status = move_uploaded_file($tempPath, $uploadPath);
                    $_POST['image'] = $file_name;

                    $myParentObject = new Parents;
                    $myParentRespObj = $myParentObject->addNewRequestParent($_POST);
                } else {

                    $_POST->image = $file_name;

                    $myParentObject = new Parents;
                    $myParentRespObj = $myParentObject->addNewRequestParent(Input::except('file'));
                }

                if ($myParentRespObj['success']) {

                    $response['success'] = true;
                    $response['data'] = $myParentRespObj['parent'];
                    $response['msg']="Parent id ".Input::get('id')." added Successfully";

                } else {
                    $response['success'] = false;
                    $response['msg'] = $myParentRespObj['msg'];
                }
                return $response;

            } catch (Exception $ex) {
                $response['success'] = false;
                $response['msg'] = $ex->getMessage();
                $response['line'] = $ex->getLine();
                return $response;
            }
        }
        return $this->updateParent($_POST['id']);
    }


    /**
     * Display specific parent details.
     *
     * @param  int  $id
     * @return Response
     */
    public function getParent($id)
    {
        try
        {
            $myParentObject = new Parents;
            $myParent = $myParentObject->getParent($id);
            return $myParent;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }


    /**
     * Actions before rendering UpdateParent form, if any.
     *
     * @param  int  $id
     * @return Response
     */
    public function renderUpdateParent($id)
    {
        //
    }


    /**
     * Update the specific parent in Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateParent($id)
    {


        try
        {
            $response=array();
            $myParent=Parents::find($id);
            if (!empty($_FILES)) {

                $tempPath = $_FILES['file']['tmp_name'];
                $file_name = time() . $_FILES['file']['name'];
                $uploadPath = public_path() . '/content/parent/' . $file_name;
                $status = move_uploaded_file($tempPath, $uploadPath);
                if ($status) {
                    $myParent->image = $file_name;
//                    return $myParent;
                }
            }
            $myParent->father_name=Input::get('father_name');
            $myParent->father_email=Input::get('father_email');
            $myParent->father_mobile=Input::get('father_mobile');
            $myParent->mother_name=Input::get('mother_name');
            $myParent->mother_email=Input::get('mother_email');
            $myParent->mother_mobile=Input::get('mother_mobile');
            $myParent->mother_job=Input::get('mother_job');
            $myParent->father_job=Input::get('father_job');
            $myParent->save();
            $response['msg']="Parent id ".Input::get('id')." updated Successfully";
            $response['success']=true;
            $response['data']=$myParent;

        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex->getMessage();
            $response['line']=$ex->getLine();
        }

        return $response;
    }


    /**
     * Delete specific parent from Database.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteParent($id)
    {
        try
        {
            $myParentObject = new Parents;
            $myParent = $myParentObject->deleteParent($id);
            //		return $myParent;
            //      changed by supreet to return all parents list on admin
            return $this->getParents();
        }
        catch(Exception $ex)
        {
            return false;
        }
    }



    public function  deleteParentImage($id){
        $response=array();
        try {
            $parent = Parents::find($id);
            $deleteImage = $this->deleteImage('parent/' . $parent->image);
            if ($deleteImage['success']) {
                $parent->image='';
                $parent->save();
                $response=$deleteImage;
                $response['parent']=$parent;
                $response['msg']='Image Deleted Successfully';
            } else {
                $response=$deleteImage;
            }
        } catch (Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }
        return $response;
    }

}