<?php

class TestimonialController extends BaseController {



    public function getTestimonials()
    {
        $testimonials=Testimonial::where('status','approved')->get();
        foreach($testimonials as $testimonial){
            $testimonial->Parents;
            $students=$testimonial->Parents->Student;
                        foreach($students as $student){
                            ($student->School);
                        }

        }
        return $testimonials;
    }

    public function getNewTestimonials()
    {
        
        $testimonials=Testimonial::where('status','pending')->get();
        foreach($testimonials as $testimonial){
            $testimonial->Parents;
            $students=$testimonial->Parents->Student;
//            foreach($students as $student){
//                ($student->School);
//            }
            
        }
        return $testimonials;
    }
    
    
    public function getAllTestimonials()
    {
        
        $testimonials=Testimonial::all();
        foreach($testimonials as $testimonial){
            $testimonial->Parents;
            $students=$testimonial->Parents->Student;
//            foreach($students as $student){
//                ($student->School);
//            }
            
        }
        return $testimonials;
    }
    
    public function addTestimonial(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $response=array();
        $response['success']=false;
        
        try{
           $testimonial=Testimonial::create([
               'parent_id'=>$_POST->parent_id,
               'testimonial'=>$_POST->testimonial,
               'posted_by'=>$_POST->posted_by,
           ]);
            $testimonial->Parents;
            $students=$testimonial->Parents->Student;
            $response['testimonial']=$testimonial;
            $response['success']=true;
            
        } catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex;
            
        }
        
        return $response;
        
    }
    
    public function updateTestimonial($id){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $testimonial = Testimonial::find($id);
        $testimonial->parent_id=$_POST->parent_id;
        $testimonial->testimonial=$_POST->testimonial;
        $testimonial->posted_by=$_POST->posted_by;
        if($testimonial->save()){
            
        $response['success']=true;
            $testimonial->Parents;
            $students=$testimonial->Parents->Student;
        }
        else 
        $response['success']=false;
        $response['testimoial']=$testimonial;
        return $response;
    }

}
