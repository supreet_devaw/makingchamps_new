<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}


    public function deleteImage($path){
        $response=array();
        try{
            $response['success']=(File::delete(public_path('content/').$path));
            if(!$response['success'])
                $response['msg'] ='File Not present at path '.public_path('content/').$path;
        } catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();

        }
        return $response;
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /** Common Send Email Function
     *
     *$params
     *1) email view File Path
     *2) data to pass to the view file
     * 3) $email id of user
     * 4) name of the user
     * 5) subject of the mail
     */
    public function sendEmail($emailFilePath,$passedData,$receiverEmail,$receiverName,$emailSubject){
        $response=array();

        try{
            Mail::send($emailFilePath,  $passedData, function($message) use ($receiverEmail,$receiverName,$emailSubject) {
                $message->to($receiverEmail, $receiverName)->subject($emailSubject);
            });
            $response['success']=true;
        }catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }

        return $response;
    }

    /** Common Send Email Function
     *
     *$params
     *1) email view File Path
     *2) data to pass to the view file
     * 3) $email id of user
     * 4) name of the user
     * 5) subject of the mail
     */
    public function sendEmailToQueue($emailFilePath,$passedData,$receiverEmail,$receiverName,$emailSubject){
        $response=array();

        try{
            Mail::queue($emailFilePath,  $passedData, function($message) use ($receiverEmail,$receiverName,$emailSubject) {
                $message->to($receiverEmail, $receiverName)->subject($emailSubject);
            });
            $response['success']=true;
        }catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }

        return $response;
    }

}
