<?php

class TeamController extends BaseController {

    public function isTeamMemberLoginException(){
        $user=array();
        $user['isLoggedIn']=false;
        if (Session::has('admin'))
        {
            $userJsonArray = Session::get('admin');
            if(isset($userJsonArray)){
                $user['isLoggedIn']=true;
                //$userArray = json_decode($userJsonArray);
                $user['user'] = $userJsonArray->toArray();
                //$user['user'] = $userArray;
            }

            return $user;
        }
        return $user;
    }


    public function isTeamMemberLogin(){
        $user=array();
        $user['isLoggedIn']=false;
        if (Session::has('admin'))
        {
            $userJsonArray = Session::get('admin');
            if(isset($userJsonArray)){
                $user['isLoggedIn']=true;
                $users = json_decode($userJsonArray);
                $user['user']= json_encode($users[0]);
            }

            return $user;
        }
        return $user;
    }


    public function adminLogout(){
        return Session::forget('admin');
    }


    public function adminLoginBackup(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $user=array();
        $username=$_POST->username;
        $user=Admin::where('username',$username)->get();
        if(!empty($user) && !empty($user[0]) ){
            if((Hash::check($_POST->password, $user[0]->password)))
            {
                Session::put('admin', json_encode($user->toArray()));
                return $user[0]->toArray();
            }

            return $user->toArray();
        }

    }


    public function adminLogin(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $user=array();
        $username=$_POST->username;
        $user=Admin::where('username',$username)->get();
        if(!empty($user) && !empty($user[0]) ){
            if((Hash::check($_POST->password, $user[0]->password)))
            {
                Session::put('admin', $user);
                //Session::put('admin', json_encode($user->toArray()));
                return $user[0]->toArray();
            }
            return $user->toArray();
        }

    }


    public function getTeamMembers()
    {
        return Admin::where('show_on_web','1')->get();
    }


    public function getAllTeamMembers()
    {
        return Admin::all();
    }


    public function addTeamMember(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if (!isset($_POST['id'])) {
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=$_POST['name'].'_'.time().'.'.pathinfo($_FILES[ 'file' ][ 'name' ], PATHINFO_EXTENSION);
            $uploadPath =public_path().'/content/team/'.$file_name;
            $status=move_uploaded_file( $tempPath, $uploadPath );
            if($status) {
                $member = Admin::create([
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'post' => $_POST['post'],
                    'username' => $_POST['username'],
                    'description' => $_POST['description'],
                    'linkedin' => $_POST['linkedin'],
                    'access' => $_POST['access'],
                    'show_on_web' => isset($_POST['show_on_web']) ? $_POST['show_on_web'] : 1,
                    'image' => $file_name,
                    'password' => Hash::make('secret')
                ]);

                return $member;

            }
        }
        } else {
            return $this->updateTeamMeber($_POST['id']);
        }

        return json_encode($_POST);
    }

    public function updateTeamMeber($id){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $member = Admin::find($id);

        if (!empty($_FILES)) {
            $tempPath = $_FILES['file']['tmp_name'];
            $file_name = time() . $_FILES['file']['name'];
            $uploadPath = public_path() . '/content/team/' . $file_name;
            $status = move_uploaded_file($tempPath, $uploadPath);
            if ($status) {
                $member->image = $file_name;
                $member->name=$_POST['name'];
                $member->email=$_POST['email'];
                $member->mobile=$_POST['mobile'];
                $member->post=$_POST['post'];
                $member->username=$_POST['username'];
                $member->description=$_POST['description'];
                $member->linkedin=$_POST['linkedin'];
                $member->access=$_POST['access'];
                $member->show_on_web=$_POST['show_on_web'];
            }
        } else{
            $member->name=$_POST->name;
            $member->email=$_POST->email;
            $member->mobile=$_POST->mobile;
            $member->post=$_POST->post;
            $member->username=$_POST->username;
            $member->description=$_POST->description;
            $member->linkedin=$_POST->linkedin;
            $member->access=$_POST->access;
            $member->show_on_web=$_POST->show_on_web;
        }


        $member->save();
        return $member;
    }




    public function deleteTeamMember($id){
        $member = Admin::find($id);
        File::delete(public_path('/content/team/').$member->image);
        $success=$member->delete();
        echo $this->getTeamMembers();
    }



    public function getMyBatches($list_type=''){
        $batches=array();
        if($list_type=='all' || $list_type=='upcoming' || $list_type=='ongoing' || $list_type=='past'){

            try{
                if (Session::has('admin'))
                {
                    $userJsonArray = Session::get('admin');
                    if(isset($userJsonArray)){
                        $teacherId =    json_decode($userJsonArray)[0]->id;
                        if($list_type=='all'){
                            $batches=Admin::find($teacherId)->Batch;
                        } else if($list_type=='past'){
                            $batches=Admin::find($teacherId)->Batch()->where('end_date', '<', date("Y-m-d", (time())))->get();
                        } else if($list_type=='ongoing'){
                            $batches=Admin::find($teacherId)->Batch()->where('start_date', '<=',date("Y-m-d", (time())))->where('end_date', '>=', date("Y-m-d", (time())))->get();
                        } else if($list_type=='upcoming')
                            $batches=Admin::find($teacherId)->Batch()->where('start_date', '>=',date("Y-m-d", (time())))->get();


                        foreach($batches as $batch){
                            $reviewingHomeworkCount=DB::select('select * from student_homework where student_id in (select id from student where batch_id  = :id) and status="reviewing"', ['id' => $batch->id]);
                            $batch['reviewingHomeworkCount']=sizeof($reviewingHomeworkCount);
                        }

                        Log::error($batches);
                    }
                }
            } catch(Exception $ex){
                return $ex;
            }
        }
        return $batches;

    }
    function getTeacherCurrentBatchesCount(){

        $batches= array();
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches['count']=Admin::find($teacherId)->Batch()->where('start_date', '<=',date("Y-m-d", (time())))->where('end_date', '>=', date("Y-m-d", (time())))->count();
                    $reviewingHomeworkCount=DB::select('select * from student_homework where student_id in (select id from student where batch_id in (select id from batch where teacher_id = :id and start_date <= :start_date and end_date >= :end_date)) and status="reviewing"', ['id' => $teacherId,'end_date'=>date("Y-m-d", (time())),'start_date'=>date("Y-m-d", (time()))]);
                    $batches['reviewingHomeworkCount']=sizeof($reviewingHomeworkCount);
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
            return $ex->getMessage();
        }
               return $batches;
    }

    function getTeacherUpcomingBatchesCount(){

        $batches= array();
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches['count']=Admin::find($teacherId)->Batch()->where('start_date', '>=',date("Y-m-d", (time())))->count();
                    $reviewingHomeworkCount=DB::select('select * from student_homework where student_id in (select id from student where batch_id in (select id from batch where teacher_id = :id and start_date >= :start_date)) and status="reviewing"', ['id' => $teacherId,'start_date'=>date("Y-m-d", (time()))]);
                    $batches['reviewingHomeworkCount']=sizeof($reviewingHomeworkCount);
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $batches;
    }

    function getTeacherPastBatchesCount(){

        $batches= array();
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches['count']=Admin::find($teacherId)->Batch()->where('end_date', '<',date("Y-m-d", (time())))->count();
                    $reviewingHomeworkCount=DB::select('select * from student_homework where student_id in (select id from student where batch_id in (select id from batch where teacher_id = :id and end_date < :end_date)) and status="reviewing"', ['id' => $teacherId,'end_date'=>date("Y-m-d", (time()))]);
                    $batches['reviewingHomeworkCount']=sizeof($reviewingHomeworkCount);
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $batches;
    }

    function getTeacherUnreviewedHomeworksCount(){
        $unreviewdHomework=0;
        try{
            Log::error(Session::get('admin'));
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches=Admin::find($teacherId)->Batch;
                    foreach($batches as $batch){
                        $homeworks=$batch->Homework;
                        foreach($homeworks as $homework){

                            $unreviewdHomework+=$homework->StudentHomework()->where('status', '=', 'reviewing')->count();
                        }
                    }
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $unreviewdHomework;
    }
    function getTeacherIncompleteHomeworksCount(){
        $unreviewdHomework=0;
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches=Admin::find($teacherId)->Batch;
                    foreach($batches as $batch){
                        $homeworks=$batch->Homework;
                        foreach($homeworks as $homework){

                            $unreviewdHomework+=$homework->StudentHomework()->where('status', '=', 'incomplete')->count();
                        }
                    }
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $unreviewdHomework;
    }


    public function getTeacherUnreviewedHomeworks(){
        $unreviewdHomework=array();
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                $myStudentObject = new Student;
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches=Admin::find($teacherId)->Batch;
                    foreach($batches as $batch){
                        $homeworks=$batch->Homework;
                        foreach($homeworks as $homework){
                            $myHomework = $homework->StudentHomework()->where('status', '=', 'reviewing')->get();
                            $myHomeworkArray = json_decode($myHomework);
                            foreach($myHomework as $myArray)
                            {
                                Log::error($myArray->student_id);
                                $myStudentName = $myStudentObject->getStudentName($myArray->student_id);
                                Log::error($myStudentName);
                                $myArray['student_name'] = $myStudentName;
                            }
                            array_push($unreviewdHomework, $myHomework);
                        }
                    }
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $unreviewdHomework;
    }

    public function getTeacherIncompleteHomeworks(){
        $unreviewdHomework= array();
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                $myStudentObject = new Student;
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches=Admin::find($teacherId)->Batch;
                    foreach($batches as $batch){
                        $homeworks=$batch->Homework;
                        foreach($homeworks as $homework){
                            $myHomework = $homework->StudentHomework()->where('status', '=', 'incomplete')->get();
                            $myHomeworkArray = json_decode($myHomework);
                            foreach($myHomework as $myArray)
                            {
                                Log::error($myArray->student_id);
                                $myStudentName = $myStudentObject->getStudentName($myArray->student_id);
                                Log::error($myStudentName);
                                $myArray['student_name'] = $myStudentName;
                            }
                            array_push($unreviewdHomework, $myHomework);
                        }
                    }
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $unreviewdHomework;
    }

    public function getTeacherHomeworks(){
        $unreviewdHomework=0;
        try{
            if (Session::has('admin'))
            {
                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $batches=Admin::find($teacherId)->Batch;
                    foreach($batches as $batch){
                        $homeworks=$batch->Homework;
                        foreach($homeworks as $homework){

                            $unreviewdHomework+=$homework->StudentHomework()->get();
                        }
                    }
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
        }
        return $unreviewdHomework;
    }

    public function getTeacherAcademicDetails(){
        $details=array();
        $details['teacherCurrentBatchesCount']=$this->getTeacherCurrentBatchesCount();
        $details['teacherUpcomingBatchesCount']=$this->getTeacherUpcomingBatchesCount();
        $details['teacherPastBatchesCount']=$this->getTeacherPastBatchesCount();
        return $details;
    }


    public function changeAdminPassword()
    {
        try
        {
            $myRequestObject = Input::all();
            $myAdmin = $this->isTeamMemberLoginException();
            $myAdmin = $myAdmin['user'];
            $myAdminObject = new Admin;
            $passwordChangeResult = $myAdminObject->changePassword($myAdmin[0]['id'], $myRequestObject);
            return $passwordChangeResult;
        }
        catch(Exception $ex)
        {
            $result = [];
            $result['success'] = false;
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            $result['test'] = $ex;
            return $result;
        }
    }

    /**
     * Validate Admin Username
     * Attributes - Username
     * return - boolean isunique status
     * Models used - Admin
     */
    public function validateAdminDetails()
    {
        try
        {
            $myRequestObject = Input::all();
            $myAdminObject = new Admin;
            while(list($key, $value) = each($myRequestObject))
            {
                $funct_name = "validate_" . $key;
                if (isset($myRequestObject[$key]))
                {
                    $myValidateResult = $myAdminObject->$funct_name($value);
                }
            }
            return $myValidateResult;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    /**
     * Update Testimonial Status
     * Attributes - testimonial_id
     * return - testimonial_record
     * Models used - Testimonial
     */
    public function updateTestimonialStatus($id, $status)
    {
        try
        {
            $myTestimonialObject = new Testimonial;
            $myTestimonial = $myTestimonialObject->updateStatus($id, $status);
            return $myTestimonial;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    /**
     * Create Student_Star record
     * Attributes - student_id, Star_id, rating
     * return - response dict
     * Models used - StudentStars
     */
    public function rateStudent()
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myRequestObject = Input::all();
            $myStudentObject = new Student;
            $myStudentBatch = $myStudentObject->getStudentBatch($myRequestObject['student_id']);
            if ($myStudentBatch == "false")
            {
                $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
                return $result;
            }
            $myRequestObject['batch_id'] = (int)$myStudentBatch;
            $myStudentStarObject = new StudentStars;
            $myStudentRating = $myStudentStarObject->rateStudent($myRequestObject);
            return $myStudentRating;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /** 
     * Create Star record by Admin
     * Attributes - DB fields
     * return - Inserted DB Field
     * Models used - Stars
     */
    public function addStar()
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myRequestObject = Input::all();
            $myStarObject = new Stars;
            $myStar = $myStarObject->addNewRequestStar($myRequestObject);
            return $myStar;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /** 
     * Update Star record by Admin
     * Attributes - Id, DB fields
     * return - Updated DB Field
     * Models used - Stars
     */
    public function updateStar($id)
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myRequestObject = Input::all();
            $myStarObject = new Stars;
            $myStar = $myStarObject->updateStar($id, $myRequestObject);
            return $myStar;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /** 
     * Delete Star record by Admin
     * Attributes - Id
     * return - Deleted DB Field
     * Models used - Stars
     */
    public function deleteStar($id)
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myStarObject = new Stars;
            $myStar = $myStarObject->deleteStar($id);
            return $myStar;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /** 
     * Get all Star record
     * Attributes - 
     * return - all Star records
     * Models used - Stars
     */
    public function getStars()
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myStarObject = new Stars;
            $myStars = $myStarObject->getStars();
            return $myStars;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /** 
     * Get a Star record
     * Attributes - Id
     * return - Star record
     * Models used - Stars
     */
    public function getStar($id)
    {
        $result = [];
        $result['success'] = false;
        try
        {
            $myStarObject = new Stars;
            $myStar = $myStarObject->getStar($id);
            return $myStar;
        }
        catch(Exception $ex)
        {
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }





    public function  deleteTeamMemberImage($id){

        $response=array();
        try {
            $teamMember = Admin::find($id);
            $deleteImage = $this->deleteImage('team/' . $teamMember->image);
            if ($deleteImage['success']) {
                $teamMember->image='';
                $teamMember->save();
                $response=$deleteImage;
                $response['member']=$teamMember;
                $response['msg']='Image Deleted Successfully';
            } else {
                $response=$deleteImage;
            }
        } catch (Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }
        return $response;

    }

}
