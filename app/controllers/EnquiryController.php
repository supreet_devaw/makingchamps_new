<?php

class EnquiryController extends BaseController {



    public function enquiry()
    {

        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $response=array();
        $response['success']=false;
        if(isset($_POST->student_name)){
        $response['data']= Enquiry::create([
            'student_name'=>$_POST->student_name,
            'school_name'=>$_POST->school_name,
            'class'=>$_POST->std,
            'contact'=>$_POST->contact,
            'parent_name'=>$_POST->parent_name,
            'dob'=>$_POST->dob,
            'location'=>$_POST->location,
            'email'=>$_POST->email,
            'comments'=>$_POST->comments
        ]);
        $response['success']=true;
            Log::error('im here');
           $response['mailResp']=$this->sendEmail('emails.enquiry',$response,'makingchampsindia@gmail.com',$_POST->parent_name,'New Enquiry From '.$_POST->parent_name.' for student '.$_POST->student_name);
        }
        return ($response);
    }
    
    public function getNewEnquiries(){
        return Enquiry::where('status','new')
            ->get();
    }

    public function getEnquiries(){
        return Enquiry::orderBy('status','asc')->get();
    }
    
    public function getEnquiryDetails($id){
        $enquiry=Enquiry::find($id);
        $enquiry->status='viewed';
        $enquiry->save();
        return $enquiry;
        
    }

}
