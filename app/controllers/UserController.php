<?php

class UserController extends BaseController {

    public function login()
    {
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $user=array();
        
            $user=Student::where('user_name',$_POST->username)->get();
            
        
        if(!empty($user) && !empty($user[0]) ){
            if((Hash::check($_POST->password, $user[0]->password))){
                Session::put('users', json_encode($user->toArray()));
                
                return $user[0]->toArray();
            }
        }

        return $user->toArray();
    }

    public function getLoggedInUser(){
        $user=array();
        if (Session::has('users'))
        {
            $userJsonArray = Session::get('users');
            if(isset($userJsonArray)){
                $users = json_decode($userJsonArray);
                return json_encode($users[0]);
            }

            return $user->toArray();
        }
    }

    public function logout(){
        return Session::forget('users');
    }


    public function getParentStudentDetails(){

        if (Session::has('users')){
            $user= json_decode($this->getLoggedInUser());
            $student_id=$user->id;
            $student=array();
            $student['id']=$student_id;
            $student['details']=Student::findOrFail($student_id);
            $student['parent_details']=Student::findOrFail($student_id)->Parents;
            $student['batch']=Student::findOrFail($student_id)->Batch;
            $student['batch']->BatchDetails;
            $student['levelTaskDetails']=App::make('StudentController')->getBatchLevelTaskStatus($student_id);
            $student['testimonials']=$this->getParentTestimonial($student_id);
            $student['teacher']=Student::findOrFail($student_id)->Batch->Admin;
            $student['homework']=Student::findOrFail($student_id)->StudentHomework;
            for($i=0;$i<sizeof($student['homework']);$i++){
                $student['homework'][$i]->Homework;
            }
            /*
              student star function
            */

            $stars=Stars::all();
            if (date('w', time()) == 1)
                $beginning_of_week = date('Y-m-d h:i:s',strtotime('Today',time()));
            else
                $beginning_of_week =  date('Y-m-d h:i:s',strtotime('last Sunday',time()));
            if(date('w', time()) == 7)
                $end_of_week =  date('Y-m-d h:i:s',strtotime('Today', time()) + 86400);
            else
                $end_of_week =  date('Y-m-d h:i:s',strtotime('next Saturday', time()) + 86400);
            $student['stars']=array();
            foreach($stars as $star){
                $ratingStars=array();
                $ratingStars['latest_student_rating']=StudentStars::where('student_id',$student_id)->where('batch_id',$student['details']['batch_id'])->where('star_id',$star->id)->where('created_at','>=',$beginning_of_week)->where('created_at','<=',$end_of_week)->count();
                $ratingStars['cumulative_student_rating']=StudentStars::where('student_id',$student_id)->where('batch_id',$student['details']['batch_id'])->where('star_id',$star->id)->count();
                $student['stars'][$star->id]['star']=$star;
                $student['stars'][$star->id]['ratings']=$ratingStars;
            }

            return $student;
        }
    }

    public function getParentTestimonial($student_id){
        return Student::findOrFail($student_id)->Parents->Testimonial;
    }

    public function addTestimonial(){
                        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));

        $response=array();
        if (Session::has('users')){
            
            if(isset($_POST->testimonial)){
            $user= json_decode($this->getLoggedInUser());
            $student_id=$user->id;
            $parent_id=Student::find($student_id)->Parents->id;
            $response['testimonial']=Testimonial::create([ 
                'testimonial'=>$_POST->testimonial,
                'parent_id' => $parent_id,

            ]); 
            $response['success']=true;
            } else{
            $response['success']=false;
            $response['msg']='Please insert The Testimonial';
            }
            
        } else {
            $response['success']=false;
            $response['msg']='User Not Logged in';
        }
        return $response;

    }

    public function forgot_password()
    {
        $resp = array();
        try
        {
            $myRequestObject = Input::all();
	    if(empty($myRequestObject['user_name']))
	    {
		$resp['success'] = false;
		$resp['msg'] = 'Please provide user name.';
		return $resp;
	    }
            $userType = ucfirst($myRequestObject['user_type']);
            if($userType=='Student'){
                $myStudentObject = new Student;
                $user_name = $myRequestObject['user_name'];
		$resp = $myStudentObject->forgotPassword($user_name);
	    }
	    else if( $userType == 'Admin' )
	    {
		$myAdminObject = new Admin;
		$user_name = $myRequestObject['user_name'];
		$resp = $myAdminObject->forgotPassword($user_name);
	    }
	    else
	    {
                $resp['success']= false;
                $resp['msg'] = 'User type '.$userType.' not found';
            }
        }
        catch(Exception $ex)
        {
            $resp['called']=true;
            $resp['success']= false;
            $resp['msg'] = $ex->getMessage();
        }
        return $resp;
    }





}
