<?php

class StudentController extends \BaseController {

    /**
	 * Display all the Students.
	 *
	 * @return Response
	 */
    public function getStudents()
    {
        try
        {
            $myStudentObject = new Student;
            $allStudents = $myStudentObject->getStudents();
            return $allStudents;
        }
        catch(Exception $ex)
        {
            return "API Failure.";
        }
    }


    /**
	 * Actions before rendering AddStudent Form, if any.
	 *
	 * @return Response
	 */
    public function renderAddStudent()
    {
        //
    }


    /**
	 * Store a new Student in Database.
	 *
	 * @return Response
	 */
    public function addStudent()
    {
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if (!Input::get('id')) {
            $respsone = array();

            try {
                if (!empty($_FILES)) {
                    $tempPath = $_FILES['file']['tmp_name'];
                    $file_name = Input::get('name') . time() . $_FILES['file']['name'];
                    $uploadPath = public_path() . "\content\student\\" . $file_name;
                    $status = move_uploaded_file($tempPath, $uploadPath);
                    $_POST['user_image'] = $file_name;

                    $myStudentObject = new Student;

                    $myStudent = $myStudentObject->addNewRequestStudent($_POST);
                } else {
                    $myStudentObject = new Student;

                    $myStudent = $myStudentObject->addNewRequestStudent(Input::all());
                }
                if ($myStudent['success']) {

                    $respsone['success'] = true;
                    $respsone['student'] = $myStudent['student'];
                } else {
                    $respsone['success'] = false;
                    $respsone['msg'] = $myStudent['msg'];

                }
                return $respsone;
            } catch (Exception $ex) {
                $respsone['success'] = false;
                $respsone['msg'] = $ex->getMessage();
                return $ex;
            }
        } else {
            return $this->updateStudent($_POST['id']);
        }
    }


    /**
	 * Display specific Student Details.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function getStudent($id)
    {
        try
        {
            $myStudentObject = new Student;
            $student = $myStudentObject->getStudent($id);
            return $student;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }


    /**
	 * Actions before rendering UpdateStudent form, if any.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function renderUpdateStudent($id)
    {
        //
    }


    /**
	 * Update the specific Student.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function updateStudent($id)
    {

        try
        {
            $response=array();
            $myStudent=Student::find($id);
            if (!empty($_FILES)) {
                $tempPath = $_FILES['file']['tmp_name'];
                $file_name = time() . $_FILES['file']['name'];
                $uploadPath = public_path() . '/content/student/' . $file_name;
                $status = move_uploaded_file($tempPath, $uploadPath);
				
				
                if ($status) {
                    $myStudent->user_image = $file_name;
                  
                }
            } 
                $myStudent->name=Input::get('name');
                $myStudent->school_id=Input::get('school_id');
                $myStudent->parent_id=Input::get('parent_id');
                $myStudent->batch_id=Input::get('batch_id');
                $myStudent->class=Input::get('class');
                $myStudent->dob=Input::get('dob');
                $myStudent->admit_date=Input::get('admit_date');
                $myStudent->address=Input::get('address');
            

            $myStudent->save();
            $response['success']=true;
            $response['student']=$myStudent;
            return $response;
        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex->getMessage();
            $response['line']=$ex->getLine();
            return $response;
        }
    }


    /**
	 * Remove the specific Student.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function deleteStudent($id)
    {
        try
        {
            $myStudentObject = new Student;
            $myStudent = $myStudentObject->deleteStudent($id);
            return $this->getStudents();
        }
        catch(Exception $ex)
        {
            return false;
        }
    }
    
    
    
    /*
     * Adding Student Level Task
     * posting used_at & comment & level_task_id
     * replying studentLevelTask Object
     * function addStudentLevelTaskDetails
     *
     */
    public function addStudentLevelTaskDetails(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $response=array();
        $response['success']=false;
        if(isset($_POST->level_task_id)){
            
            $user= json_decode(App::make('UserController')->getLoggedInUser());
            if(isset($user->id)){
                
            $student_id=$user->id;
                $response['data']= StudentTaskStatus::create([ 

                'student_id'=>$student_id,
                'level_task_id'=>$_POST->level_task_id,
                'used_at'=>$_POST->used_at,
                'comment'=>$_POST->comment
            ]);  
            $response['success']=true;
            }
        }
        
        return $response;
    }
    
    

    /**
	 * Get Status of Batch_Level_Tasks.
	 * attributes passed - StudentID
	 * return - Level Task List
	 * Models used - Student, BatchLevelTask, Batch
	 */
    public function getBatchLevelTaskStatus($id)
    {
        try
        {
            $myStudentObject = new Student;
            $myLevelTaskObject = new LevelTask;
            $myStudentAllTasks = $myStudentObject->getBatchLevelTaskStatus($id);
            $myStudentCompletedTaskList = $myStudentObject->getStudentCompletedTaskList($id);
            $myStudentAllTasks = $myLevelTaskObject->generateCompletionMapping($myStudentAllTasks, $myStudentCompletedTaskList,$id);
            return $myStudentAllTasks;
        }
        catch(Exception $ex)
        {
            return $ex;
        }
    }

    /**
     * Change Password for Students
     * Attributes passed - old_password, new_password, confirm_password
     * return - boolean status
     * Models used -
     */
    public function changeStudentPassword()
    {
        try
        {
            $myRequestObject = Input::all();
            $myUserObject = new UserController;
            $myStudent = json_decode($myUserObject->getLoggedInUser());
            $myStudentObject = new Student;
            $passwordChangeResult = $myStudentObject->changePassword($myStudent->id, $myRequestObject);
            return $passwordChangeResult;
        }
        catch(Exception $ex)
        {
            $result = [];
            $result['success'] = false;
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /**
     * Validate Student Username
     * Attributes - Username
     * return - boolean isunique status
     * Models used - Student
     */
    public function validateStudentDetails()
    {
        try
        {
            $myRequestObject = Input::all();
            $myStudentObject = new Student;
            while(list($key, $value) = each($myRequestObject))
            {
                $funct_name = "validate_" . $key;
                if (isset($myRequestObject[$key]))
                {
                    $myValidateResult = $myStudentObject->$funct_name($value);
                }
            }
            return $myValidateResult;
        }
        catch(Exception $ex)
        {
            return $funct_name;
        }
    }

    /**
     * Get Student's Star Details
     * Attributes - Id
     * return - list of student star deetails
     * models used - Student, StudentStars
     */
//    public function getStudentStarDetails($id)
//    {
//        $result = [];
//        try
//        {
//            $myStudentObject = new Student;
//            $myStudentStarsObject = new StudentStars;
//            $myStudentStarDetails = $myStudentStarsObject->getStudentStarDetails($id);
//            return $myStudentStarDetails;
//        }
//        catch(Exception $ex)
//        {
//            $result['success'] = false;
//            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
//            return $result;
//        }
//    }

    
    
    public function getStudentBatchDetails($student_id){
        $response=array();
//        $response['studentStarDetails']=$this->getStudentStarDetails($student_id);
        $response['details']=Student::findOrFail($student_id);
        $response['parent_details']=Student::findOrFail($student_id)->Parents;
        $response['homework']=Student::findOrFail($student_id)->StudentHomework;
        $response['taskList']=$this->getBatchLevelTaskStatus($student_id);
        for($i=0;$i<sizeof($response['homework']);$i++){
            $response['homework'][$i]->Homework;
        }
        $stars=Stars::all();
        if (date('w', time()) == 1)
            $beginning_of_week = date('Y-m-d h:i:s',strtotime('Today',time()));
        else
            $beginning_of_week =  date('Y-m-d h:i:s',strtotime('last Sunday',time()));
        if(date('w', time()) == 7)
            $end_of_week =  date('Y-m-d h:i:s',strtotime('Today', time()) + 86400);
        else
            $end_of_week =  date('Y-m-d h:i:s',strtotime('next Saturday', time()) + 86400);
        $response['stars']=array();
        foreach($stars as $star){
            $ratingStars=array();
            $ratingStars['latest_student_rating']=StudentStars::where('student_id',$student_id)->where('batch_id',$response['details']['batch_id'])->where('star_id',$star->id)->where('created_at','>=',$beginning_of_week)->where('created_at','<=',$end_of_week)->count();
            $ratingStars['cumulative_student_rating']=StudentStars::where('student_id',$student_id)->where('batch_id',$response['details']['batch_id'])->where('star_id',$star->id)->count();
            $response['stars'][$star->id]['star']=$star;
            $response['stars'][$star->id]['ratings']=$ratingStars;
        }
//        for($i=0;$i<sizeof($response['stars']);$i++){
//
//        }
        return $response;
    }

    
    public function addStudentRating(){
        $response=array();
        $response['success']=false;
        try{
            $student_star=new StudentStars;
            $response=$student_star->rateStudent(Input::all());
//            $response['success']=true;
//            $response['data']=$student_star;
////            $response['data']['studentStarDetails']=$this->getStudentStarDetails($student_star->student_id);
//            $response['data']['stars']=Stars::all();
//            for($i=0;$i<sizeof($response['data']['stars']);$i++){
//                $rating=StudentStars::where('student_id',$student_star->student_id)->where('star_id',$response['data']['stars'][$i]->id)->orderBy('created_at','desc')->select('rating')->limit(1)->get();
//                $response['data']['stars'][$i]['latest_student_rating']=null;
//                if(isset($rating[0]))
//                    $response['data']['stars'][$i]['latest_student_rating']=$rating[0]['rating'];
//                $response['data']['stars'][$i]['cumulative_student_rating']=StudentStars::where('student_id',$student_star->student_id)->where('star_id',$response['data']['stars'][$i]->id)->avg('rating');
//            }
            
        }catch(Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }
        return $response;
    }

    public  function deleteStudentImage($id){

        $response=array();
        try {
            $student = Student::find($id);
            $deleteImage = $this->deleteImage('student/' . $student->user_image);
            if ($deleteImage['success']) {
                $student->user_image='';
                $student->save();
                $response=$deleteImage;
                $response['student']=$student;
                $response['msg']='Image Deleted Successfully';
            } else {
                $response=$deleteImage;
            }
        } catch (Exception $ex){
            $response['success']=false;
            $response['msg']=$ex->getMessage();
        }
        return $response;
    }


    
}
