<?php

class BatchesController extends BaseController {



    public function getBatches()
    {

        $batches=array();
        $ongoingBatches=Batch::where('end_date','>=',date('Y-m-d', time()))->where('start_date','<=',date('Y-m-d', time()))->orderBy('level_id','asc')->orderBy('start_date','asc')->get();
        foreach($ongoingBatches as $batch){
            $batch['days']=$batch->BatchDetails()->get();
            $batches['ongoing'][]=$batch;
        }
        $upcomingBatches=Batch::where('start_date','>',date('Y-m-d', time()))->orderBy('start_date','asc')->orderBy('level_id','asc')->get();
        foreach($upcomingBatches as $batch){
            $batch['days']=$batch->BatchDetails()->get();
            $batches['upcoming'][]=$batch;
        }
        return $batches;
    }


    public function getAllBatches(){
        $batches= Batch::all();
        foreach($batches as $batch){
            $batch->Admin;
        }
        return $batches;
    }
    //Batch CRUD Operations

    /**
     * Get a specific Batch.
     *
     */
    public function getBatch($id)
    {
        try
        {
            $myBatchObject = new Batch;
            $myBatch = $myBatchObject->getBatch($id);
            $myBatch->details=BatchDetails::where('batch_id',$myBatch->id)->get();
            return $myBatch;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    
    //toggle batch task status
    
    public function toggleSubtaskCompletionStatus($task_id,$batch_id){
        
        $response=array();
        try{
            $teacher=Batch::select('teacher_id')->find($batch_id);
            $teacher_id=$teacher->teacher_id;
            $auth_user=json_decode(Session::get('admin'));
            if($auth_user[0]->id==$teacher_id){
                
            $response['success']=true;
            $is_task_set=BatchLevelTask::where('level_task_id',$task_id)->where('batch_id',$batch_id)->get();
            if(sizeof($is_task_set)){
                $is_task_set[0]->delete();
            } else {
                BatchLevelTask::create([
                    'level_task_id'=>$task_id,
                    'batch_id'=>$batch_id,
                ]);
            }
            $response['data']=$this->getBatchTaskStatus($batch_id);
            } else {
                $response['success']=false;
                
                $response['msg']='unauthorized access';
            }
        }catch(Exception $e){
            $response['success']=false;
            $response['msg']=$e;
        }
        return $response;
    }
    /**
     * Add a new Batch to Database.
     *
     */
    public function addBatch()
    {

        try
        {
            $response=array();
            $myBatchObject = new Batch;
            $myRequestObject = Input::except('details');
            $myBatchResp = $myBatchObject->addNewRequestBatch($myRequestObject);
            //adding the batch details to database
            if($myBatchResp['success']){
                $myBatch=$myBatchResp['batch'];
                $batch_details = Input::only('details');
                for($i=0;$i<sizeof($batch_details['details']);$i++){
                    $BatchDetailObj= new BatchDetails;
                    $BatchDetailObj->day=$batch_details['details'][$i]['day'];
                    $BatchDetailObj->location=$batch_details['details'][$i]['location'];
                    $BatchDetailObj->start_time=$batch_details['details'][$i]['start_time'];
                    $BatchDetailObj->end_time=$batch_details['details'][$i]['end_time'];
                    $BatchDetailObj->batch_id=$myBatch->id;
                    $BatchDetailObj->save();

                }
                $myBatch->details=BatchDetails::where('batch_id',$myBatch->id)->get();
                $response['batch']=$myBatch;
                $response['success']=true;
                return $response;
            } else {
                $response['msg']=$myBatchResp['msg'];
                $response['success']=false;
                $response['resp']=$myBatchResp;
                return $response;
            }


        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex;
            return $response;
        }

        //        $response['success']=false;
        //        $response['msg']=' batch not added';
        //        return $response;
    }

    /**
     * Update a specific Batch in Database.
     *
     */
    public function updateBatch($id)
    {

        $response=array();
        try
        {
            $myBatchObject = new Batch;
            $myRequestObject = Input::except('details');
            $myBatch = $myBatchObject->updateBatch($id, $myRequestObject);
            $response['success']=$myBatch;
            //adding the batch details to database
            if($myBatch){

                $batch_details = Input::only('details');
                for($i=0;$i<sizeof($batch_details['details']);$i++){

                    if(isset($batch_details['details'][$i]['id']))
                        $BatchDetailObj=BatchDetails::find($batch_details['details'][$i]['id']);
                    else
                        $BatchDetailObj= new BatchDetails;
                    $BatchDetailObj->day=$batch_details['details'][$i]['day'];
                    $BatchDetailObj->location=$batch_details['details'][$i]['location'];
                    $BatchDetailObj->start_time=$batch_details['details'][$i]['start_time'];
                    $BatchDetailObj->end_time=$batch_details['details'][$i]['end_time'];
                    if(isset($batch_details['details'][$i]['id']))
                        $BatchDetailObj->id=$batch_details['details'][$i]['id'];
                    $BatchDetailObj->batch_id=$myBatch->id;
                    $BatchDetailObj->save();

                }
                $myBatch->details=BatchDetails::where('batch_id',$myBatch->id)->get();
                $response['batch']=$myBatch;
                $response['success']=true;//$myBatch;
            } else {
                $response['msg']='batch code already exists';
            }



            return $response;
        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex->msg;
            return $response;//$ex;//"false";
        }
    }

    /**
     * Delete a specific Batch.
     *
     */
    public function deleteBatch($id)
    {
        try
        {
            $myBatchObject = new Batch;
            $myBatch = $myBatchObject->deleteBatch($id);
            return $this->getAllBatches();
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    //Start Batch_Detail CRUD Operations

    /**
     * Get Specific BatchDetails.
     * URL - /getBatchDetails/id
     *
     */
    public function getBatchDetails($id)
    {
        try
        {
            $myBatchDetailsObject = new BatchDetails;
            $myBatchDetails = $myBatchDetailsObject->getBatchDetails($id);
            return $myBatchDetails;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    /**
     * Add details about pre-existing Batch.
     * URL - /addBatchDetails/id
     *
     */
    public function addBatchDetails()
    {
        try
        {
            $myBatchDetailsObject = new BatchDetails;
            $myRequestObject = Input::all();
            $myBatchDetails = $myBatchDetailsObject->addnewRequestBatchDetails($myRequestObject);
            return $myBatchDetails;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    /**
     * Update details about pre-existing batch.
     * URL - /updateBatchDetails/id
     *
     */
    public function updateBatchDetails($id)
    {
        try
        {
            $myBatchDetailsObject = new BatchDetails;
            $myRequestObject = Input::all();
            $myBatchDetails = $myBatchDetailsObject->updateBatchDetails($id, $myRequestObject);
            return $myBatchDetails;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    /**
     * Delete Details about pre-existing batch.
     * URL - /deleteBatchDetails/id
     *
     */
    public function deleteBatchDetails($id)
    {
        try
        {
            $myBatchDetailsObject = new BatchDetails;
            $myBatchDetails = $myBatchDetailsObject->deleteBatchDetails($id);
            return $myBatchDetails;
        }
        catch(Exception $ex)
        {
            return "false out";
        }
    }


    public function getBatchInfo($id){
        $batchInfo=array();
        try{
            if (Session::has('admin'))
            {
                $batchInfo['batchInfo']=$this->getBatch($id);
                $batchInfo['batchSchedule']=Batch::find($id)->BatchDetails;
                $batchInfo['batchTaskStatus']=$this->getBatchTaskStatus($id);

                $userJsonArray = Session::get('admin');
                if(isset($userJsonArray)){
                    Log::error($userJsonArray);
                    $teacherId =    json_decode($userJsonArray)[0]->id;
                    $access =    json_decode($userJsonArray)[0]->access;
                    if($access=='admin'  || $batchInfo['batchInfo']['teacher_id']==$teacherId){
                        $batchInfo['batchStudentsList']=Batch::find($id)->Student;

                        foreach($batchInfo['batchStudentsList'] as $student){
                            $reviewingHomeworkCount=DB::select('select * from student_homework where student_id  = :id and status="reviewing"', ['id' => $student->id]);
                            $student['reviewingHomeworkCount']=sizeof($reviewingHomeworkCount);

                            $stars=Stars::all();
                            if (date('w', time()) == 1)
                                $beginning_of_week = date('Y-m-d h:i:s',strtotime('Today',time()));
                            else
                                $beginning_of_week =  date('Y-m-d h:i:s',strtotime('last Sunday',time()));
                            if(date('w', time()) == 7)
                                $end_of_week =  date('Y-m-d h:i:s',strtotime('Today', time()) + 86400);
                            else
                                $end_of_week =  date('Y-m-d h:i:s',strtotime('next Saturday', time()) + 86400);
                            $student['stars'];
                                $studentStars=array();
                            foreach($stars as $star){
                                $ratingStars=array();
                                $ratingStars['ratings']=array();
                                $ratingStars['ratings']['latest_student_rating']=StudentStars::where('student_id',$student->id)->where('batch_id',$id)->where('star_id',$star->id)->where('created_at','>=',$beginning_of_week)->where('created_at','<=',$end_of_week)->count();
                                $ratingStars['ratings']['cumulative_student_rating']=StudentStars::where('student_id',$student->id)->where('batch_id',$id)->where('star_id',$star->id)->count();
                                $ratingStars['star']=$star;
                                $ratingStars['staratingsr']=$star;
//                                array_push($student['stars'],$ratingStars);
                                $studentStars[$star->id]=$ratingStars;
                            }

                            $student['stars']=$studentStars;

                        }
                    }
                    if($batchInfo['batchInfo']['teacher_id']==$teacherId ){
                        $batchInfo['batchHomeworksList']=Batch::find($id)->Homework;

                    }
                    if($access=='admin'){

                        $batchInfo['batchTeacherDetails']=Batch::find($id)->Admin;
                    }
                }
            }
        } catch(Exception $ex){
            var_dump($ex);
        }
        return $batchInfo;
    }

    /**
     * Get Batch Task Status
     * Attributes - batch_id
     * return - Tasks
     * Models used - Student, Batch, LevelTask
     */
    public function getBatchTaskStatus($myBatchId)
    {
        $result = [];
        try
        {
            $myRequestObject = Input::all();
            $myStudentObject = new Student;
            $myLevelTaskObject = new LevelTask;
            $myBatchLevelTaskObject = new BatchLevelTask;
            //            $myBatchId = $myRequestObject['batch_id'];
            $myBatchLevel = $myStudentObject->getStudentLevel($myBatchId);
            $myBatchLevelAllTasks = $myLevelTaskObject->getBatchLevelTasks($myBatchLevel->level_id);
            $myBatchLevelCompletedTasks = $myBatchLevelTaskObject->getBatchLevelTasks($myBatchId);
            $myBatchLevelTasks = $myLevelTaskObject->generateCompletionMapping($myBatchLevelAllTasks, $myBatchLevelCompletedTasks);
            return $myBatchLevelTasks;
        }
        catch(Exception $ex)
        {
            $result['success'] = false;
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }

    /**
     * Change Batch_Level_Tasks
     * Attributes - batch_id, task_id ( level_task )
     * return - to_be_decided
     * Models used - Batch, LevelTask, BatchLevelTask
     */
    public function updateBatchLevelTask()
    {
        $result = [];
        try
        {
            $myRequestObject = Input::all();
            $myBatchLevelTaskObject = new BatchLevelTask;
            $myLevelTaskObject = new LevelTask;
            $myTask = $myRequestObject['task_id'];
            $myBatch = $myRequestObject['batch_id'];
            $isMyTaskParent = $myLevelTaskObject->isParent($myTask);
            $myTaskSubTasks = [];
            if ($isMyTaskParent == "true")
            {
                $myTaskSubTasks = $myLevelTaskObject->getSubTasksList($myTask);
            }
            else
            {
                array_push($myTaskSubTasks, $myTask);
            }
            $updateResult = $myBatchLevelTaskObject->updateBatchLevelTasks($myTaskSubTasks, $myBatch);
            return $updateResult;
        }
        catch(Exception $ex)
        {
            $result['success'] = false;
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            return $result;
        }
    }



    public  function updateStudentTaskStatus($taskId)
    {

        $myRequestObject=Input::all();
        
        $result = [];
        try
        {
            if(isset($myRequestObject['id']))
            {
            $myTask = StudentTaskStatus::where('level_task_id', $taskId)->where('id',$myRequestObject['id'])->first();
            }

            $status = $myRequestObject['status'];
        
                   
            if($myTask->status == 'review' or $myTask->status ==1)
            {
                $myTask->status = $myRequestObject['status'];
                if ($status == "incomplete")
                {
                    $myTask->delete();
                } else 
                $myTask->save();
            }
            
            $result['success']=true;

            $result['data']=$myTask;
            return $result;
        }
        catch(Exception $ex)
        {
            $result['success'] = false;
            $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
            $result['ex'] =$ex->getMessage();
            $result['line'] =$ex->getLine();
            return $result;
        }
    }
}
