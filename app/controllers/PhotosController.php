<?php

class PhotosController extends BaseController {



    public function getPhotos($status='active')
    {

        if($status=='inActive'){
            $galleryEvents = GalleryEvents::all();
            for($i=0;$i<sizeof($galleryEvents);$i++){
                $galleryEvents[$i]['photos']=$galleryEvents[$i]->galleryPhotos()->get();
            }
        } else {
            $galleryEvents = GalleryEvents::where('status','!=','inactive')->get();
            for($i=0;$i<sizeof($galleryEvents);$i++){
                $galleryEvents[$i]['photos']=$galleryEvents[$i]->galleryPhotos()->where('status','!=','inactive')->get();
            }
        }
        return $galleryEvents;
    }
    
    public function getHomePagePhotos($status='active')
    {
        if($status=='inActive'){
            $galleryPhotos=GalleryPhotos::all();
        } else {
            $galleryPhotos=GalleryPhotos::where('status','!=','inactive')->where('show_on_home_page',true)->get();
        }
        return $galleryPhotos;
    }
    
    public function getGalleryEvents(){
        return GalleryEvents::all();
    }
    
    public function addGaleryEvent(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        if(isset($_POST->id) && $_POST->id!="" ){
            $event = GalleryEvents::find($_POST->id);
            $event->name=$_POST->name;
            $event->category=$_POST->category;
            $event->save();
        }
        else if(isset($_POST->name)){
            GalleryEvents::create([
                'name'=> $_POST->name,
                'category'=> $_POST->category,
            ]);
        }        
        return $this->getGalleryEvents();
    }
    
    
    public function deleteGalleryEvent($id){
        $event = GalleryEvents::find($id);
        $success=$event->delete();
        
        echo GalleryEvents::all();
    }
    
    public function toggleGalleryEventStatus($id){
        $event = GalleryEvents::find($id);
        $event->status=(($event->status=='active')?'inactive':'active');
        $event->save();
         echo GalleryEvents::all();
    }
    public function toggleGalleryPhotoStatus($id){
        $photo = GalleryPhotos::find($id);
        $photo->status=(($photo->status=='active')?'inactive':'active');
        $photo->save();
        return $this->getGalleryPhotos();
    }
    
    public function getGalleryPhotos(){
        $galleryPhotos = GalleryPhotos::all();
        foreach($galleryPhotos as $photo)
            $photo->galleryEvents;
        return $galleryPhotos;

    }
    
    public function addGalleryPhoto(){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));

        if(isset($_POST->type) && $_POST->type='video'){
            GalleryPhotos::create([ 
                'name' =>$_POST->name,
                'caption' =>$_POST->caption,
                'gallery_events_id' =>$_POST->gallery_events_id,
                'type' =>$_POST->type,
                'show_on_home_page' =>$_POST->show_on_home_page,
            ]);  
            return $this->getGalleryPhotos();
        }
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $file_name=time().$_FILES[ 'file' ][ 'name' ];
            $uploadPath =public_path().'/content/gallery/'.$file_name;
            $status=move_uploaded_file( $tempPath, $uploadPath );
            if($status){
                GalleryPhotos::create([ 
                    'name' =>$file_name,
                    'caption' =>$_POST['caption'],
                    'gallery_events_id' =>$_POST['gallery_events_id'],
                    'show_on_home_page' =>$_POST['show_on_home_page'],
                ]);  

            }
            return $this->getGalleryPhotos();
        }

        return json_encode($_POST);
    }
    
  
    
    public function updateGalleryPhoto($id){
        if(!$_POST)$_POST = json_decode(file_get_contents("php://input"));
        $photo = GalleryPhotos::find($id);
        $photo->caption=$_POST->caption;
        $photo->gallery_events_id=$_POST->gallery_events_id;
        $photo->show_on_home_page=$_POST->show_on_home_page;
        if($photo->type=='video')
            $photo->name=$_POST->name;
        $photo->save();
        return $this->getGalleryPhotos();
    }
    
    public function deleteGalleryPhoto($id){
        $photo = GalleryPhotos::find($id);
        if($photo->type=='image')
        File::delete(public_path('/content/gallery/').$photo->name);

        $success=$photo->delete();

        echo $this->getGalleryPhotos();
    }


}
