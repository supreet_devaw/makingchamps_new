<?php

class BlogController extends BaseController {


    /**
* Display a list of All Blog Entries.
*
* @return Response
*/
    public function getBlogs(){
        return Blogs::orderBy('id', 'DESC')->get();
    }

    /**
* Store a new Blog in Database.
*
* @return Response
*/
    public function addBlog()
    {
        try
        {
            
            $blog= Blogs::create(Input::all());
           
            return $blog;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }


    /**
* Update the specific blog in Database.
*
* @param  int  $id
* @return Response
*/
    public function updateBlog($id)
    {
        try
        {
            
            $myBlogsObject = new Blogs;
            $myRequestObject = Input::all();
            $myBlogs = $myBlogsObject->updateBlog($id,$myRequestObject);
            return $myBlogs;
        }
        catch(Exception $ex)
        {
            return $ex;//"false";
        }
    }


    /**
* Delete specific Blog from Database.
*
* @param  int  $id
* @return Response
*/
    public function deleteBlog($id)
    {
        try
        {
            
            $blog=Blogs::find($id);
            $blog->delete($id);
            return $this->getBlogs();
        }
        catch(Exception $ex)
        {
            return $ex;//"false";
        }
    }



}
