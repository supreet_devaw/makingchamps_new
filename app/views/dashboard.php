<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Web Application</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!--    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet" type="text/css">-->
    <!-- needs images, font... therefore can not be part of ui.css -->
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/bower_components/weather-icons/css/weather-icons.min.css">
    <!-- end needs images -->

    <link rel="stylesheet" href="/css/admin/main.css">
    <link rel="stylesheet" href="/css/lib/loading-bar.css">

</head>
<body data-ng-app="app" id="app" data-custom-background="" data-off-canvas-nav="" ng-controller="MainCtrl">
<div id ="center" style="position:fixed;top:50%;left:50%;z-index:999"></div>
    <!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <div data-ng-controller="AppCtrl">
        <div data-ng-hide="isSpecificPage()" data-ng-cloak="" class="no-print">
            <section data-ng-include=" '/views/admin/header-admin.html' " id="header" class="top-header"></section>

            <aside data-ng-hide="isTeacherLogin()" data-ng-include=" '/views/admin/nav.html' " id="nav-container"></aside>
        </div>

        <div class="view-container">
            <section data-ng-view="" id="content" class="animate-fade-up"></section>
        </div>
    </div>


<!--    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
    <script src="/scripts/admin/vendor.js"></script>

    <script src="/scripts/admin/ui.js"></script>
    <script src="/scripts/common/lib/spin.min.js"></script>

    <script src="/scripts/admin/app.js"></script>
    <script src="/scripts/admin/MakingChampsAdminApp.js"></script>
    <script src="/scripts/admin/services/apiUtility.js"></script>
    <script src="/scripts/admin/services/userUtility.js"></script>
    <script src="/scripts/admin/services/deleteItem.js"></script>
    <script src="/scripts/admin/directives/ngThumb.js"></script>
    <script src="/scripts/admin/ctrls/HomePageSliderCtrl.js"></script>
    <script src="/scripts/admin/ctrls/DownloadCtrl.js"></script>
    <script src="/scripts/admin/ctrls/GalleryCtrl.js"></script>
    <script src="/scripts/admin/ctrls/SchoolCtrl.js"></script>
    <script src="/scripts/admin/ctrls/TeamCtrl.js"></script>
    <script src="/scripts/admin/ctrls/MainCtrl.js"></script>
    <script src="/scripts/admin/ctrls/ParentCtrl.js"></script>
    <script src="/scripts/admin/ctrls/NewsCtrl.js"></script>
    <script src="/scripts/admin/ctrls/MediaCtrl.js"></script>
    <script src="/scripts/admin/ctrls/StudentCtrl.js"></script>
    <script src="/scripts/admin/ctrls/LoginCtrl.js"></script>
    <script src="/scripts/admin/ctrls/NavCtrl.js"></script>
    <script src="/scripts/admin/ctrls/DashboardCtrl.js"></script>
    <script src="/scripts/admin/ctrls/MyBatchesCtrl.min.js"></script>
    <script src="/scripts/admin/ctrls/BatchesCtrl.js"></script>
    <script src="/scripts/admin/ctrls/BlogCtrl.js"></script>
    <script src="/scripts/admin/ctrls/EventCtrl.js"></script>
    <script src="/scripts/admin/ctrls/TestimonialCtrl.js"></script>
    <script src="/scripts/admin/ctrls/EnquiryCtrl.js"></script>
    
    
    

    <script src="/scripts/common/lib/angular-file-upload.min.js"></script>
    <script src="/scripts/common/lib/loading-bar.js"></script>
    
</body>
</html>