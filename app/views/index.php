<!DOCTYPE html>
<html lang="en" ng-app="makingChampsApp" ng-controller="makingChampsMainCtrl">
    <head>
        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42878819-1', 'makingchamps.co.in');
            ga('send', 'pageview');

        </script>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!--        <title>Making Champs - Program by IIM Alumni and CA for children-->
<!--        </title>-->
        <title ng-bind="pageTitle"></title>
        <link href="css/lib/lazy-image-style.css" rel="stylesheet">
        <link href="css/lib/bootstrap.icon-large.min.css" rel="stylesheet">
        <link href="css/lib/ang-accordion.css" rel="stylesheet">
        <link href="css/lib/angular-bootstrap-lightbox.css" rel="stylesheet">
        <link href="css/lib/angular-socialshare.min.css" rel="stylesheet">
        <link href="css/lib/loading-bar.css" rel="stylesheet">
        <link href="css/lib/bootstrap.min.css" rel="stylesheet">
        <link href="css/lib/jquery-ui.min.css" rel="stylesheet">
        <link href="css/web/MakingChamps.css" rel="stylesheet">
<!--        <link href="css/lib/angular-datepicker.min.css" rel="stylesheet">-->
        
        <link id="baseUrl" href="http://<?=$_SERVER['SERVER_NAME']?><?=$_SERVER['REQUEST_URI']?>" rel="text"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    </head>

    <body ng-controller="makingChampsMainCtrl" style="" >
    <div id ="center" style="position:fixed;top:50%;left:50%;z-index:999; background-color: rgba(0,0,0,0.5);"></div>

    <div id="header" ng-include=" 'views/web/header.html' "></div>



    <div data-ui-view="" class="body-content container slide_view" style="padding:0%;border: 1px #e4e4e4 solid;

                                                                              border-radius: 4px;
                                                                              box-shadow: 0 0 6px #ccc;
                                                                              background-color: #fff;"  >
        </div>
        <div ng-include=" 'views/web/footer.html' "></div>



        <script src="scripts/common/lib/jquery.min.js"></script>
        <script src="scripts/common/lib/jquery-ui.min.js"></script>
    <script src="/scripts/common/lib/spin.min.js"></script>
    <script src="scripts/common/lib/bootstrap.min.js"></script>
        <script src="scripts/common/lib/transition.js"></script>
        <script src="scripts/common/lib/angular.min.js"></script>
        <script src="scripts/common/lib/angular-ui-router.min.js"></script>
        <script src="scripts/common/lib/lazy-image.module.js"></script>
        <script src="scripts/common/lib/lazy-image.directive.js"></script>
        <script src="scripts/common/lib/lazy-image.service.js"></script>
        <script src="scripts/common/lib/ang-accordion.js"></script>
        <script src="scripts/common/lib/angular-isotope.min.js"></script>
        <script src="scripts/common/lib/jquery.isotope.js"></script>
        <script src="scripts/common/lib/ng-scrollto.js"></script>
        <script src="scripts/common/lib/imagesLoaded.min.js"></script>
        <script src="scripts/common/lib/bootbox.min.js"></script>
        <script src="scripts/common/lib/ngBootbox.min.js"></script>
        <script src="scripts/common/lib/angular-file-upload.min.js"></script>
        <script src="scripts/common/lib/angular-animate.js"></script>
<!--        <script src="scripts/common/lib/ui-bootstrap-0.13.0.min.js"></script>-->
        <script src="scripts/common/lib/ui-bootstrap-tpls.js"></script>
        <script src="scripts/common/lib/angular-bootstrap-lightbox.js"></script>
        <script src="scripts/common/lib/loading-bar.js"></script>
        <script src="scripts/common/lib/angular-touch.min.js"></script>
        <script src="scripts/common/lib/angular-socialshare.min.js"></script>
<!--        <script src="scripts/common/lib/angular-datepicker.min.js"></script>-->
        <script src="scripts/common/lib/date.js"></script>

        <script src="scripts/web/MakingChampsApp.js"></script>

        <script src="scripts/web/services/apiUtility.js"></script>
        <script src="scripts/web/services/userUtility.js"></script>

        <script src="scripts/web/ctrls/makingChampsMainCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsHomeCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsOfferingsCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsBatchesCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsDownloadsCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsTestimonalsCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsGalleryCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChmapsAboutCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsMyAccountCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChmapsNewsAndEventsCtrl.js"></script>
        <script src="scripts/web/ctrls/makingChampsLevelCtrl.js"></script>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
       

    </body>
</html>
