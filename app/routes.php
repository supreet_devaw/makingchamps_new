<?php
//Route::get('/', function()
//           {
//               return View::make('index');
//           });
Route::get('/admin', function()
           {
               return View::make('dashboard');
           });

Route::get('/sitemap',function(){
    $content = View::make('sitemap');
    return Response::make($content)->header('Content-Type', 'text/xml;charset=utf-8');
});
Route::group(array('prefix' => 'api/v1', 'before' => 'basic.outh'), function(){

    Route::get('getHomeSlider/{include_inactive?}',      'HomeController@getHomeSlider');
    Route::post('addHomePageSliderImages',      'HomeController@addHomePageSliderImages');
    Route::delete('removeHomePageSliderImage/{id}',      'HomeController@removeHomePageSliderImage');
    Route::get('toggleHomePageSliderImageStatus/{id}',      'HomeController@toggleHomePageSliderImageStatus');

    Route::get('getHomeGallery/{include_inactive?}',      'PhotosController@getHomePagePhotos');

    Route::get('getPhotos/{include_inactive?}',      'PhotosController@getPhotos');

    Route::get('getGalleryEvents',      'PhotosController@getGalleryEvents');
    Route::post('addGaleryEvent',      'PhotosController@addGaleryEvent');
    Route::delete('deleteGalleryEvent/{id}',      'PhotosController@deleteGalleryEvent');
    Route::get('toggleGalleryEventStatus/{id}',      'PhotosController@toggleGalleryEventStatus');

    Route::get('getGalleryPhotos',      'PhotosController@getGalleryPhotos');
    Route::post('addGalleryPhoto',      'PhotosController@addGalleryPhoto');
    Route::post('updateGalleryPhoto/{id}',      'PhotosController@updateGalleryPhoto');
    Route::get('toggleGalleryPhotoStatus/{id}',      'PhotosController@toggleGalleryPhotoStatus');
    Route::delete('deleteGalleryPhoto/{id}',      'PhotosController@deleteGalleryPhoto');




    Route::get('getDownloads/{include_inactive?}',      'DownloadController@getDownloads');

    Route::get('getDownloadCategories',      'DownloadController@getDownloadCategories');
    Route::post('addDownloadCategory',      'DownloadController@addDownloadCategory');
    Route::delete('deleteDownloadCategory/{id}',      'DownloadController@deleteDownloadCategory');

    Route::post('addDonwloadFile',      'DownloadController@addDonwloadFile');
    Route::get('getDownloadFiles',      'DownloadController@getDownloadFiles');
    Route::delete('deleteDownloadFile/{id}',      'DownloadController@deleteDownloadFile');
    Route::get('toggleDownloadFileStatus/{id}',      'DownloadController@toggleDownloadFileStatus');
    Route::post('updateDownloadFile/{id}',      'DownloadController@updateDownloadFile');


    //Testimonial Controller
    Route::get('getTestimonial',      'TestimonialController@getTestimonials');
    Route::get('getNewTestimonials',      'TestimonialController@getNewTestimonials');
    Route::get('getAllTestimonials',      'TestimonialController@getAllTestimonials');
    Route::post('addTestimonial',      'TestimonialController@addTestimonial');
    Route::post('updateTestimonial/{id}',      'TestimonialController@updateTestimonial');


    Route::get('getMedia',      'AboutUsController@getMedia');
    Route::get('getSchools',      'AboutUsController@getSchools');
    Route::post('addSchool',      'AboutUsController@addSchool');
    Route::post('updateSchool/{id}',      'AboutUsController@updateSchool');
    Route::delete('deleteSchool/{id}',      'AboutUsController@deleteSchool');
    Route::delete('deleteSchoolImage/{id}',      'AboutUsController@deleteSchoolImage');




    Route::post('login',      'UserController@login');
    Route::get('getLoggedInUser',      'UserController@getLoggedInUser');
    Route::get('logout',      'UserController@logout');
    Route::get('getParentStudentDetails',      'UserController@getParentStudentDetails');
    Route::post('testimonial',      'UserController@addTestimonial');

    Route::post('addStudentLevelTaskDetails',      'StudentController@addStudentLevelTaskDetails');



    Route::post('enquiry',      'EnquiryController@enquiry');
    Route::get('getNewEnquiries',      'EnquiryController@getNewEnquiries');
    Route::get('getEnquiries',      'EnquiryController@getEnquiries');
    Route::get('getEnquiries',      'EnquiryController@getEnquiries');
    Route::get('getEnquiryDetails/{id}',      'EnquiryController@getEnquiryDetails');

    //Start ParentController Routes
    Route::get('getParents','ParentController@getParents');
    Route::post('addParent','ParentController@addParent');
    Route::get('getParent/{id}', 'ParentController@getParent');
    Route::post('updateParent/{id}','ParentController@updateParent');
    Route::delete('deleteParent/{id}','ParentController@deleteParent');
    Route::delete('deleteParentImage/{id}','ParentController@deleteParentImage');

    
    //Start StudentController Routes
    Route::post('addStudent','StudentController@addStudent');
    Route::get('getStudents','StudentController@getStudents');
    Route::get('getStudent/{id}','StudentController@getStudent');
    Route::post('updateStudent/{id}','StudentController@updateStudent');
    Route::post('changeStudentPassword','StudentController@changeStudentPassword');
    Route::get('validateStudentDetails','StudentController@validateStudentDetails');
    Route::delete('deleteStudent/{id}','StudentController@deleteStudent');
    Route::delete('deleteStudentImage/{id}','StudentController@deleteStudentImage');

    Route::post('forgot_password','UserController@forgot_password');

    
    //Testing Phase
    Route::get('getBatchLevelTaskStatus/{id}','StudentController@getBatchLevelTaskStatus');
    
    
    
    
    //Start Batch Controller
    //Route::get('getBatches','BatchesController@getBatches');
    Route::get('getBatches',      'BatchesController@getBatches');
    Route::get('getAllBatches',      'BatchesController@getAllBatches');
    Route::get('getBatch/{id}','BatchesController@getBatch');
    Route::post('addBatch','BatchesController@addBatch');
    Route::post('updateBatch/{id}','BatchesController@updateBatch');
    Route::delete('deleteBatch/{id}','BatchesController@deleteBatch');
    Route::get('getBatchTaskStatus/{id}','BatchesController@getBatchTaskStatus');
    Route::get('updateBatchLevelTask','BatchesController@updateBatchLevelTask');
    Route::get('toggleSubtaskCompletionStatus/{task_id}/{batch_id}','BatchesController@toggleSubtaskCompletionStatus');
    Route::post('updateStudentTaskStatus/{task_id}','BatchesController@updateStudentTaskStatus');
    
    //Start Batch Details Controller
    Route::get('getBatchDetails/{id}','BatchesController@getBatchDetails');
    Route::post('addBatchDetails','BatchesController@addBatchDetails');
    Route::post('updateBatchDetails/{id}','BatchesController@updateBatchDetails');
    Route::delete('deleteBatchDetails/{id}','BatchesController@deleteBatchDetails');
    
    
    Route::get('getBatchInfo/{id}','BatchesController@getBatchInfo');

    
    
    //Homework Upload Section
    Route::post('homework/{student_homework_id}','HomeworkController@addHomework');
    Route::post('homeworkText/{student_homework_id}','HomeworkController@addHomeworkText');

    
    
    //Team Management Section and also Admin/teacher Login and logout functionality
    Route::get('isTeamMemberLogin','TeamController@isTeamMemberLogin');
    Route::POST('adminLogin','TeamController@adminLogin');
    Route::get('adminLogout','TeamController@adminLogout');
    Route::get('getTeamMembers',      'TeamController@getTeamMembers');
    Route::get('getAllTeamMembers',      'TeamController@getAllTeamMembers');
    Route::delete('deleteTeamMember/{id}','TeamController@deleteTeamMember');
    Route::delete('deleteTeamMemberImage/{id}','TeamController@deleteTeamMemberImage');
    Route::POST('addTeamMember','TeamController@addTeamMember');
    Route::POST('updateTeamMeber/{id}','TeamController@updateTeamMeber');
    Route::get('getTeacherAcademicDetails','TeamController@getTeacherAcademicDetails');
    Route::get('getMyBatches/{list_type}','TeamController@getMyBatches');
//    Route::get('getTeacherUnreviewedHomeworksCount','TeamController@getTeacherUnreviewedHomeworksCount');
    Route::post('changeAdminPassword','TeamController@changeAdminPassword');
    Route::get('validateAdminDetails','TeamController@validateAdminDetails');
    Route::get('updateTestimonialStatus/{id}/{status}','TeamController@updateTestimonialStatus');
    Route::get('getTeacherUnreviewedHomeworks','TeamController@getTeacherUnreviewedHomeworks');
    Route::get('getTeacherIncompleteHomeworks','TeamController@getTeacherIncompleteHomeworks');
    Route::get('getTeacherHomeworks','TeamController@getTeacherHomeworks');
    
    
    
    
    //NewsFeed CRUD OPERATIONS
    Route::get('getNews/{include_inactive?}',      'NewsController@getNews');
    Route::get('getSingleNews/{id}','NewsController@getSingleNews');
    Route::post('addNews','NewsController@addNews');
    Route::post('updateNews/{id}','NewsController@updateNews');
    Route::delete('deleteNews/{id}','NewsController@deleteNews');
    
    
    
    
    //Media CRUD OPERATIONS
    Route::get('getMedia/{id}','MediaController@getMedia');
    Route::get('getAllMedia','MediaController@getAllMedia');
    Route::post('addMedia','MediaController@addMedia');
    Route::post('updateMedia/{id}','MediaController@updateMedia');
    Route::delete('deleteMedia/{id}','MediaController@deleteMedia');

    //AssignHomework
    Route::post('generateHomework','HomeworkController@generateHomework');
    Route::post('editHomework/{id}','HomeworkController@editHomework');
    Route::delete('deleteHomework/{id}','HomeworkController@deleteHomework');
    Route::post('updateHomeworkStatus/{id}','HomeworkController@updateHomeworkStatus');
    Route::post('changeStudentHomeworkStatus/{id}','HomeworkController@changeStudentHomeworkStatus');
    Route::get('assignHomework/{batchId}/{HomeworkId}','HomeworkController@assignHomework');

    //Student Star Actions
    Route::post('rateStudent','TeamController@rateStudent');
    Route::get('getStudentStarDetails/{id}','StudentController@getStudentStarDetails');

    
    Route::get('getStudentBatchDetails/{student_id}','StudentController@getStudentBatchDetails');
    Route::post('addStudentRating','StudentController@addStudentRating');

    
    //Star CRUD
    Route::get('getStar/{id}','TeamController@getStar');
    Route::get('getStars','TeamController@getStars');
    Route::post('addStar','TeamController@addStar');
    Route::post('updateStar/{id}','TeamController@updateStar');
    Route::delete('deleteStar/{id}','TeamController@deleteStar');
    
    //Events
    Route::get('getEvents','EventController@getEvents');
    Route::get('getAllEvents','EventController@getAllEvents');
    Route::post('addEvent','EventController@addEvent');
    Route::delete('deleteEvent/{id}','EventController@deleteEvent');
    
    //Blogs
    Route::get('getBlogs','BlogController@getBlogs');
    Route::post('addBlog','BlogController@addBlog');
    Route::post('updateBlog/{id}',      'BlogController@updateBlog');
    Route::delete('deleteBlog/{id}',      'BlogController@deleteBlog');


    Route::get('emailStudentHomework/{id}','HomeworkController@emailStudentHomework');

});

App::missing(function($exception)
             {
                 
                                return View::make('index');

//                 return Response::view('404', array(), 404);
             });
