<?php

class Homework extends Eloquent{

    protected $table = 'homework';
    
    public function Batch()
    {
        return $this->belongsTo('Batch','batch_id','id');
    }
    
    public function StudentHomework(){
        return $this->HasMany('StudentHomework','homework_id','id');

    }

    public function assignHomework($batchId, $homeworkId)
    {
	
    }

    public function addNewRequestHomework($myRequestObject)
    {
	try
	{
	    $myHomeworkObject = new Homework;
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myHomeworkObject->$key = $value;
		}
	    }
	    $myHomeworkObject->save();
	    return $myHomeworkObject;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success'] = false;
	    $result['msg']='1Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function editHomework($homeworkId, $myRequestObject)
    {
	try
	{
	    $myHomework = Homework::find($homeworkId);
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myHomework->$key = $value;
		}
	    }
	    $myHomework->save();
	    return $myHomework;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function deleteHomework($homeworkId)
    {
	$result = [];
	try
	{
	    $myHomeworkObject = Homework::find($homeworkId);
	    $result = $this->deleteHomeworkFile($homeworkId);
	    if($result['success'])
	    {
		$myHomeworkObject->delete();
		return $myHomeworkObject;
	    }
	    $result['success'] = false;
	    $result['msg'] = 'Failure to delete Homework File. Try again later.';
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function deleteHomeworkFile($id)
    {
	$result = [];
	try
	{
	    $myHomeworkFiles = StudentHomework::where('homework_id', $id)->lists('file_name');
	    foreach($myHomeworkFiles as $myFile)
	    {
		if(file_exists(public_path('/content/student_homework/').$myFile))
		{
		    File::delete(public_path('/content/student_homework/').$myFile);
		}
	    }
	    $result['success'] = true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }


}