<?php

class BatchDetails extends Eloquent{

    protected $table = 'batch_details';
    protected $fillable = array('start_time','end_time','batch_id','day','location');

    
     public function Batch()
    {
        return $this->belongsTo('Batch','id','batch_id');
    }


    public function addnewRequestBatchDetails($myRequestObject)
    {
	try
	{
	    $myBatchDetailsObject = new BatchDetails;
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myBatchDetailsObject->$key = $value;
		}
	    }
	    $myBatchDetailsObject->save();
	    return $myBatchDetailsObject;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    public function updateBatchDetails($id, $myRequestObject)
    {
	try
	{
	    $myBatchDetailsObject = BatchDetails::find($id);
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myBatchDetailsObject->$key = $value;
		}
	    }
	    $myBatchDetailsObject->save();
	    return $myBatchDetailsObject;
	}
	catch(Exception $ex)
	{
	    return "false in";
	}
    }

    public function getBatchDetails($id)
    {
	try
	{
	    $myBatchDetails = BatchDetails::find($id);
	    return $myBatchDetails;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    publiC function deleteBatchDetails($id)
    {
	try
	{
	    $myBatchDetailsObject = BatchDetails::find($id);
	    $myBatchDetailsObject->delete();
	    return $myBatchDetailsObject;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }
    
}