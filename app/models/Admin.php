<?php

class Admin extends Eloquent{

    protected $table = 'admin';
    
    protected $fillable = array('name','email','mobile','post','username','description','linkedin','access','image','password');

    
     public function Batch()
    {
        return $this->hasMany('Batch','teacher_id','id');
    }
    
    protected $hidden = ['password'];

    public function changePassword($adminId, $myRequestObject)
    {
	$result = [];
	try
	{
	    $oldPassword = $myRequestObject['old_password'];
	    $myAdmin = Admin::find($adminId);
	    if((Hash::check($oldPassword, $myAdmin->password)))
	    {
		$myAdmin->password = Hash::make($myRequestObject['new_password']);
		$myAdmin->save();
		$result['success'] = true;
	    }
	    else
	    {
		$result['success'] = false;
		$result['msg'] = 'Current Password does not matches with stored password.';
	    }
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function validate_user_name($userName)
    {
	$result = [];
	try
	{
	    $myAdmins = Admin::where('username', $userName)->get()->count();
	    if ($myAdmins > 0 )
	    {
		$result['success'] = false;
		$result['msg'] = 'Username already exists.Please select another username.';
		return $result;
	    }
	    $result['success'] = true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function forgotPassword($username)
    {
        $resp = array();
        try
        {
            $myAdmin = Admin::where('username',$username)->get();
	    if (empty($myAdmin[0]))
	    {
		$resp['success'] = false;
		$resp['msg'] = 'Invalid user name provided.';
		return $resp;
	    }
            $myAdmin = $myAdmin[0];
            $data=array();
            $myBaseControllerObject = new BaseController;
            $data['data']=array();
            $data['data']['new_password']=$myBaseControllerObject->generateRandomString(6);
            $myAdmin->password = Hash::make($data['data']['new_password']);
            $myAdmin->save();
            $data['data']['admin']=$myAdmin;
            $resp['email_status']=$myBaseControllerObject->sendEmail('emails.forgot_password_admin',$data,$myAdmin->email,$myAdmin->name,'New Password for '.$myAdmin->name);
            $resp['success'] = true;
            $resp['msg'] = 'Your New Password has been emailed to your registered email id';
       } catch(Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg'] = $ex->getMessage();
        }
        return $resp;
    }

}