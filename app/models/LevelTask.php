<?php

class LevelTask extends Eloquent{

    protected $table = 'level_task';
    
    public function StudentTaskStatus()
    {
        return $this->hasMany('StudentTaskStatus','level_task_id','id');
    }
    
    public function getBatchLevelTasks($levelId)
    {
	try
	{
	    $myBatchLevelTasks = LevelTask::where('level_id',$levelId)->where('parent_level','=',0)->get();
	    $count = 0;
	    foreach($myBatchLevelTasks as $parent)
	    {
		$myBatchLevelChildTasks = LevelTask::where('level_id',$levelId)->where('parent_level','=',$parent->id)->get();
		$parent['sub_task'] = $myBatchLevelChildTasks;
	    }
	    return $myBatchLevelTasks;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    public function generateCompletionMapping($allTasks, $completedTasksList,$student_id=null)
    {
	$result = [];
	try
	{
        
        
            foreach($allTasks as $singleTask)
            {
                
                foreach($singleTask->sub_task as $subTask)
                {
                    
                    if(in_array($subTask->id, $completedTasksList))
                    {
                        $subTask["is_completed"] = true;
                    }
                    else
                    {
                        $subTask["is_completed"] = false;
                    }
                    
                    ///showing where the student used it
                    if($student_id!=null)
                        $subTask['student_tasks']=StudentTaskStatus::where('student_id',$student_id)->where('level_task_id',$subTask->id)->get();
		}
	    }
	    return $allTasks;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function isParent($taskId)
    {
	try
	{
	    $myTask = LevelTask::find($taskId);
	    if ($myTask->parent_level == 0)
	    {
		return "true";
	    }
	    return "false";
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    public function getTask($taskId)
    {
	$result = [];
	try
	{
	    $myTask = LevelTask::find($taskId);
	    return $myTask;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function getSubTasksList($parentTaskId)
    {
	$result = [];
	try
	{
	    $myTasks = LevelTask::where('parent_level',$parentTaskId)->lists('id');
	    return $myTasks;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }


}