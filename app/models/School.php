<?php

class School extends Eloquent{

    protected $table = 'school';
    protected $fillable = array('name','address','logo');

    public function Student()
    {
        return $this->hasMany('Student','school_id','id');
    }
    
}