<?php

class HomePageSlider extends Eloquent{

    protected $table = 'home_page_slider';
    protected $fillable = array('image_name', 'image_caption','status');

}