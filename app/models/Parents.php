<?php

class Parents extends Eloquent{

    protected $table = 'parents';

        protected $fillable = array('father_name','father_email','father_job','father_mobile','mother_name','mother_email','mother_job','mother_mobile','image','mother_image');


    public function Student()
    {
        return $this->hasMany('Student','parent_id','id');
    }

    public function Testimonial()
    {
        return $this->hasMany('Testimonial','parent_id','id');
    }

    public function addNewRequestParent($myRequestObject)
    {
        
        $response=array();
        try
        {
            $myParentObject = new Parents;
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myParentObject->$key = $value;
                }
            }
            $myParentObject->save();
            $response['success']=true;
            $response['parent']=$myParentObject;
            return $response;
        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex;
            return $response;

        }


    }

    public function getParents()
    {
        try
        {
            $allParents = Parents::all();
            return $allParents;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function getParent($parentId)
    {
        try
        {
            $parent = Parents::find($parentId);
            return $parent;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function updateParent($parentId, $myRequestObject)
    {
        try
        {
            $myParentObject = Parents::find($parentId);
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myParentObject->$key = $value;
                }
            }
            $myParentObject->save();
            return $myParentObject;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function deleteParent($parentId)
    {
        try
        {
            $myParentObject = Parents::find($parentId);
            $myParentObject->delete();
            return $myParentObject;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }


}