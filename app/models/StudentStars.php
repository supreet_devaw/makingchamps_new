<?php

class StudentStars extends Eloquent{

    protected $table = 'student_stars';
    protected $fillable = array('student_id','star_id','batch_id');


    public function rateStudent($myRequestObject)
    {
	$result = [];
	try
	{
	    $myStudentStar = new StudentStars;
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myStudentStar->$key = $value;
		}
	    }
	    $myStudentStar->save();
        $result['success'] = true;
        $result['data'] = $myStudentStar;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    $result['ex'] = $ex->getMessage();
	    return $result;
	}
    }

//    public function getStudentStarDetails($studentId)
//    {
//	$result = [];
//	try
//	{
//        $stud=Student::find($studentId);
//        $stars=Stars::get();
//        foreach($stars as $star){
//            $star['student_star']=StudentStars::where('student_id',$studentId)->where('star_id',$star->id)->where('batch_id',$stud->batch_id)->get();
//
//        }
//
//        return $stars;
//	}
//	catch(Exception $ex)
//	{
//	    $result['success'] = false;
//	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
//	    return $result;
//	}
//    }

}