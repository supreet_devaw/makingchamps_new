<?php

class DownloadCategory extends Eloquent{

    protected $table = 'download_categories';

    protected $fillable = array('name');

    public function downloadFiles()
    {
        return $this->hasMany('DownloadFiles','download_categories_id','id');
    }
}