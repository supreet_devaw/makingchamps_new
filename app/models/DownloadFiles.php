<?php

class DownloadFiles extends Eloquent{

    protected $table = 'download_files';
    protected $fillable = array('file_name','file_caption','download_categories_id','access');

    public function downloadCategory()
    {
        return $this->belongsTo('DownloadCategory','download_categories_id','id');
    }

}