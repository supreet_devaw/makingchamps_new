<?php

class BatchLevelTask extends Eloquent{

    protected $table = 'batch_level_task';
    protected $fillable = array('batch_id','level_task_id');

    public function Batch()
    {
        return $this->belongsTo('Batch','id','batch_id');
    }

    public function getBatchLevelTasks($batchId)
    {
	try
	{
	    $myBatchLevelTasks = BatchLevelTask::where('batch_id',$batchId)->lists("level_task_id");
	    return $myBatchLevelTasks;
	}
	catch(Exception $ex)
	{
	    return "false in";
	}
    }

    public function updateBatchLevelTasks($levelTasks, $batchId)
    {
	$result = [];
	try
	{
	    foreach($levelTasks as $levelTask)
	    {
		$taskCount = BatchLevelTask::where('level_task_id',$levelTask)->where('batch_id',$batchId)->count();
		if ($taskCount > 0)
		{
		    $myBatchLevelTask =  BatchLevelTask::where('level_task_id',$levelTask)->where('batch_id',$batchId)->first();
		    $myBatchLevelTask->delete();
		}
		else
		{
		    $myBatchLevelTask = new BatchLevelTask;
		    $myBatchLevelTask->batch_id  = $batchId;
		    $myBatchLevelTask->level_task_id = $levelTask;
		    $myBatchLevelTask->save();
		}
	    }
	    $result['success'] = true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['test'] = $taskCount;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

}