<?php

class Media extends Eloquent{

    protected $table = 'media';
    

    public function addNewRequestMedia($myRequestObject)
    {
        try
        {
            $myMediaObject = new Media;
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myMediaObject->$key = $value;
                }
            }
            $myMediaObject->save();
            return $myMediaObject;
        }
        catch(Exception $ex)
        {
            return "false";
        }


    }

    public function getAllMedia()
    {
        try
        {
            $allMedia = Media::all();
            return $allMedia;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function getMedia($mediaId)
    {
        try
        {
            $media = Media::find($mediaId);
            return $media;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function updateMedia($mediaId, $myRequestObject)
    {
        try
        {
            $myMediaObject = Media::find($mediaId);
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myMediaObject->$key = $value;
                }
            }
            $myMediaObject->save();
            return $myMediaObject;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function deleteMedia($mediaId)
    {
        try
        {
            $myMediaObject = Media::find($mediaId);
            $myMediaObject->delete();
            return $myMediaObject;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }



}