<?php

class Batch extends Eloquent{

    protected $table = 'batch';

    public function Admin()
    {
        return $this->belongsTo('Admin','teacher_id','id');
    }

    public function Level()
    {
        return $this->belongsTo('Level','id','level_id');
    }

    public function BatchDetails()
    {
        return $this->hasMany('BatchDetails','batch_id','id');
    }

    public function BatchLevelTask()
    {
        return $this->hasMany('BatchLevelTask','batch_id','id');
    }

    public function Homework()
    {
        return $this->hasMany('Homework','batch_id','id');
    }

    public function Student()
    {
        return $this->hasMany('Student','batch_id','id');
    }

    public function getBatches()
    {
        try
        {
            $allBatches = Batch::all();
            return $allBatches;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function getBatch($batchId)
    {
        try
        {
            $myBatch = Batch::find($batchId);
            return $myBatch;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function addNewRequestBatch($myRequestObject)
    {

        $response=array();
        try
        {
            $myBatchObject = new Batch;
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myBatchObject->$key = $value;
                }
            }
            $myBatchObject->save();
            $response['success']=true;
            $response['batch']=$myBatchObject;
            return $response;
        }
        catch(Exception $ex)
        {
            $response['success']=false;
            $response['msg']=$ex->errorInfo;

            return $response;
        }
    }

    public function updateBatch($batchId, $myRequestObject)
    {
        try
        {
            $myBatchObject = Batch::find($batchId);
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myBatchObject->$key = $value;
                }
            }
            $myBatchObject->save();
            return $myBatchObject;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    public function deleteBatch($batchId)
    {
        try
        {
            $myBatchObject = Batch::find($batchId);
            $myBatchObject->delete();
            return $myBatchObject;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }


}