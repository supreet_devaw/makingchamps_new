<?php

class NewsFeed extends Eloquent{

    protected $table = 'news_feed';


    public function addNewRequestNewsFeed($myRequestObject)
    {
        try
        {
            $myNewsFeedObject = new NewsFeed;
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myNewsFeedObject->$key = $value;
                }
            }
	    $myNewsFeedObject->save();
            return $myNewsFeedObject;
        }
        catch(Exception $ex)
        {
            return "false in";
        }


    }

    public function getNewsFeeds()
    {
        try
        {
            $allNewsFeeds = NewsFeed::all();
            return $allNewsFeeds;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function getNewsFeed($newsFeedId)
    {
        try
        {
            $newsFeed = NewsFeed::find($newsFeedId);
            return $newsFeed;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function updateNewsFeed($newsFeedId, $myRequestObject)
    {
        try
        {
            $myNewsFeedObject = NewsFeed::find($newsFeedId);
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myNewsFeedObject->$key = $value;
                }
            }
            $myNewsFeedObject->save();
            return $myNewsFeedObject;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }

    public function deleteNewsFeed($newsFeedId)
    {
        try
        {
            $myNewsFeedObject = NewsFeed::find($newsFeedId);
            $myNewsFeedObject->delete();
            return $myNewsFeedObject;
        }
        catch(Exception $ex)
        {
            return "false";
        }
    }
    

}