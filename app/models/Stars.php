<?php

class Stars extends Eloquent{

    protected $table = 'stars';
    
    public function StudentStars()
    {
        return $this->hasMany('StudentStars','star_id','id');
    }

    public function addNewRequestStar($myRequestObject)
    {
	$result = [];
	$result['success'] = false;
	try
	{
	    $myStarObject = new Stars;
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myStarObject->$key = $value;
		}
	    }
	    $myStarObject->save();
	    return $myStarObject;
	}
	catch(Exception $ex)
	{
	    Log::error($ex);
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function updateStar($starId, $myRequestObject)
    {
	$result = [];
	$result['success'] = false;
	try
	{
	    $myStarObject = Stars::find($starId);
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myStarObject->$key = $value;
		}
	    }
	    $myStarObject->save();
	    return $myStarObject;
	}
	catch(Exception $ex)
	{
	    Log::error($ex);
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function deleteStar($starId)
    {
	$result = [];
	$result['success'] = false;
	try
	{
	    $myStarObject = Stars::find($starId);
	    $myStarObject->delete();
	    return $myStarObject;
	}
	catch(Exception $ex)
	{
	    Log::error($ex);
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function getStars()
    {
	$result = [];
	$result['success'] = false;
	try
	{
	    $myStarObjects = Stars::all();
	    return $myStarObjects;
	}
	catch(Exception $ex)
	{
	    Log::error($ex);
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function getStar($starId)
    {
	$result = [];
	$result['success'] = false;
	try
	{
	    $myStarObject = Stars::find($starId);
	    return $myStarObject;
	}
	catch(Exception $ex)
	{
	    Log::error($ex);
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

}