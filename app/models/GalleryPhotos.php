<?php

class GalleryPhotos extends Eloquent{

    protected $table = 'gallery_photos';
    protected $fillable = array('name','caption','gallery_events_id','status','type');

    public function galleryEvents()
    {
        return $this->belongsTo('GalleryEvents','gallery_events_id','id');
    }

}