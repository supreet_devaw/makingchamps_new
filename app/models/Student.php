<?php

class Student extends Eloquent{

    protected $table = 'student';
    protected $hidden = ['password'];

     public function Batch()
    {
         return $this->belongsTo('Batch','batch_id','id');
    }
    
     public function Parents()
    {
         return $this->belongsTo('Parents','parent_id','id');
    }
    
    public function School()
    {
        return $this->belongsTo('School','school_id','id');
    }
  
    public function StudentHomework()
    {
        return $this->hasMany('StudentHomework','student_id','id');
    }
    
    public function StudentStars()
    {
        return $this->hasMany('StudentStars','student_id','id');
    }

    public function getStudents()
    {
	try
	{
	    $allStudents = Student::all();
        foreach($allStudents as $student){
            $student['batch_caption']=$student->Batch->caption.' ('.date("j M Y", strtotime($student->Batch->start_date)).' - '.date("j M Y", strtotime($student->Batch->end_date)).')';
            $student['teacher_name']=Batch::find($student->Batch->id)->Admin->name;
            $student['school_name']=$student->School->name;
            $student['father_name']=$student->Parents->father_name;
            $student['mother_name']=$student->Parents->mother_name;
        }
        return $allStudents;
	}
	catch(Exception $ex)
	{
        
        Log::error($ex);
	    return false;
	}
    }

    public function getStudent($studentId)
    {
	try
	{
	    $myStudent = Student::find($studentId);
        $myStudent['batch_caption']=$myStudent->Batch->caption.' ('.date("j M Y", strtotime($myStudent->Batch->start_date)).' - '.date("j M Y", strtotime($myStudent->Batch->end_date)).')';
        $myStudent['teacher_name']=Batch::find($myStudent->Batch->id)->Admin->name;
        $myStudent['teacher']=Batch::find($myStudent->Batch->id)->Admin;
        $myStudent['school_name']=$myStudent->School->name;
        $myStudent['father_name']=$myStudent->Parents->father_name;
        $myStudent['mother_name']=$myStudent->Parents->mother_name;
	    return $myStudent;
	}
	catch(Exception $ex)
	{
        Log::error($ex);
        return false;
	}
    }

    public function addNewRequestStudent($myRequestObject)
    {
        
        $response=array();
	try
	{
	    $myStudentObject = new Student;
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myStudentObject->$key = $value;
		}
	    }
	    $myStudentObject->password=Hash::make('secret');
	    $myStudentObject->save();
        $response['success']=true;
        $response['student']=$myStudentObject;
        return $response;
	}
	catch(Exception $ex)
	{
	    $response['success']=false;
        $response['msg']=$ex->getMessage();
        return $response;
	}
    }
    
    public function updateStudent($studentId, $myRequestObject)
    {
	try
	{
	    $myStudentObject = Student::find($studentId);
	    while(list($key, $value) = each($myRequestObject))
	    {
		if (isset($myRequestObject[$key]))
		{
		    $myStudentObject->$key = $value;
		}
	    }
        Log::error($myStudentObject);
	    $myStudentObject->save();
	    return $myStudentObject;
	}
	catch(Exception $ex)
	{
        
        Log::error($ex);
	    return false;
	}
    }

    public function deleteStudent($studentId)
    {
	try
	{
	    $myStudentObject = Student::find($studentId);
	    $myStudentObject->delete();
	    return $myStudentObject;
	}
	catch(Exception $ex)
	{
	    return false;
	}
    }

    public function getStudentBatch($id)
    {
	try
	{
	    $myStudentBatch = Student::find($id)->batch_id;
	    return $myStudentBatch;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    public function getStudentLevel($batchId)
    {
	try
	{
	    $myStudentLevel = Batch::find($batchId);
	    return $myStudentLevel;
	}
	catch(Exception $ex)
	{
	    return "false level";
	}
    }

    public function getBatchLevelTaskStatus($id)
    {
	try
	{
	    $myStudentObject = new Student;
	    $myStudentBatch = $myStudentObject->getStudentBatch($id);
	    $myStudentLevel = $myStudentObject->getStudentLevel($myStudentBatch);
	    $myBatchLevelTaskObject = new LevelTask;
	    $myBatchLevelTasks = $myBatchLevelTaskObject->getBatchLevelTasks($myStudentLevel->level_id);
	    return $myBatchLevelTasks;
	}
	catch(Exception $ex)
	{
	    return "false student";
	}
    }

    public function getStudentCompletedTaskList($id)
    {
	try
	{
	    $myStudentObject = new Student;
	    $myStudentBatch = $myStudentObject->getStudentBatch($id);
	    $myStudendBatchLevelTaskObject = new BatchLevelTask;
	    $myStudentCompletedTaskList = $myStudendBatchLevelTaskObject->getBatchLevelTasks($myStudentBatch);
	    return $myStudentCompletedTaskList;
	}
	catch(Exception $ex)
	{
	    return "false out";
	}
    }

    public function getBatchStudentsList($batchId)
    {
	try
	{
	    $myBatchStudentsList = Student::where('batch_id',$batchId)->lists('id');
	    return $myBatchStudentsList;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }

    public function changePassword($studentId, $myRequestObject)
    {
	$result = [];
	try
	{
	    $oldPassword = $myRequestObject['old_password'];
	    $myStudent = Student::find($studentId);
	    if((Hash::check($oldPassword, $myStudent->password)))
	    {
		$myStudent->password = Hash::make($myRequestObject['new_password']);
		$myStudent->save();
		$result['success'] = true;
	    }
	    else
	    {
		$result['success'] = false;
		$result['msg'] = "Current Password does not matches";
	    }
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function validate_user_name($userName)
    {
	$result = [];
	try
	{
	    $myStudents = Student::where('user_name', $userName)->get()->count();
	    if ($myStudents > 0 )
	    {
		$result['success'] = false;
		$result['msg'] = 'Username already exists.Please select another username.';
		return $result;
	    }
	    $result['success'] = true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function getStudentName($id)
    {
	$result = [];
	try
	{
	    $myStudentName = Student::find($id);
	    return $myStudentName->name;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function forgotPassword($username)
    {
        $resp = array();
        try
        {
            $myStudent = Student::where('user_name',$username)->get();
	    if (empty($myStudent[0]))
	    {
		$resp['success'] = false;
		$resp['msg'] = 'Invalid user name provided.';
		return $resp;
	    }
            $myStudent = $myStudent[0];
            $data=array();
            $myBaseControllerObject = new BaseController;
            $data['data']=array();
            $data['data']['new_password']=$myBaseControllerObject->generateRandomString(6);
            $myStudent->password = Hash::make($data['data']['new_password']);
            $myStudent->save();
            $data['data']['student']=$myStudent;
            $resp['father_email']=$myStudent->Parents->father_email;$myBaseControllerObject->sendEmail('emails.forgot_password',$data,$myStudent->Parents->father_email,$myStudent->name,'New Password for '.$myStudent->name);
            $resp['mother_email']=$myStudent->Parents->mother_email;$myBaseControllerObject->sendEmail('emails.forgot_password',$data,$myStudent->Parents->mother_email,$myStudent->name,'New Password for '.$myStudent->name);
            $resp['success'] = true;
            $resp['msg'] = 'Your New Password has been emailed to your parents email id';
       } catch(Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg'] = $ex->getMessage();
        }
        return $resp;
    }
}