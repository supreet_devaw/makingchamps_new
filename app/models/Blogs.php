<?php

class Blogs extends Eloquent{

    protected $table = 'blogs';
    protected $fillable = array('caption', 'link');

    
    public function updateBlog($blogId, $myRequestObject)
    {
        try
        {
            $myBlogObject = Blogs::find($blogId);
            while(list($key, $value) = each($myRequestObject))
            {
                if (isset($myRequestObject[$key]))
                {
                    $myBlogObject->$key = $value;
                }
            }
            $myBlogObject->save();
            return $myBlogObject;
        }
        catch(Exception $ex)
        {
            
            return $ex;
            return "false";
        }
    }
}