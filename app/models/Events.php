<?php

class Events extends Eloquent{

    protected $table = 'events';
    protected $fillable = array('caption', 'link','eventDate');

}