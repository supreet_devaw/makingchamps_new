<?php

class StudentTaskStatus extends Eloquent{

    protected $table = 'student_task_status';
    protected $fillable = array('student_id','level_task_id','used_at','comment','status');

    public function Student()
    {
        return $this->belongsTo('Student','id','student_id');
    }

   

}