<?php

class StudentHomework extends Eloquent{

    protected $table = 'student_homework';
    
    public function Homework(){
        return $this->belongsTo('Homework','homework_id','id');
    }

    public function Student(){
        return $this->belongsTo('Student','student_id','id');
    }


    public function assignHomework($homeworkId, $studentsList)
    {
	try
	{
        $studentHomworkIdLists=array();
	    foreach($studentsList as $student)
	    {
		$studentHomeworkObject = new StudentHomework;
		$studentHomeworkObject->student_id = (int)$student;
		$studentHomeworkObject->homework_id = (int)$homeworkId;
		$studentHomeworkObject->save();
            array_push($studentHomworkIdLists,$studentHomeworkObject->id);
	    }
	    $result = [];
	    $result['studentIdLists']=$studentHomworkIdLists;
	    $result['success']=true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result = [];
	    $result['success']=false;
	    $result['msg']='Failure to Connect with MakingChamps Server. Try again later.';
	    return $result;
	}
    }

    public function updateHomeworkStatus($homeworkId, $myRequestObject)
    {
	$result = [];
	try
	{
	    if(isset($myRequestObject['student_id']))
	    {
		$myHomework = StudentHomework::where('homework_id', $homeworkId)->where('student_id',$myRequestObject['student_id'])->first();
	    }

	    $status = $myRequestObject['status'];
	    if($myHomework->status == 'reviewing' or $myHomework->status ==2)
	    {
		$myHomework->status = $myRequestObject['status'];
		$myHomework->save();
		if ($status == 'incomplete' or $status == 3)
		{
		    $myHomework = $this->deleteStudentHomeworkFile($myHomework->file_name);
		}
	    }
	    elseif ($myHomework->status == "incomplete" or $myHomework->status == 3)
	    {
		$result['success'] = false;
		$result['msg'] = 'HomeWork Status is incomplete. Cannot be altered.';
		return $result;
	    }
	    elseif ($myHomework->status == "complete" or $myHomework->status == 1)
	    {
		$result['success'] = false;
		$result['msg'] = 'Homework is already completed.';
		return $result;
	    }
        $result['success']=true;
        $result['data']=$myHomework;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    $result['ex'] =$ex->getMessage();
	    $result['line'] =$ex->getLine();
	    return $result;
	}
    }

    public function deleteStudentHomeworkFile($fileName)
    {
	$result = [];
	try
	{
	    if(file_exists(public_path('/content/student_homework/').$fileName))
	    {
		File::delete(public_path('/content/student_homework/').$fileName);
	    }
	    $result['success'] = true;
	    return $result;
	}
	catch(Exception $ex)
	{
	    $result['success'] = false;
	    $result['msg'] = 'Failure to Connect with MakingChamps Server. Try again later.';
	    $result['ex'] = $ex->getMessage();
	    return $result;
	}
    }

}