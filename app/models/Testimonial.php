<?php

class Testimonial extends Eloquent{

    protected $table = 'testimonial';
    protected $fillable = array('testimonial', 'parent_id','status','posted_by');

    public function Parents()
    {
        return $this->belongsTo('Parents','parent_id','id');
    }

    public function updateStatus($myTestimonialId, $status)
    {
	try
	{
	    $myTestimonial = Testimonial::find($myTestimonialId);
	    $myTestimonial->status = $status;
	    $myTestimonial->save();
	    return $myTestimonial;
	}
	catch(Exception $ex)
	{
	    return "false";
	}
    }
}