<?php

class GalleryEvents extends Eloquent{

    protected $table = 'gallery_events';
    protected $fillable = array('name','status','category');

    
    public function galleryPhotos()
    {
        return $this->hasMany('GalleryPhotos','gallery_events_id','id');
    }
}