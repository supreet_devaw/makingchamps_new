<?php
class NewsFeedTableSeeder extends Seeder 
{

    public function run()  
    {  

        NewsFeed::truncate();
        NewsFeed::create([ 
            'feed' => 'New Batches Starting From 1st April',
            'link' => '',
            'status' =>'new',  
        ]);  

        NewsFeed::create([ 
            'feed' => 'Admissions Open Now',
            'link' => '',
            'status' =>'old',  
        ]);  

        NewsFeed::create([ 
            'feed' => 'Results on 4th march',
            'link' => '',
            'status' =>'inactive',  
        ]);  



    }
}  