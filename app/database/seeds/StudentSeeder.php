<?php
class StudentSeeder extends Seeder 
{

    public function run()  
    {  

        $faker = Faker\Factory::create();

        School::truncate();

        School::create([  
            'name' =>'Anchorwala Umraben Lalji Vidyalaya',  
            'logo' => 'anchorwala.jpg',
            'address' => 'VASHI'
        ]);  
        School::create([  
            'name' =>'Avalon Heights International School',  
            'logo' => 'avalonheights.jpg',
            'address' => 'VASHI'
        ]);  
        School::create([  
            'name' =>'Christ Academy High School',  
            'logo' => 'christacademy.jpg',
            'address' => 'Koparkhairane'
        ]);  
        School::create([  
            'name' =>'Datta Meghe World Academy',  
            'logo' => 'dattameghe.jpg',
            'address' => 'AIROLI'
        ]);  
        School::create([  
            'name' =>'D A V Public School',  
            'logo' => 'dav.jpg',
            'address' => 'AIROLI'
        ]);  
        School::create([  
            'name' =>'Delhi Public School',  
            'logo' => 'dps.jpg',
            'address' => 'Nerul'
        ]);  
        School::create([  
            'name' =>'Euro School',  
            'logo' => 'euro.jpg',
            'address' => 'AIROLI'
        ]);  
        School::create([  
            'name' =>'Fr Agnel Multipurpose School',  
            'logo' => 'fr_angel.jpg',
            'address' => 'Vashi'
        ]);  
        School::create([  
            'name' =>'Goldcrest High',  
            'logo' => 'goldcrest.jpg',
            'address' => 'Vashi'
        ]);  
        School::create([  
            'name' =>'New Horizon Public school',  
            'logo' => 'newhorizon.jpg',
            'address' => 'AIROLI'
        ]);  
        School::create([  
            'name' =>'North Point School',  
            'logo' => 'northpoint.jpg',
            'address' => 'Koparkhairane'
        ]);  
        School::create([  
            'name' =>'Podar International School',  
            'logo' => 'podar.jpg',
            'address' => 'Nerul'
        ]);  
        School::create([  
            'name' =>'Reliance Foundation School',  
            'logo' => 'reliance.jpg',
            'address' => 'Koparkhairane'
        ]);  
        School::create([  
            'name' =>'Ryan International School',  
            'logo' => 'ryan.jpg',
            'address' => 'Sanpada'
        ]);  
        School::create([  
            'name' =>'St Lawrence High School',  
            'logo' => 'st_lawrence.jpg',
            'address' => 'Vashi'
        ]);  
        School::create([  
            'name' =>'St Mary ICSE School',  
            'logo' => 'stmarysicse.jpg',
            'address' => 'Koparkhairane'
        ]);  
        School::create([  
            'name' =>"St. Mary's Mulitipurpose High school",  
            'logo' => 'stmarysvashi.jpg',
            'address' => 'Vashi'
        ]);  

        
        Parents::truncate();
        
        foreach(range(1,15) as $index)  
        {  
            Parents::create([  
                
                'father_name' => $faker->firstName,  
                'father_email' => $faker->email,
                'mother_name' => $faker->firstName,  
                'mother_email' => $faker->email,
                'mother_mobile' => $faker->phoneNumber,  
                'father_mobile' => $faker->phoneNumber,
                'mother_job' => $faker->word,  
                'father_job' => $faker->word,
                'image' => 'g1.jpg'
               
            ]);  
        }
        
        Level::truncate();

       
        Level::create([  
            'name' => 'Level 0'  
        ]);  

        Level::create([  
            'name' => 'Level 1'  
        ]);  

        Level::create([  
            'name' => 'Level 2'  
        ]);  

        Level::create([  
            'name' => 'Level 3'  
        ]);  


        Batch::truncate();
        BatchDetails::truncate();

        $teacherId = Admin::all()->lists('id');

        Batch::create([  
            'caption'=>'Sr KG - 1st',
            'start_date'=>'2015-01-01',
            'end_date'=>'2015-03-31',
            'level_id' => 1,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  
        BatchDetails::create([  
            'start_time'=>'15:15:00',
            'end_time'=>'16:45:00',
            'day'=>'friday',
            'is_followup_session'=>1,
            'batch_id' => 1,
            'location'=> 'Gold Crest High School'

        ]); 

        BatchDetails::create([  
            'start_time'=>'15:15:00',
            'end_time'=>'16:45:00',
            'day'=>'saturday',
            'is_followup_session'=>1,
            'batch_id' => 1,
            'location'=> 'Making Champs Center'

        ]); 

        BatchDetails::create([  
            'start_time'=>'15:15:00',
            'end_time'=>'16:45:00',
            'day'=>'monday',
            'is_followup_session'=>1,
            'batch_id' => 1,
            'location'=> 'Making Champs Center'

        ]); 

        Batch::create([  
            'caption'=> '2nd - 4th',
            'start_date'=>'2015-01-01',
            'end_date'=>'2015-03-31',
            'level_id' => 2,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  
        BatchDetails::create([  
            'start_time'=>'17:00:00',
            'end_time'=>'18:30:00',
            'day'=>'thursday',
            'is_followup_session'=>0,
            'batch_id' => 2,
            'location'=> 'Making Champs Center'

        ]); 
        BatchDetails::create([  
            'start_time'=>'11:00:00',
            'end_time'=>'12:30:00',
            'day'=>'sunday',
            'is_followup_session'=>0,
            'batch_id' => 2,
            'location'=> 'Making Champs Center'

        ]); 


        BatchDetails::create([  
            'start_time'=>'15:15:00',
            'end_time'=>'16:45:00',
            'day'=>'wednesday',
            'is_followup_session'=>0,
            'batch_id' => 2,
            'location'=> 'Gold Crest High School'

        ]); 

        Batch::create([  
            'caption'=>'5th - 7th',
            'start_date'=>'2015-01-01',
            'end_date'=>'2015-03-31',
            'level_id' => 2,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  

        BatchDetails::create([  
            'start_time'=>'17:00:00',
            'end_time'=>'18:30:00',
            'day'=>'tuesday',
            'is_followup_session'=>0,
            'batch_id' => 3,
            'location'=> 'Making Champs Center'

        ]); 

        BatchDetails::create([  
            'start_time'=>'09:30:00',
            'end_time'=>'11:00:00',
            'day'=>'sunday',
            'is_followup_session'=>0,
            'batch_id' => 3,
            'location'=> 'Making Champs Center'

        ]); 

        BatchDetails::create([  
            'start_time'=>'16:00:00',
            'end_time'=>'17:30:00',
            'day'=>'saturday',
            'is_followup_session'=>0,
            'batch_id' => 3,
            'location'=> 'North point School'

        ]); 



        Batch::create([  
            'caption'=>'2nd - 4th',
            'start_date'=>'2015-01-01',
            'end_date'=>'2015-03-31',
            'level_id' => 3,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  

        BatchDetails::create([  
            'start_time'=>'18:30:00',
            'end_time'=>'20:00:00',
            'day'=>'friday',
            'is_followup_session'=>0,
            'batch_id' => 4,
            'location'=> 'Making Champs Center'

        ]); 


        BatchDetails::create([  
            'start_time'=>'18:30:00',
            'end_time'=>'20:00:00',
            'day'=>'wednesday',
            'is_followup_session'=>1,
            'batch_id' => 4,
            'location'=> 'Making Champs Center'

        ]); 


        
        Batch::create([  
            'caption'=>'5th - 7th',
            'start_date'=>'2015-01-01',
            'end_date'=>'2015-03-31',
            'level_id' => 3,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  


        BatchDetails::create([  
            'start_time'=>'15:30:00',
            'end_time'=>'17:00:00',
            'day'=>'saturday',
            'is_followup_session'=>0,
            'batch_id' => 5,
            'location'=> 'Making Champs Center'

        ]); 
        
        Batch::create([  
            'caption'=>'Sr KG - 1st',
            'start_date'=>'2015-04-01',
            'end_date'=>'2015-06-30',
            'level_id' => 1,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  

        BatchDetails::create([  
            'start_time'=>'10:30:00',
            'end_time'=>'12:00:00',
            'day'=>'saturday',
            'is_followup_session'=>0,
            'batch_id' => 6,
            'location'=> 'Making Champs Center'

        ]); 

        Batch::create([  
            'caption'=>'2nd - 4th',
            'start_date'=>'2015-04-01',
            'end_date'=>'2015-06-30',
            'level_id' => 2,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  


        BatchDetails::create([  
            'start_time'=>'12:00:00',
            'end_time'=>'13:30:00',
            'day'=>'sunday',
            'is_followup_session'=>0,
            'batch_id' => 7,
            'location'=> 'Making Champs Center'

        ]); 

        Batch::create([  
            'caption'=>'5th - 7th',
            'start_date'=>'2015-04-01',
            'end_date'=>'2015-06-30',
            'level_id' => 3,
            'teacher_id' => $faker->randomElement($teacherId)

        ]);  

        BatchDetails::create([  
            'start_time'=>'12:00:00',
            'end_time'=>'13:30:00',
            'day'=>'saturday',
            'is_followup_session'=>0,
            'batch_id' => 8,
            'location'=> 'Making Champs Center'

        ]); 

        Student::truncate();
        $parentId = Parents::all()->lists('id');
        $schoolId = School::all()->lists('id');
        $batchId = Batch::all()->lists('id');


        foreach(range(1,30) as $index)  
        {  
            Student::create([ 
                    
                'name'=>$faker->word,
                'user_name'=>$faker->word,
                'password'=>Hash::make('secret'),
                'class'=>$faker->word,
                'user_image'=>"g1.jpg",
                'favourite_subject'=>$faker->word,
                'dob'=>$faker->date,
                'admit_date'=>$faker->date,
                'address'=>$faker->text,
                'hobbies'=>$faker->text,
                'sibling'=>$faker->text,
                'parent_id' => $faker->randomElement($parentId),
                'school_id' => $faker->randomElement($schoolId),
                'batch_id' => $faker->randomElement($batchId)

            ]);  
        }
        
        Homework::truncate();
        $batchId = Batch::all()->lists('id');
        foreach(range(1,30) as $index)  
        {  
            Homework::create([ 
                'homework'=>$faker->text,
                          'batch_id' => $faker->randomElement($batchId)

            ]);  
        }
        
        StudentHomework::truncate();
        $studentId = Student::all()->lists('id');
        $homeworkId = Homework::all()->lists('id');
        
        foreach(range(1,30) as $index)  
        {  
            StudentHomework::create([ 
                'file_name'=>$faker->word,
                          'status'=>$faker->randomNumber($min=1,$max=3),
                          'student_id' => $faker->randomElement($studentId),
                          'homework_id' => $faker->randomElement($homeworkId)

            ]);  
        }
        
        
        Testimonial::truncate();
        $parentId = Parents::all()->lists('id');
        
        foreach(range(1,30) as $index)  
        {  
            Testimonial::create([ 
                'testimonial'=>$faker->text,
                          'status'=>$faker->randomNumber($min=1,$max=3),
                          'parent_id' => $faker->randomElement($parentId),

            ]);  
        }
        
         Media::truncate();
        
        foreach(range(1,30) as $index)  
        {  
            Media::create([ 
                'link'=>$faker->text,
                'caption'=>$faker->word

                ]);  
        }
        
         Enquiry::truncate();
        foreach(range(1,30) as $index)  
        {  
            Enquiry::create([ 
                    
                'student_name'=>$faker->word,
                'school_name'=>$faker->word,
                'class'=>$faker->word,
                'contact'=>$faker->word,
                'parent_name'=>$faker->word,
                'dob'=>$faker->date,
                'location'=>$faker->word,
                'email'=>$faker->word,
                'status'=>$faker->randomNumber($min=1,$max=3)
               ]);  
        }
        
//        LevelTask::truncate();
//        $levelId = Level::all()->lists('id');
//        foreach(range(1,30) as $index)  
//        {  
//            LevelTask::create([                     
//                'caption'=>$faker->word,
//                'parent_level'=>$faker->word,
//                'level_id' => $faker->randomElement($levelId)
//               ]);  
//        }
//        
//         Stars::truncate();
//        foreach(range(1,30) as $index)  
//        {  
//            Stars::create([ 
//                    
//                'name'=>$faker->word,
//                'caption'=>$faker->word,
//                'image'=>$faker->word
//                ]);  
//        }
        
          
        
        BatchLevelTask::truncate();
        $batchId = Batch::all()->lists('id');
        $levelTaskId = LevelTask::all()->lists('id');

        foreach(range(1,30) as $index)  
        {  
            BatchLevelTask::create([ 
                          
                 'batch_id' => $faker->randomElement($batchId),
                 'level_task_id' => $faker->randomElement($levelTaskId)

            ]);  
        }
        
        StudentTaskStatus::truncate();
        $studentId = Student::all()->lists('id');
        $levelTaskId = LevelTask::all()->lists('id');

        foreach(range(1,30) as $index)  
        {  
            StudentTaskStatus::create([ 
                          
                 'student_id' => $faker->randomElement($batchId),
                 'level_task_id' => $faker->randomElement($levelTaskId),
                'used_at'=>$faker->text,
                'comment'=>$faker->text,

            ]);  
        }
        
        StudentStars::truncate();
        $studentId = Student::all()->lists('id');
        $starId = Stars::all()->lists('id');
        $batchId = Batch::all()->lists('id');

        foreach(range(1,30) as $index)  
        {  
            StudentStars::create([ 
                   
                 'rating'=>$faker->randomNumber,
                 'student_id' => $faker->randomElement($studentId),
                 'star_id' => $faker->randomElement($starId),
                 'batch_id' => $faker->randomElement($batchId)

            ]);  
        }    
        
    }
}  



                           