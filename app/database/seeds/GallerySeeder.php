<?php
class GallerySeeder extends Seeder 
{

    public function run()  
    {  
        $faker = Faker\Factory::create();

        GalleryEvents::truncate();
            GalleryEvents::create([ 
                'name' => '1st Prize Distribution Program',  
                'status' =>'active',
                'category'  => $faker->randomNumber($min=1,$max=3)
            ]);  
        
            GalleryEvents::create([ 
                'name' => 'Diwali Holiday Special Session',  
                'status' =>'active',
                'category' => $faker->randomNumber($min=1,$max=3)
            ]);  
        
            GalleryEvents::create([ 
                'name' => 'Demo Session(6th & 7th July)',  
                'status' => 'active',
                'category'=> $faker->randomNumber($min=1,$max=3)
            ]);  
        
            GalleryEvents::create([ 
                'name' => 'Dummy',  
                'status' =>'inactive',
                'category' => $faker->randomNumber($min=1,$max=3)
            ]);
        
        GalleryPhotos::truncate();

        GalleryPhotos::create([ 
            'name' => '1017268_404801866293143_165072409_n.jpg',
            'caption' => 'Children and the proud parents',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '999571_404801942959802_953929153_n.jpg',
            'caption' => 'Young champ Advait with the Chief Guests',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '564216_404801876293142_1110404197_n.jpg',
            'caption' => 'Champ Chaitanya receiving the medal and the Certificate',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '544739_404801919626471_1898795957_n.jpg',
            'caption' => 'Our Host Harsh with the Chief Guests & Champ Chaitanya',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '970664_404801952959801_1502651547_n.jpg',
            'caption' => 'Young Champ Shreeram feeling proud on receiving the medal',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'
        ]);  

        GalleryPhotos::create([ 
            'name' => '1098242_404802009626462_1182833620_n.jpg',
            'caption' => 'Parent of our champ Dr. Viddesh Bari sharing his views',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '544498_404802032959793_1670584373_n.jpg',
            'caption' => 'Professor Mrs. Trupti Harhare & our young host Harsh',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '993333_404802069626456_535067281_n.jpg',
            'caption' => 'Dr. Anuradha Sharma',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '547387_404802079626455_527523486_n.jpg',
            'caption' => 'Chief Guest Mrs. Sumita Krishnakumar addressing at the event',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1006308_404802076293122_701307198_n.jpg',
            'caption' => 'Chief Guest Dr. Deepali Patil encouraging our Champs',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '999713_404802129626450_693246887_n.jpg',
            'caption' => 'Three Cheers for our young CHAMPS',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>1,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '579578_404802132959783_395802269_n.jpg',
            'caption' => 'Facilitators Mili & Prasanna with the CHAMPS',
            'status' =>'active',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1017268_404801866293143_165072409_n.jpg',
            'caption' => 'Children and the proud parents',
            'status' =>'inactive',  
            'gallery_events_id'=>1,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1461840_448453455261317_289781179_n.jpg',
            'caption' => 'Whats going to be next?',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1379683_448453468594649_1544534102_n.jpg',
            'caption' => 'Cheerful girls',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '536209_448453458594650_1837192489_n.jpg',
            'caption' => 'Champs in Action',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1463004_448453555261307_251100086_n.jpg',
            'caption' => 'Mili explaining the game',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '995243_448453565261306_2110897459_n.jpg',
            'caption' => 'Mili with the champs',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1471327_448453641927965_937120991_n.jpg',
            'caption' => 'Ananya and Vihan, coordinating with each other in the game',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1469934_448453558594640_359995692_n.jpg',
            'caption' => 'Chaitanya , blindfolded, following the instructions',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1460289_448453658594630_1901891018_n.jpg',
            'caption' => 'Mr P M Shrawak giving away the awards',
            'status' =>'active',  
            'gallery_events_id'=>2,
            'show_on_home_page'=>1,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '996496_395474707225859_1485302599_n.jpg',
            'caption' => 'Jewson, Aryan and Harsh — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '484696_395474900559173_518410953_n.jpg',
            'caption' => 'Arav speaking on a topic — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '942692_395474733892523_273098241_n.jpg',
            'caption' => 'The class in full flow. Ipsha, Jiya and Aditi — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1004078_395474730559190_1934636357_n.jpg',
            'caption' => 'Jewson eager to answer a question — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1011136_395474753892521_1982019260_n.jpg',
            'caption' => 'Some introspection — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '7978_395474917225838_1998463188_n.jpg',
            'caption' => 'Jainam is saying something. Yash, Ananya and Hetanshi listerning eagerly — at Making Champs Centre',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1004752_395474833892513_358764185_n.jpg',
            'caption' => 'Arav with Prasanna. Ananya and Hetanshi looking on — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1012383_395474820559181_963391988_n.jpg',
            'caption' => 'Siya, Bhakti, Apurva, Yash, Arav, Jainam..... — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1005432_395474793892517_1300734113_n.jpg',
            'caption' => 'deep thoughts! — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1045234_395474930559170_404197398_n.jpg',
            'caption' => 'Yash eager to sat something. Bhakti and Apurva appear amused — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1000078_395474960559167_1969615968_n.jpg',
            'caption' => 'Siya in a deep thought as Bhakti says something — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '1017350_395474990559164_1786407798_n.jpg',
            'caption' => 'Jainam speaking — at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>1,
            'type'=>'image'

        ]);  

        GalleryPhotos::create([ 
            'name' => '996526_395475003892496_519284318_n.jpg',
            'caption' => 'Ananya speaking — with Nidhi Jindal at Making Champs Centre.',
            'status' =>'active',  
            'gallery_events_id'=>3,
            'show_on_home_page'=>0,
            'type'=>'image'

        ]);  

    }
}  