<?php
class MakingChampsTableSeeder extends Seeder 
{

    public function run()  
    {  

        $faker = Faker\Factory::create();

       
        
        BatchDetails::truncate();
        
       foreach(range(1,10) as $index)  
        {  
            BatchDetails::create([ 
               
                'location' => $faker->text,
                'batch_type' =>$faker->randomNumber($min=1,$max=2),
                'level_type' =>$faker->randomNumber($min=1,$max=4)
                ]);  
        }
        
        
        
        
        SessionDetails::truncate();
        $batch_details = BatchDetails::all()->lists('id');
          
        foreach(range(1,30) as $index)  
        {  
            SessionDetails::create([ 
                
                'start_time' => $faker->time($format = 'H:i:s', $max = 'now'),
                'end_time' => $faker->time($format = 'H:i:s', $max = 'now'),
                'is_followup_session' => $faker->boolean(50),
                'is_vacation' => $faker->boolean(50),
                'day' =>$faker->randomNumber($min=1,$max=7),  
                'batch_id'=> $faker->randomElement($batch_details)
                
            ]);  
        }
    
        
        
    
        
        StudentDetails::truncate();
        $batch_details = BatchDetails::all()->lists('id');
          
        foreach(range(1,30) as $index)  
        {  
            StudentDetails::create([ 
                
                'first_name' => $faker->word,
                'last_name' => $faker->word,
                'school_name' => $faker->word,
                'image' => $faker->word,
                'batch_id'=> $faker->randomElement($batch_details)
                
            ]);  
        }
    
    
    
         ParentDetails::truncate();
       
          
        foreach(range(1,30) as $index)  
        {  
            ParentDetails::create([ 
                
                'first_name' => $faker->word,
                'last_name' => $faker->word,
                'job_details' => $faker->word,
                'image' => $faker->word,
                'testinomial' => $faker->text,
                
                
            ]);  
        }
        
        StudentParent::truncate();
        $student_details = StudentDetails::all()->lists('id');
        $parent_details = ParentDetails::all()->lists('id');
         foreach(range(1,20) as $index)  
        {  
            StudentParent::create([ 
            
                'student_id' => $faker->randomElement($student_details),
                'parent_id' => $faker->randomElement($parent_details)
                        
            ]);  
                    
       
    }

    }}  