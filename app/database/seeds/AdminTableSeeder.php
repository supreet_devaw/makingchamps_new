<?php
class AdminTableSeeder extends Seeder 
{

    public function run()  
    {  

        Admin::truncate();
        
        Admin::create([ 
            'name' => 'Prasanna Pahade',
            'email' => 'prasanna@makingchamps.co.in',
            'mobile' =>'+91 98702 95945',  
            'username' =>'admin_prasanna',  
            'password' =>Hash::make('secret'),  
            'linkedin' =>'',  
            'description' =>'MBA from IIM- Calcutta and BE – Mechanical from Govt College of Engineering, Pune (COEP).',  
            'post' =>'Teacher A',  
            'image' =>'prasanna pahade.jpg',  
            'access' =>'admin',  
        ]);  

        Admin::create([ 
            'name' => 'Mili Pahade',
            'email' => 'mili@makingchamps.co.in',
            'mobile' =>'+91 98701 16557',  
            'username' =>'admin_mili',  
            'password' =>Hash::make('secret'),  
            'linkedin' =>'',  
            'description' =>'Chartered Accountant, B Com.',  
            'post' =>'Teacher B',  
            'image' =>'mili pahade.jpg',  
            'access' =>'admin',  
        ]);
        
        Admin::create([ 
            'name' => 'abc abc',
            'email' => 'abc@makingchamps.co.in',
            'mobile' =>'+91 98701 16557',  
            'username' =>'admin_abc',  
            'password' =>Hash::make('secret'),  
            'linkedin' =>'',  
            'description' =>'abc abc abc',  
            'post' =>'Teacher c',  
            'image' =>'abc abc.jpg',  
            'access' =>'teacher',  
        ]);


    }
}  