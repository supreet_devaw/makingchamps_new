<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        $this->call('HomePageSliderTableSeeder');
        $this->command->info('home_page_slider table successfully seeded!');
        
        $this->call('NewsFeedTableSeeder');
        $this->command->info('news_feed table successfully seeded!');
        
        $this->call('GallerySeeder');
        $this->command->info('gallery successfully seeded!');
		
        $this->call('DownloadSeeder');
        $this->command->info('download successfully seeded!');
        
        $this->call('AdminTableSeeder');
        $this->call('StudentSeeder');
        $this->command->info('Making Champs table successfully seeded!');
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}
