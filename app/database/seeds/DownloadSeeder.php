<?php
class DownloadSeeder extends Seeder 
{

    public function run()  
    {  
        DownloadCategory::truncate();
        DownloadFiles::truncate();

        DownloadCategory::create([ 
            'name' => 'Brochures',
        ]);
        
        DownloadFiles::create([ 
            'file_caption' => 'Making Champs Brochure',
            'file_name' => 'Handbill.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 1,
        ]);
        
       
        DownloadCategory::create([ 
            'name' => 'Army Against Time Robbers',
        ]);  

        DownloadFiles::create([ 
            'file_caption' => ' Play time',
            'file_name' => 'Play time.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Homework',
            'file_name' => 'Homework.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Food Time',
            'file_name' => 'Food Time.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Getting Ready',
            'file_name' => 'Getting Ready.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Packing Bags',
            'file_name' => 'Packing Bags.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Coming from School',
            'file_name' => 'Coming from School.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 2,
        ]);
        
        
        DownloadCategory::create([ 
            'name' => 'Trackers & Related',
        ]);  
        DownloadFiles::create([ 
            'file_caption' => 'Priorities Large Print',
            'file_name' => 'priorities large print.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 3,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Learning Capture Template',
            'file_name' => 'learning capture template.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 3,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Daily Tracker New Habits Single Goal',
            'file_name' => 'daily tracker new habits single goal.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 3,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Daily Tracker New Habits 2 Goals',
            'file_name' => 'daily tracker new habits 2 goals.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 3,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Problem Solving Template',
            'file_name' => 'Problem solving template.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 3,
        ]);
        DownloadCategory::create([ 
            'name' => 'Other Charts',
        ]);  
        DownloadFiles::create([ 
            'file_caption' => 'Serenity Prayer',
            'file_name' => 'serenity prayer.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 4,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Self Discipline',
            'file_name' => 'Self Discipline.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 4,
        ]);
        DownloadFiles::create([ 
            'file_caption' => 'Interesting vs Important',
            'file_name' => 'interesting vs important.pdf',
            'access' => 'all',
            'status' => 'active',
            'download_categories_id' => 4,
        ]);



    }
}  