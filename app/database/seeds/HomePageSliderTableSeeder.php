<?php
class HomePageSliderTableSeeder extends Seeder 
{

    public function run()  
    {  
        HomePageSlider::truncate();

        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_1.jpg',
            'status' =>'active'
        ]);  


        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_2.jpg',
            'status' =>'active'
        ]);  


        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_3.jpg',
            'status' =>'active'
        ]);  


        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_4.jpg',
            'status' =>'active'
        ]);  


        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_5.jpg',
            'status' =>'active'
        ]);  


        HomePageSlider::create([ 
            'image_caption' => '',
            'image_name' =>'index_bigbanner_6.jpg',
            'status' =>'active'
        ]);  



    }
}  