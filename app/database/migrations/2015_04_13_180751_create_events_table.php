<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('events',function($table)
                       {
                        $table->increments('id');
                        $table->text('caption');
                        $table->text('link');
                        $table->date('eventDate');
                        $table->timestamps('timestamp');
                    
      });	
    }
                       
	public function down()
	{
        Schema::dropIfExists('events');
    }
}
