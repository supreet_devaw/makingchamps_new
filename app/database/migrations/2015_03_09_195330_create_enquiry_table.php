<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiryTable extends Migration {

	public function up()
	{
        Schema::create('enquiry',function($table)
                       {
                           $table->increments('id');
                           $table->string('student_name');
                           $table->string('school_name');
                           $table->string('class');
                           $table->string('parent_name');
                           $table->string('contact');
                           $table->date('dob');
                           $table->string('email');
                           $table->text('location');
                           $table->timestamps('timestamp');
                       });	
    }

	public function down()
	{
        Schema::dropIfExists('enquiry');
    }

}
