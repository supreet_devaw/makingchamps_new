<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStarsTable extends Migration {

	public function up()
	{
        Schema::create('stars',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->string('caption');
                           $table->string('image');
                                                      $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('stars');
    }


}
