<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('homework',function($table)
                       {
                           $table->increments('id');
                           $table->text('homework');
                           $table->integer('batch_id')->unsigned();
                         $table->timestamps('timestamp');
                        $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade')->onUpdate('cascade');   

                          
                           

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('homework');
    }
}
