<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('school',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->string('logo');
                           $table->string('address');
                           $table->timestamps('timestamp');
                       });	
    }

	public function down()
	{
        Schema::dropIfExists('school');
    }

}
