<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('batch_details',function($table)
                       {
                           $table->increments('id');
                           $table->time('start_time');
                           $table->time('end_time');
                           $table->integer('batch_id')->unsigned();
                           $table->enum('day',array('sunday','monday','tuesday','wednesday','thursday','friday','saturday'));
                           $table->boolean('is_followup_session');
                           $table->string('location');
                           $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade')->onUpdate('cascade');   
                           $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('batch_details');
    }
}
