<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelTaskTable extends Migration {

	public function up()
	{
        Schema::create('level_task',function($table)
                       {
                           $table->increments('id');
                           $table->string('caption');
                           $table->integer('parent_level');
                            $table->timestamps('timestamp');
 

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('level_task');
    }

}
