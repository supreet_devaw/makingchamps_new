<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('student',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->string('user_name');
                           $table->string('password');
                           $table->integer('school_id')->unsigned();
                           $table->integer('parent_id')->unsigned();
                           $table->integer('batch_id')->unsigned();
                           $table->string('class');
                           $table->string('favourite_subject');
                           $table->date('dob');
                           $table->date('admit_date');
                           $table->text('address');
                           $table->text('hobbies');
                           $table->text('sibling');
                           $table->timestamps('timestamp');
                        $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade')->onUpdate('cascade');   
                        $table->foreign('parent_id')->references('id')->on('parents')->onDelete('cascade')->onUpdate('cascade');   
                        $table->foreign('school_id')->references('id')->on('school')->onDelete('cascade')->onUpdate('cascade');   

                          
                           

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('student');
    }
}
