<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentStarsTable extends Migration {

	public function up()
	{
        Schema::create('student_stars',function($table)
                       {
                        $table->increments('id');
                        $table->integer('student_id')->unsigned();
                        $table->integer('star_id')->unsigned();
                        $table->float('rating');
                        $table->timestamps('timestamp');
 
                        $table->foreign('student_id')->references('id')->on('student')->onDelete('cascade')->onUpdate('cascade');   
                        $table->foreign('star_id')->references('id')->on('stars')->onDelete('cascade')->onUpdate('cascade');   
      });	
    }
                       
	public function down()
	{
        Schema::dropIfExists('student_stars');
    }


}
