<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentHomeworkTable extends Migration {

	
	public function up()
	{
        Schema::create('student_homework',function($table)
                       {
                           $table->increments('id');
                           $table->integer('student_id')->unsigned();
                           $table->integer('homework_id')->unsigned();
                           $table->enum('status',array('complete','reviewing','incomplete'))->default('incomplete');
                           $table->string('file_name');
                           $table->foreign('student_id')->references('id')->on('student')->onDelete('cascade')->onUpdate('cascade');   
                           $table->foreign('homework_id')->references('id')->on('homework')->onDelete('cascade')->onUpdate('cascade');   
                            $table->timestamps('timestamp');
                       });	
    }

	public function down()
	{
        Schema::dropIfExists('student_homework');
    }
}
