<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('blogs',function($table)
                       {
                        $table->increments('id');
                        $table->text('caption');
                        $table->text('link');
                        $table->timestamps('timestamp');
      });	
    }
                       
	public function down()
	{
        Schema::dropIfExists('blogs');
    }
}
