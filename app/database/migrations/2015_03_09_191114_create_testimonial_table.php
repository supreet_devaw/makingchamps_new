<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialTable extends Migration {

	public function up()
	{
        Schema::create('testimonial',function($table)
                       {
                           $table->increments('id');
                           $table->integer('parent_id')->unsigned();
                           $table->enum('status',array('pending','approved','rejected'))->default('pending');
                           $table->text('testimonial');
                           $table->foreign('parent_id')->references('id')->on('parents')->onDelete('cascade')->onUpdate('cascade');   
                           $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('testimonial');
    }

}
