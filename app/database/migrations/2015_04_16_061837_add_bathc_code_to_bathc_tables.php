<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBathcCodeToBathcTables extends Migration {

    public function up()
    {
        Schema::table('batch', function($table) {
            $table->string('batch_code')->unique();
        });
    }

    public function down()
    {
        Schema::table('batch', function($t) {
            $t->dropColumn('batch_code');
        });
    }


}
