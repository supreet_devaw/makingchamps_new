<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevelIdToLevelTask extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('level_task', function($table) {
                $table->integer('level_id')->unsigned();
                $table->foreign('level_id')->references('id')->on('level')->onDelete('cascade')->onUpdate('cascade');   
        });
	}

	public function down()
	{
        Schema::table('level_task', function($t) {
            $t->dropColumn('level_id');
        });
	}


}
