<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryEventsTable extends Migration {

    public function up()
    {
        Schema::create('gallery_events',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->enum('status',array('active','inactive'));
                           $table->timestamps('timestamp');
                       });
    }

    public function down()
    {
        Schema::dropIfExists('gallery_events');
    }

}
