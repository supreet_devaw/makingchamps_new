<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToGalleryPhotos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('gallery_photos', function($table) {
            $table->enum('type', array('image', 'video'))->default('image');
        });
	}

	public function down()
	{
        Schema::table('gallery_photos', function($t) {
            $t->dropColumn('type');
        });
	}
}
