<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToStudent extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('student', function($table) {
                $table->string('user_image');
        });
	}

	public function down()
	{
        Schema::table('student', function($t) {
            $t->dropColumn('user_image');
        });
	}

}
