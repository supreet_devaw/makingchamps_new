<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('batch',function($table)
                       {
                           $table->increments('id');
                           $table->string('caption');
                           $table->date('start_date');
                           $table->date('end_date');
                           $table->integer('level_id')->unsigned();
                           $table->integer('teacher_id')->unsigned();
                           $table->timestamps('timestamp');
            $table->foreign('level_id')->references('id')->on('level')->onDelete('cascade')->onUpdate('cascade');   
            $table->foreign('teacher_id')->references('id')->on('admin')->onDelete('cascade')->onUpdate('cascade');   

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('batch');
    }

}
