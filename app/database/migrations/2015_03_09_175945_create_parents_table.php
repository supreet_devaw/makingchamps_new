<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('parents',function($table)
                       {
                           $table->increments('id');
                           $table->string('father_name');
                           $table->string('father_email');
                           $table->string('father_mobile');
                           $table->string('mother_name');
                           $table->string('mother_email');
                           $table->string('mother_mobile');
                           $table->string('mother_job');
                           $table->string('father_job');
                           $table->string('image');
                           
                           $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('parents');
    }

}
