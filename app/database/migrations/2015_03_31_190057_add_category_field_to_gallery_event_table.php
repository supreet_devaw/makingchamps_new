<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryFieldToGalleryEventTable extends Migration {

    public function up()
    {
        Schema::table('gallery_events', function($table) {
            $table->enum('category', array('photos', 'videos','student outputs'))->default('photos');
        });
    }

    public function down()
    {
        Schema::table('gallery_events', function($t) {
            $t->dropColumn('category');
        });
    }

}

