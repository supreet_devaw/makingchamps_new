<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	public function up()
	{
        Schema::create('media',function($table)
                       {
                           $table->increments('id');
                           $table->string('link');
                           $table->string('caption');
                             $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('media');
    }

}
