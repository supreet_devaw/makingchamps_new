<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadFilesTable extends Migration {

    public function up()
    {
        Schema::create('download_files',function($table)
                       {
                           $table->increments('id');
                           $table->string('file_caption');
                           $table->string('file_name');
                           $table->enum('access',array('parent','all'));
                           $table->enum('status',array('active','inactive'));
                           $table->integer('download_categories_id')->unsigned();
                           $table->foreign('download_categories_id')->references('id')->on('download_categories')->onDelete('cascade')->onUpdate('cascade');
                           $table->timestamps('timestamp');
                       });
    }

    public function down()
    {
        Schema::dropIfExists('download_files');
    }


}
