<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsFeedTable extends Migration {

	public function up()
	{
        Schema::create('news_feed',function($table)
                       {
                           $table->increments('id');
                           $table->text('feed');
                           $table->string('link');
                           $table->enum('status', array('new', 'old', 'inactive'));
                           $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('news_feed');
    }

}
