<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('level',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->timestamps('timestamp');

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('level');
    }

}
