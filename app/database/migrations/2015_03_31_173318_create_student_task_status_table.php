<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTaskStatusTable extends Migration {

    public function up()
    {
        Schema::create('student_task_status',function($table)
                       {
                           $table->increments('id');
                           $table->integer('student_id')->unsigned();
                           $table->integer('level_task_id')->unsigned();
                           $table->text('used_at');
                           $table->text('comment');
                           $table->timestamps('timestamp');

                           $table->foreign('student_id')->references('id')->on('student')->onDelete('cascade')->onUpdate('cascade');   
                           $table->foreign('level_task_id')->references('id')->on('level_task')->onDelete('cascade')->onUpdate('cascade'); 
                       });	
    }

    public function down()
    {
        Schema::dropIfExists('student_task_status');
    }

}
