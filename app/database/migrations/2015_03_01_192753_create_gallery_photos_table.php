<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryPhotosTable extends Migration {

    public function up()
    {
        Schema::create('gallery_photos',function($table)
                       {
                           $table->increments('id');
                           $table->integer('gallery_events_id')->unsigned();
                           $table->string('name');
                           $table->string('caption');
                           $table->enum('status',array('active','inactive'));
                           $table->boolean('show_on_home_page');
                           $table->foreign('gallery_events_id')->references('id')->on('gallery_events')->onDelete('cascade')->onUpdate('cascade');   
                           $table->timestamps('timestamp');
                       });
    }

    public function down()
    {
        Schema::dropIfExists('gallery_photos');
    }

}
