<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadCategoriesTable extends Migration {

    public function up()
    {
        Schema::create('download_categories',function($table)
                       {
                           $table->increments('id');
                           $table->string('name');
                           $table->timestamps('timestamp');
                       });	
    }

    public function down()
    {
        Schema::dropIfExists('download_categories');
    }

}
