<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBatchIdToStudentStars extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('student_stars', function($table) {
                $table->integer('batch_id')->unsigned();
                $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade')->onUpdate('cascade');   
        });
	}

	public function down()
	{
        Schema::table('student_stars', function($t) {
            $t->dropColumn('batch_id');
        });
	}

}
