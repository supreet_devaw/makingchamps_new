<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchLevelTaskTable extends Migration {

	public function up()
	{
        Schema::create('batch_level_task',function($table)
                       {
                           $table->increments('id');
                           $table->integer('batch_id')->unsigned();
                           $table->integer('level_task_id')->unsigned();
                           $table->timestamps('timestamp');

                           $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade')->onUpdate('cascade');   
                           $table->foreign('level_task_id')->references('id')->on('level_task')->onDelete('cascade')->onUpdate('cascade');   

                       });	
    }

	public function down()
	{
        Schema::dropIfExists('batch_level_task');
    }

}
