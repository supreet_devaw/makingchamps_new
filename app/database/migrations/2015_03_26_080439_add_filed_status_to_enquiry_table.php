<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledStatusToEnquiryTable extends Migration {

	public function up()
	{
        Schema::table('enquiry', function($table) {
            $table->enum('status', array('new', 'viewed','replied'))->default('new');
        });
	}

	public function down()
	{
        Schema::table('enquiry', function($t) {
            $t->dropColumn('status');
        });
	}

}

