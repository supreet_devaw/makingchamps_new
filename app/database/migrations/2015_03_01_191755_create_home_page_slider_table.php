<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePageSliderTable extends Migration {

    public function up()
    {
        Schema::create('home_page_slider',function($table)
                       {

                           $table->increments('id');
                           $table->string('image_caption');
                           $table->string('image_name');
                           $table->enum('status',array('active','inactive'));
                           $table->timestamps('timestamp');
                       });	}


    public function down()
    {
        Schema::dropIfExists('home_page_slider');
    }

}
